﻿using PSM.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Implementation.StorageDevice))]
namespace PSM.UWP.Implementation
{
    public class StorageDevice : IStorageDevice
    {
        public string GetBatteryLevel()
        {
            return "196";
        }

        public string GetFreeStorageDevice()
        {
            return FreeSize().Result;
        }

        async Task<string> FreeSize()
        {
            string result = string.Empty;

            const String k_freeSpace = "System.FreeSpace";

            DriveInfo[] driveInfos = DriveInfo.GetDrives();
            foreach (var item in driveInfos)
            {
                try
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(item.RootDirectory.FullName);
                    var props = await folder.Properties.RetrievePropertiesAsync(new string[] { k_freeSpace });
                    result += $"{(UInt64)props[k_freeSpace]}|";
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        async Task<string> TotalSize()
        {
            string result = string.Empty;

            const String k_totalSpace = "System.Capacity";

            DriveInfo[] driveInfos = DriveInfo.GetDrives();
            foreach (var item in driveInfos)
            {
                try
                {
                    StorageFolder folder = await StorageFolder.GetFolderFromPathAsync(item.RootDirectory.FullName);
                    var props = await folder.Properties.RetrievePropertiesAsync(new string[] { k_totalSpace });
                    result += $"{(UInt64)props[k_totalSpace]}|";
                }
                catch (Exception ex)
                {

                }
            }

            return result;
        }

        public string GetFullStorageDevice()
        {
            return TotalSize().Result;
        }
    }
}
