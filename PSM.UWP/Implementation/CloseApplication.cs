﻿using PSM.Interfaces;
using Windows.ApplicationModel.Core;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Implementation.CloseApplication))]
namespace PSM.UWP.Implementation
{
    public class CloseApplication : ICloseApplication
    {
        public void CloseApp()
        {
            CoreApplication.Exit();
        }
    }
}
