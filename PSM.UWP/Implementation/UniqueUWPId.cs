﻿using PSM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Implementation.UniqueUWPId))]
namespace PSM.UWP.Implementation
{
    public class UniqueUWPId : IUniqueAndroidId
    {
        public string GetIdentifier()
        {
            return "UWP_PMS_40";
        }

        public List<Tuple<string, string>> GetInstalledApps()
        {
            return new List<Tuple<string, string>>();
        }
    }
}
