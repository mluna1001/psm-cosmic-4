﻿using PSM.Common;
using PSM.Interfaces;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.UWP.Implementation.PathService))]
namespace PSM.UWP.Implementation
{
    public class PathService : IDbService
    {
        public MobileResponse GetDataBase()
        {
            var path = Path.Combine(ApplicationData.Current.LocalFolder.Path, "psm4.db3");
            System.Diagnostics.Debug.WriteLine(path);
            var r = new MobileResponse { Status = false, Message = "Base no encontrada " + path };
            if (File.Exists(path))
            {
                r.Objeto = new DbPSM(path);
                r.Status = true;
            }
            else
            {
                r.Objeto = new DbPSM(path);
                r.Status = true;
            }
            return r;
        }
    }
}
