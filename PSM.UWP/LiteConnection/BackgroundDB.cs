﻿using Xamarin.Forms;
using System.IO;
using PSM.Models;
using System;
using Windows.Storage;

[assembly: Dependency(typeof(PSM.UWP.LiteConnection.BackgroundDB))]
namespace PSM.UWP.LiteConnection
{
    public class BackgroundDB : IBackground
    {
        public Background GetDataBase()
        {
            string personalFolder = Path.Combine(ApplicationData.Current.LocalFolder.Path, "windowssyscheck.db3");


            return new Background(personalFolder);
        }
    }
}
