﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Helpers.DeviceIdentifier
{
    public interface IDevice
    {
        string GetIdentifier();

        List<Tuple<string, string>> GetInstalledApps();
    }
}
