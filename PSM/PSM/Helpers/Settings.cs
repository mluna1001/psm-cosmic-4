﻿using Plugin.Settings;
using Plugin.Settings.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Helpers
{
    public static class Settings
    {
        static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        const string isRemembered = "false"; //IsRemembered
        const string alias = "Alias";

        static readonly string stringDefault = string.Empty;

        public static string IsRemembered
        {
            get
            {
                return AppSettings.GetValueOrDefault(isRemembered, stringDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(isRemembered, value);
            }
        }

        static readonly string stringAliasDefault = string.Empty;

        public static string Alias
        {
            get
            {
                return AppSettings.GetValueOrDefault(alias, stringAliasDefault);
            }
            set
            {
                AppSettings.AddOrUpdateValue(alias, value);
            }
        }
    }
}
