﻿using PSM.Models;
using Xamarin.Forms;

namespace PSM.Helpers.ReadFile
{
    public class ReadFileHelper
    {
        public RespaldoBDUsuarioDto ReadPsmUltimate()
        {
            var bd = new RespaldoBDUsuarioDto();
            var automatic = DependencyService.Get<IReadFile>();
            bd = automatic.ReadFile("psmultimate");
            bd.Typo = 2;
            return bd;
        }
    }
}