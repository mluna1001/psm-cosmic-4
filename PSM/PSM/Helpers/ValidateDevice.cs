﻿using PSM.Helpers.Services;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers
{
    public class ValidateDevice
    {
        public static void Validate(string accion, string page)
        {
            if (Device.RuntimePlatform != Device.UWP)
            {
                if (!CheckAutomaticTime.IsAutomaticTime())
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.HoraFecha, accion, page, "El usuario tiene desactivada la Fecha/Hora automáticas");
                    Device.BeginInvokeOnMainThread(new Action(async () =>
                    {
                        if (!await App.Current.MainPage.DisplayAlert("PSM", 
                            "Tu fecha y hora automáticas están desactivadas, por favor configura esto en tu dispositivo.", 
                            null, 
                            "Aceptar"))
                        {
                            new CloseApplication().CloseApp();
                        }
                    }));
                }
                else
                {
                    if (!CheckAutomaticTime.IsAutomaticZone())
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.HoraFecha, page, "El usuario tiene desactivada la zona horaria automática");
                        Device.BeginInvokeOnMainThread(new Action(async () =>
                        {
                            if (!await App.Current.MainPage.DisplayAlert("PSM", "Tu zona horaria automática está desactivada, por favor configura esto en tu dispositivo.", null, "Aceptar"))
                            {
                                new CloseApplication().CloseApp();
                            }
                        }));
                    }
                }
            }
        }

        private static void GetValidate(string accion, string page)
        {

        }
    }
}
