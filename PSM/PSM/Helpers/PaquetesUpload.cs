﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSM.Helpers
{
    public static class PaquetesUpload
    {
        public static int numeropaquetes(this float paquetes)
        {
            string[] point = paquetes.ToString().Split(new Char[] { '.' });
            int packages = 0;

            if (point.Count() > 1)
            {
                packages = int.Parse(point[0]) + 1;
            }
            else
            {
                packages = int.Parse(point[0]);
            }

            return packages;
        }

        public static void CargaRutaPendiente()
        {
            List<int> idRuta = new List<int>();
            //Rutas pendientes por subir 

            var rpl = App.DB.Ruta.Where(re => re.Status == 3 || re.Status == 2 && re.Sincronizado == false).ToList();
            if (rpl.Count != 0)
            {
                idRuta = rpl.Select(s => s.IdRuta).ToList();
            }
            App.idRuta = idRuta;

        }

        public static void LimpiarRutaPendiente()
        {
            List<int> idRuta = new List<int>();
            App.idRuta = idRuta;
        }
    }
}
