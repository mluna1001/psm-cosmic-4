﻿using PSM.Models.Dto;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PSM.Helpers
{
    public class MapBehaviorAsistencia : BindableBehavior<Map>
    {
        public static readonly BindableProperty ItemsSourceProperty =
            BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable<PointMapDto>), typeof(MapBehaviorAsistencia), null, BindingMode.Default, propertyChanged: ItemsSourceChanged);

        public IEnumerable<PointMapDto> ItemsSource
        {
            get => (IEnumerable<PointMapDto>)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        private static void ItemsSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bindable is MapBehaviorAsistencia behavior)) return;
            behavior.AddPins();
        }

       
        private void AddPins()
        {
            var latitudes = new List<double>();
            var longitudes = new List<double>();

            var map = AssociatedObject;
            for (int i = map.Pins.Count - 1; i >= 0; i--)
            {
                map.Pins[i].Clicked -= PinOnClicked;
                map.Pins.RemoveAt(i);
            }

            var pins = ItemsSource.Select(x =>
            {
                var pin = new Pin
                {
                    Type = PinType.Place,
                    Position = new Position((double)x.Latitude, (double)x.Longitude),
                    Label = x.Name,
                    Address = x.Description,

                };

                pin.Clicked += PinOnClicked;

                latitudes.Add(x.Latitude);
                longitudes.Add(x.Longitude);

                return pin;
            }).ToArray();


            foreach (var pin in pins)
                map.Pins.Add(pin);

            double lowestLat = latitudes.Min();
            double highestLat = latitudes.Max();
            double lowestLong = longitudes.Min();
            double highestLong = longitudes.Max();
            double finalLat = (lowestLat + highestLat) / 2;
            double finalLong = (lowestLong + highestLong) / 2;
            double distance = DistanceCalculation.GeoCodeCalc.CalcDistance(lowestLat, lowestLong, highestLat, highestLong, DistanceCalculation.GeoCodeCalcMeasurement.Kilometers);

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(finalLat, finalLong), Distance.FromKilometers(distance)));
        }

        private void PinOnClicked(object sender, EventArgs eventArgs)
        {
            var pin = sender as Pin;
            if (pin == null) return;
            var viewModel = ItemsSource.FirstOrDefault(x => x.Name == pin.Label);
            if (viewModel == null) return;
            //viewModel.Command.Execute(null); // TODO We are going to implement this later ;)
        }
    }
}
