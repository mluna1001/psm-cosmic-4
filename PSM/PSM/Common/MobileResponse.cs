﻿namespace PSM.Common
{
    public class MobileResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Objeto { get; set; }
    }
}
