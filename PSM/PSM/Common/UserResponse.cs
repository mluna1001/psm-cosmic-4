﻿using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Common
{
    public class UserResponse
    {
        public bool IsAuthenticated { get; set; }
        public bool IsAnotherIMEI { get; set; }
        public int CifraControl { get; set; }
        public User User { get; set; }
        public List<MenuUser> MenuUser { get; set; }
        public List<SectionModule> SectionModule { get; set; }
        public List<Aviso> Avisos { get; set; }
        public string Message { get; set; }
    }

    public class SectionModule
    {
        public int IdSeccion { get; set; }
        public int IdProyecto { get; set; }
        public int IdCatalogoSeccion { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public string CatDescripcion { get; set; }
        public bool EsSeccion { get; set; }
        public bool SeValida { get; set; }
        public bool EsActiva { get; set; }
    }

    public class User
    {
        public int IdUsuario { get; set; }
        public string Alias { get; set; }
        public byte[] Password { get; set; }
        public string Correo { get; set; }
        public string Nombre { get; set; }
        public int IdProyecto { get; set; }
        public int IdRegion { get; set; }
        public string AppCenterToken { get; set; }
        public int BorraBase { get; set; }
        public bool Bloqueado { get; set; }
    }

    public class MenuUser
    {
        public int IdMenuMovil { get; set; }
        public string Descripcion { get; set; }
        public string TargetType { get; set; }
        public string Icon { get; set; }
        public Nullable<int> IdParent { get; set; }
        public int Orden { get; set; }
        public bool Permiso { get; set; }
    }
}
