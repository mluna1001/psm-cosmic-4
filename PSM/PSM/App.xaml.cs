﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PSM.Services;
using PSM.Views;
using PSM.ViewModels;
using System.Collections.Generic;
using PSM.Models;
using Cosmic;
using PSM.Helpers;
using PSM.API;
using PSM.Interfaces;
using System.Linq;
using Microsoft.AppCenter.Crashes;
using System.Threading.Tasks;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;

namespace PSM
{
    public partial class App : Application
    {
        public static string Token { get; set; }
        public static DbPSM DB { get; set; }
        public static PSMClient API { get; set; }
        public static Usuario CurrentUser { get; set; }
        public static BackupUsuario BackupUser { get; set; }
        public static List<BackupMenuUser> BackupMenuUser { get; set; }
        public static int CycleNumber { get; set; }
        public static Page CurrentPage { get; set; }
        public static PresentacionEmpaque CurrentPackage { get; set; }
        public static Ubicacion CurrentLocation { get; set; }
        public static Ruta CurrentRoute { get; set; }
        public static Telefono ThisDevice { get; set; }
        public static string DeviceIdentifier { get; set; }
        public static string InstalledApps { get; set; }
        public static List<Seccion> AllSections { get; set; }
        public static Seccion CurrentSection { get; internal set; }
        public static usuarioModel um { get; set; }
        public static int IdTienda { get; set; }
        public static bool ShowBtnMultimedia { get; set; }
        public static List<int> idRuta { get; set; }
        public static List<UploadRespAudit> CurrentRespAudit { get; set; }
        public static object CurrentUserCosmic { get; internal set; }
        public static List<BackupAviso> BackupAvisos { get; internal set; }
        public static TokenDevice TokenDevice { get; set; }

        public const string MODULO_ENTRADA = "Entrada";
        public const string MODULO_ENTRADAGLOB = "EntradaGlobal";
        public const string MODULO_SALIDA = "Salida";
        public const string MODULO_SALIDAGLOB = "SalidaGlobal";

        public App()
        {
            InitializeComponent();
            DB = DbPSM.GetInstance();

            DeviceIdentifier = DependencyService.Get<IUniqueAndroidId>().GetIdentifier();
            InstalledApps = DependencyService.Get<IUniqueAndroidId>().GetInstalledApps().ToJson();
            ApiUrl.SetUrl();

            API = new PSMClient();

            if (!Settings.IsRemembered.Equals("true"))
            {
                MainViewModel.GetInstance().Login = new LoginViewModel();
                MainPage = new NavigationPage(new LoginPage());
            }
            else
            {
                CurrentUser = DB.Usuario.FirstOrDefault(u => u.Alias.Equals(Settings.Alias));
                if (CurrentUser is null)
                {
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    MainPage = new NavigationPage(new LoginPage());
                }
                else
                {
                    var mvm = MainViewModel.GetInstance();
                    MainViewModel.GetInstance().Avisos = new AvisosViewModel();
                    MainViewModel.GetInstance().About = new AboutViewModel();
                    MainViewModel.GetInstance().Tienda = new TiendaViewModel();
                    MainViewModel.GetInstance().Ubicacion = new UbicacionViewModel();
                    MainViewModel.GetInstance().Descarga = new DescargaViewModel();

                    MainViewModel.GetInstance().RutaEstatus = new RutaEstatusViewModel();
                    MainViewModel.GetInstance().RutaEstatusItem = new RutaEstatusItemViewModel();
                    MainViewModel.GetInstance().Perfil = new PerfilViewModel();
                    MainViewModel.GetInstance().Impersonal = new ImpersonalViewModel();
                    mvm.Semaforo = new SemaforoViewModel();
                    mvm.SemaforoItemViewModel = new SemaforoItemViewModel();
                    mvm.SemaforoMap = new SemaforoMapViewModel();
                    mvm.LoadMenu();

                    if (Xamarin.Forms.Device.RuntimePlatform != Xamarin.Forms.Device.UWP)
                        MainPage = new MainPage();
                    else
                        MainPage = new NavigationPage(new MainPage());

                    UploadRespuestas();
                }
            }
        }

        public static IDictionary<string, string> TrackData(Exception ex)
        {
            var dic = new Dictionary<string, string>();
            if (CurrentUser != null)
            {
                dic.Add("UserLogged", CurrentUser.Alias);
                dic.Add("Proyecto", CurrentUser.IdProyecto.ToString());
            }
            else
                dic.Add("User Logged", "No one");
            return dic;
        }

        public static IDictionary<string, string> TrackData()
        {
            var dic = new Dictionary<string, string>();
            if (CurrentUser != null)
            {
                dic.Add("UserLogged", CurrentUser.Alias);
                dic.Add("Proyecto", CurrentUser.IdProyecto.ToString());
            }
            else
                dic.Add("User Logged", "No one");
            return dic;
        }

        protected override void OnStart()
        {
            AppCenter.Start("android=a016dc85-9daf-4fde-9eae-081fcd5f8234;" +
                  "uwp=f7e9284d-8725-4698-8baf-a9c7a055cf05;" +
                  "ios=20a813fa-8c3e-4551-a413-2dda9bb4f693",
                  typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        #region Methods
        public static async void UploadRespuestas()
        {
            // Checa red WIFI si hay wiki se comienza a subr la tienda.
            if (await ConnectTools.IsNetwork())
            {
                try
                {
                    var rutaEstatusItemVM = new RutaEstatusItemViewModel();
                    await rutaEstatusItemVM.Upload(true);

                }
                catch (Exception ex)
                {
                
                    Crashes.TrackError(ex, App.TrackData());
                }
            }
        }
        #endregion
    }
}