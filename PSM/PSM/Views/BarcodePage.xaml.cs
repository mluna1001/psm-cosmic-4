﻿using Cosmic;
using PSM.Interfaces;
using PSM.Models;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BarcodePage : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
            }
        }
        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return this.isLoading;
            }

            set
            {
                this.isLoading = value;
                RaisePropertyChanged("IsLoading");
            }
        }
        private int? sIdProducto = null;

        private List<PreguntaModel> __models;
        Button btnsave { get; set; }
        public List<PreguntaModel> _models
        {
            get { return __models; }
            set { __models = value; }
        }

        public bool EditAnswers { get; set; }

        public BarcodePage()
        {
            InitializeComponent();
            BindingContext = this;
            Default();
        }

        private void Default()
        {
            btnScan.Clicked += BtnScan_Clicked;

            App.CurrentPage = this;
            // no es posible editar las respuestas
            EditAnswers = false;
            // Inicializamos la seccion
        }

        #region Events
        private async void BtnScan_Clicked(object sender, EventArgs e)
        {
            if (Device.RuntimePlatform != Device.UWP)
            {
                App.CurrentLocation = App.DB.Ubicacion.FirstOrDefault(u => u.IdSeccion == App.CurrentSection.IdSeccion);
                var scanner = DependencyService.Get<IQrCodeScanningService>();
                var result = await scanner.ScanAsync();
                if (result != null)
                {
                    barcode.Text = result;
                    var p = App.DB.Producto.Where(s => s.SKU == result).ToList().GetFirst();
                    if (p != null)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.PreciosPageBarCode, "BtnScan_Clicked", "BarCodePage", "Navegando a precio");
                        await Navigation.PushAsync(new PreciosPage(new List<Producto> { p }));
                    }
                    else
                    {
                        await DisplayAlert("PSM 360", "El codigo de barras no es correcto.", "Aceptar");
                    }
                }
            }
            else
            {
                string result = "7501058619211";
                barcode.Text = result;
                var p = App.DB.Producto.Where(s => s.SKU == result).ToList().GetFirst();
                if (p != null)
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.PreciosPageBarCode, "BtnContinuar_Clicked", "BarCodePage", "Navegando a precio");
                    await Navigation.PushAsync(new PreciosPage(new List<Producto> { p }));
                }
            }
        }
        #endregion

    }
    //public partial class BarcodePage : ContentPage
    //{
    //    public BarcodePage()
    //    {
    //        InitializeComponent();
    //    }
    //}
}