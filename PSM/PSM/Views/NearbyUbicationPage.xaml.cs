﻿using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyUbicationPage : ContentPage
    {
        public NearbyUbicationPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            MainViewModel.GetInstance().NearbyUbication = new NearbyUbicationViewModel();
            slNearbyUbication.BindingContext = MainViewModel.GetInstance().NearbyUbication;
        }

        private void lvTiendasCercanas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvTiendasCercanas.SelectedItem = null;
        }
    }
}