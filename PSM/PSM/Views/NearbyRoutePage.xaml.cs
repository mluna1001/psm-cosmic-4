﻿using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NearbyRoutePage : ContentPage
    {
        public NearbyRoutePage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            MainViewModel.GetInstance().NearbyRoute = new NearbyRouteViewModel();
            slNearbyRoute.BindingContext = MainViewModel.GetInstance().NearbyRoute;
        }
        private void lvTiendasCercanas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvTiendasCercanas.SelectedItem = null;
        }
    }
}