﻿using Microsoft.AppCenter.Crashes;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.Models;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace PSM
{

    public partial class PhotoPage : ContentPage
    {
        PreguntaModel _model;
        public bool EditAnswers { get; set; }
        public PhotoPage()
        {
            InitializeComponent();
            App.CurrentPage = this;
            EditAnswers = false;
            Title = App.CurrentSection != null ? App.CurrentSection.Descripcion : "";
            Init();
        }

        int numerocaja = 0;
        private async void Init()
        {
            if (App.CurrentSection == null) return;
            // obtenemos todas las preguntas de esta seccion [_seccion]
            _model = PreguntaHelper.GetPreguntasModuloPhoto();
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_model == null)
            {
                await DisplayAlert("PSM 360", "No hay preguntas para esta modulo ", "Aceptar");
                await Navigation.PopAsync();
                return;
            }

            // obtenemos el máximo para el número de caja
            int? max = App.DB.RespuestaAuditor.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion && e.IdRuta == App.CurrentRoute.IdRuta).Max(e => e.NumerodeCaja);
            numerocaja = max.HasValue ? max.Value : 0;
            numerocaja = numerocaja + 1;
            App.CycleNumber = numerocaja;
            // asignamos las preguntas
            await SetPreguntas(_model);
        }

        private async Task SetPreguntas(PreguntaModel model)
        {
            // Limpiamos la página
            RootLayout.Children.Clear();
            try
            {
                Label label = new Label
                {
                    Text = model.FotoNombre,
                    FontAttributes = FontAttributes.Bold
                };
                // boton para tomar foto
                Button btnfoto = new Button
                {
                    Text = "Tomar foto"
                };
                // asignamos el id unico a la vista
                btnfoto.ClassId = model.GetHashCode().ToString();
                // asignamos el evento de click al boton
                btnfoto.Clicked += Btnfoto_Clicked;
                // si el botón tiene una respuesta previa, la asignamos
                if (!string.IsNullOrEmpty(model.Respuesta))
                {
                    btnfoto.BackgroundColor = Color.Accent;
                    btnfoto.TextColor = Color.White;
                }
                btnfoto.CornerRadius = 20;
                // asignamos la pregunta y el botno a la vista
                RootLayout.Children.Add(label);
                RootLayout.Children.Add(btnfoto);

            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
        }

        private async void Btnfoto_Clicked(object sender, EventArgs e)
        {
            var btnfoto = sender as Button;
            int hashcode = int.Parse(btnfoto.ClassId);
            var model = _model;
            if (model != null)
            {
                // nombre de la foto
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + model.IdPregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                // task para capturar foto
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                // obtenemos la foto
                var result = await task.TakePhoto();
                // si existe la foto
                if (result != null)
                {
                    // si falló la captura
                    if (!result.Success)
                    {
                        // mostramos el error generado
                        await DisplayAlert("PSM 360", result.Message, "Aceptar");
                    }
                    else
                    {
                        // guardamos el path en el modelo
                        model.Respuesta = result.Photo.Path;
                        // guardamos el archivo en el modelo
                        model.File = result;
                        // asignamos un color al botón
                        btnfoto.BackgroundColor = Color.Accent;
                    }
                }
            }
            App.CycleNumber = 0;
        }

        private void BtnSaveAnswers_Clicked(object sender, EventArgs e)
        {
            // quitamos al evento el metodo questionpage_on...
            OnRespuestaAuditorComplete -= QuestionPage_OnRespuestaAuditorComplete;
            // asignamos el metodo al evento
            OnRespuestaAuditorComplete += QuestionPage_OnRespuestaAuditorComplete;
            // procesamos las respuestas para poder guardarlas
            RespuestaAuditorProcess();
        }

        private async void QuestionPage_OnRespuestaAuditorComplete(object sender, RespuestaAuditorComplete e)
        {
            // se ha completado la seccion?
            if (!e.IsSuccess)
            {
                // no se ha completado, entonces mandamos un mensaje de error
                await DisplayAlert("PSM 360", e.Message, "Aceptar");
            }
            else
            {
                // obtebemos la fecha en que se termino la seccion
                var dateofregister = DateTime.Now;
                // iteramos las respuestas para asignar la fecha
                foreach (var respuesta in e.Respuestas)
                {
                    // asignamos la fecha actual a las respuestas
                    respuesta.Fecha = dateofregister;
                    // insertamos el valor de la respuesta en la base de datos
                    App.DB.RespuestaAuditor.Add(respuesta);
                }

                try
                {
                    // guardamos los cambios de la base de datos
                    App.DB.SaveChanges();
                    // mandamos confirmación de que se ha guardado las respuestas
                    await DisplayAlert("PSM 360", "Se han guardado las respuestas", "Aceptar");
                    // regresamos a la página anterior
                    await Navigation.PopAsync();
                    // si el catalogo es una inicidencia entonces procesamos la ruta
                    if (App.CurrentSection.IdCatalogoSeccion == 11)
                    {
                        // lanzamos el evento IncidenciaComplete, para cacharlo en cualquier parte del código en la que se haya implementado
                        OnIncidenciaCompleta();
                    }
                }
                catch
                {
                    // no se pudo guardar los datos en la base de datos...
                    await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                    // regresamos a la ventana anterior
                    await Navigation.PopAsync();
                }
            }
        }

        private event EventHandler<RespuestaAuditorComplete> OnRespuestaAuditorComplete;

        private void OnRespuestaAuditorCompleted(RespuestaAuditorComplete answer)
        {
            if (OnRespuestaAuditorComplete != null)
            {
                OnRespuestaAuditorComplete(this, answer);
            }
        }

        /// <summary>
        /// Método que procesa todas las respuestas generadas en la pagina
        /// </summary>
        public void RespuestaAuditorProcess()
        {
            // Creamos una lista de respuestas pre asignada
            List<RespuestaAuditor> respuestas = new List<RespuestaAuditor>();
            // valor que indica si se ha completado o no la seccion
            bool notcomplete = false;

            if (_model.IdControl == 6) // la pregunta es un boton de foto
            {
                if (_model.PreguntaPadre.SeValida.HasValue && _model.PreguntaPadre.SeValida.Value)
                {
                    // se debe verificar que la respuesta exista
                    // verificamos que haya una foto
                    if (_model.File == null) // no  hay foto entonces
                    {
                        // mandamos estatus false para decir que no se lleno la sección
                        OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                        {
                            IsSuccess = false,
                            Message = "Toma la foto "
                        });
                        // seccion incompleta
                        notcomplete = true;
                    }
                }

                // guardamos el valor de la respuesta
                RespuestaAuditor answer = new RespuestaAuditor
                {
                    IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                    IdRespuesta = _model.IdRespuesta,
                    IdRuta = App.CurrentRoute.IdRuta,
                    IdSeccion = _model.IdSeccion,
                    IdUbicacion = null,
                    NumerodeCaja = numerocaja,
                    Foto = _model.Foto,
                    FotoNombre = _model.FotoNombre,
                    IdProducto = null
                };
                respuestas.Add(answer);
            }
            
            //la sección se completo?
            if (!notcomplete)
            {
                // lanzamos el evento de que se ha completado, con las preguntas y el valor de si se debe hacer un update...
                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                {
                    IsSuccess = true,
                    Edit = EditAnswers,
                    Respuestas = respuestas
                });
            }
        }

        /// <summary>
        /// EVento para saber cuando una incidencia es completada
        /// </summary>
        public event EventHandler<EventArgs> InicidenciaCompleta;

        private void OnIncidenciaCompleta()
        {
            InicidenciaCompleta?.Invoke(this, EventArgs.Empty);
        }
        async void ImageButton_Clicked(System.Object sender, System.EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }
}