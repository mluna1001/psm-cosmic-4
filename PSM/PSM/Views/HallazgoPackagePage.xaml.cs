﻿using PSM.Function;
using PSM.Models;
using PSM.ViewModels;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HallazgoPackagePage : CarouselPage
    {
        private List<Producto> _presentaciones;

        public HallazgoPackagePage(List<Producto> presentaciones)
        {
            InitializeComponent();
            App.CurrentPage = this;
            _presentaciones = presentaciones;
            Init();
        }

        private void Init()
        {
            PreciosPage.SavePage += PreciosPage_SavePage;
            for (int i = 0; i < _presentaciones.Count; i++)
            {
                string backtext = "Anterior";
                string nexttext = "Siguiente";
                if ((i + 1) == _presentaciones.Count)
                {
                    nexttext = "Terminar sección";
                }
                var model = new HallazgoPackageModel
                {
                    Index = i,
                    BtnBackText = backtext,
                    BtnNextText = nexttext,
                    BtnBackCommand = new Command(BtnBack),
                    BtnNextCommand = new Command(BtnNext)
                };

                var producto = _presentaciones[i];
                PreciosPage package = new PreciosPage(new List<Producto> { producto }, model);
                this.Children.Add(package);
            }
        }

        private Dictionary<int, PreciosPage> preciospagetoremove = new Dictionary<int, PreciosPage>();

        private void BtnNext(object obj)
        {
            var model = obj as HallazgoPackageModel;
            if (model != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnNext", "HallazgoPackagePage", "Siguiente");
                var preciospage = model.Page as PreciosPage;
                if (preciospage != null)
                {
                    if (preciospagetoremove.ContainsKey(model.Index))
                    {
                        preciospagetoremove[model.Index] = preciospage;
                    }
                    else
                    {
                        preciospagetoremove.Add(model.Index, preciospage);
                    }
                    NextPage();
                }

                if (model.BtnNextText.Equals("Terminar sección"))
                {
                    SaveSecction();
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalioDeSeccionSinGuardar, "OnBackButtonPressed", "HallazgoPackagePage", $"Salió de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} sin guardar respuestas");
            Navigation.PopAsync();
            return true;
        }

        private void NextPage()
        {
            this.ShowNext();
        }

        private void BtnBack(object obj)
        {
            var model = obj as HallazgoPackageModel;
            if (model != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BtnBack", "HallazgoPackagePage", "Anterior");
                var preciospage = model.Page as PreciosPage;
                if (preciospage != null)
                {
                    if (preciospagetoremove.ContainsKey(model.Index))
                    {
                        preciospagetoremove[model.Index] = preciospage;
                    }
                    else
                    {
                        preciospagetoremove.Add(model.Index, preciospage);
                    }
                    PreviusPage();
                }
                if (model.BtnNextText.Equals("Terminar sección"))
                {
                    SaveSecction();
                }
            }
        }

        private void PreviusPage()
        {
            this.ShowPrevious();
        }

        private void SaveSecction()
        {
            foreach (var pageitem in preciospagetoremove)
            {
                pageitem.Value.SaveSection();
            }
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "SaveSecction", "HallazgoPackagePage", $"Se guardaron las respuestas de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
        }

        private async void PreciosPage_SavePage(object sender, SavePage e)
        {
            if (e.Success)
            {
                Children.Remove(e.Page);
                if (Children.Count == 0)
                {
                    await DisplayAlert("PSM 360", "Se han guardado las respuestas correctamente", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "PreciosPage_SavePage", "HallazgoPackagePage", "Se han guardado las respuestas correctamente");
                    var seccionL = MainViewModel.GetInstance().Modulo.GetSeccionList();
                    MainViewModel.GetInstance().Modulo.ListSecciones = seccionL;

                    var seccionobligatoriofaltaL = MainViewModel.GetInstance().Modulo.GetSeccionListFaltante();
                    MainViewModel.GetInstance().Modulo.ListSeccionesFaltantes = seccionobligatoriofaltaL;

                    await Navigation.PopAsync();
                }
            }
            else
            {
                await DisplayAlert("PSM 360", "Te hace falta rellenar algunos campos", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.FaltaLlenarCampos, "PreciosPage_SavePage", "HallazgoPackagePage", "Te hace falta rellenar algunos campos");
            }
        }
    }
}