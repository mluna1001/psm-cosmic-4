﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TiendaPage : ContentPage
    {
        public TiendaPage()
        {
            InitializeComponent();
        }

        private void ListaTiendas_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListaTiendas.SelectedItem = null;
        }
    }
}