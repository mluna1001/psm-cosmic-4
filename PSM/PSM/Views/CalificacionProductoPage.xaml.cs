﻿using Cosmic;
using PSM.Controls;
using PSM.Function;
using PSM.Models;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    public partial class CalificacionProductoPage : ContentPage
    {

        public static event EventHandler<SavePage> SavePage;
        public bool EditAnswers { get; set; }

        public static void OnSavePage(SavePage page)
        {
            if (SavePage != null)
            {
                SavePage.Invoke(null, page);
            }
        }

        /// <summary>
        /// Lista de presentaciones o empaques
        /// </summary>
        private List<Producto> _presentaciones;

        /// <summary>
        /// Lista de preguntas
        /// </summary>
        private List<PreguntaModel> _models;
        Button btnsave { get; set; }
        public bool _hallazgo { get; set; }
        private bool _promo { get; set; }

        //HallazgoPackageViewModel
        public CalificacionProductoPage(List<Producto> presentaciones, HallazgoPackageModel hallazomodel = null, bool promo = false)
        {
            InitializeComponent();
            App.CurrentPage = this;
            _presentaciones = presentaciones;
            _hallazgo = hallazomodel != null;
            _promo = promo;

            if (_hallazgo)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    GridHallazgo.IsVisible = true;
                });
                hallazomodel.Page = this;
                BindingContext = hallazomodel;
                SaveAnswers.IsVisible = false;
            }
            else
            {
                // StartToolBar();
            }

            Init();
        }

        private void StartToolBar()
        {
            ToolbarItem salvarseccion = new ToolbarItem
            {
                Icon = "diskette.png",
                Priority = 0,
                Order = ToolbarItemOrder.Primary,
                Text = "Salvar respuestas"
            };
            salvarseccion.Clicked += Salvarseccion_Clicked;
            ToolbarItems.Add(salvarseccion);
        }

        private void Salvarseccion_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            SaveSection();
        }

        public void SaveSection()
        {
            OnRespuestaAuditorComplete -= CalificacionProductoPage_OnRespuestaAuditorComplete;
            OnRespuestaAuditorComplete += CalificacionProductoPage_OnRespuestaAuditorComplete;
            RespuestasAuditorProcess();
        }

        protected override bool OnBackButtonPressed()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalioDeSeccionSinGuardar, "OnBackButtonPressed", "CalificacionProductoPage", $"Salió de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} sin guardar respuestas");
            Navigation.PopAsync();
            return true;
        }

        private async void Init()
        {
            int IdRuta = App.CurrentRoute.IdRuta;
            int IdSeccion = App.CurrentSection.IdSeccion;
            // verificamos que la seccion actual no sea null
            if (App.CurrentSection == null) return;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoASeccion, "Init", "CalificacionProductoPage", $"Ingresó a la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
            // obtenemos todas las preguntas de esta seccion [_seccion]
            _models = PreguntaHelper.GetPreguntas();
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_models.Count == 0)
            {
                await DisplayAlert("PSM 360", "No hay preguntas para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
                return;
            }
            // verificamos que las presentaciones no sean nullas
            if (_presentaciones != null)
            {
                var presentacionpregunta = new List<PreguntaModel>();
                foreach (var presentacion in _presentaciones)
                {
                    foreach (var model in _models)
                    {
                        var preguntamodel = new PreguntaModel
                        {
                            Id = presentacion.IdProducto,
                            IdPregunta = model.IdPregunta,
                            PreguntaPadre = model.PreguntaPadre,
                            PreguntasHijas = model.PreguntasHijas,
                            IdSeccion = model.IdSeccion,
                            IdRespuesta = model.IdRespuesta,
                            Desencadenada = model.Desencadenada,
                            File = model.File,
                            FileName = model.FileName,
                            Respuesta = model.Respuesta,
                            Respuestas = model.Respuestas,
                            View = model.View,
                            IdControl = model.IdControl
                        };

                        RespuestaAuditor respuestaauditor = null;

                        //Este tipo de seccion tiene solo tipoRespuesta o Contro RadioButton es decir idcontrol = 3
                        if (model.IdControl == 3)
                        {
                            var Respuestas = model.Respuestas.Where(r => r.IdPregunta == preguntamodel.IdPregunta).Select(r => r.IdRespuesta).ToList();

                            foreach (var item in Respuestas)
                            {
                                var respuesta = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdRuta == IdRuta && ra.IdSeccion == IdSeccion && ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);

                                if (respuesta != null)
                                {
                                    respuestaauditor = respuesta;
                                    break;
                                }

                            }
                        }

                        if (respuestaauditor != null)
                        {
                            EditAnswers = true;
                            preguntamodel.Respuesta = respuestaauditor.Respuesta;
                            preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                            preguntamodel.IdRespuestaAuditor = respuestaauditor.IdAnswerAudit;
                        }
                        presentacionpregunta.Add(preguntamodel);
                    }
                }
                _models = presentacionpregunta;
                // asignamos las preguntas a los empaques
                SetPreguntas(_models);
            }
            else
            {
                await DisplayAlert("PSM 360", "No hay presentaciones para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
            }
        }

        private async void CalificacionProductoPage_OnRespuestaAuditorComplete(object sender, RespuestaAuditorComplete e)
        {
            if (!e.IsSuccess)
            {
                await DisplayAlert("Info", e.Message, "Aceptar");
            }
            else
            {
                bool respuestas = false;
                try
                {
                    var dateofregister = DateTime.Now;
                    if (e.Edit)
                    {
                        foreach (var respuesta in e.Respuestas)
                        {
                            respuesta.Fecha = dateofregister;
                            //App.DB.Entry(respuesta).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                        respuestas = true;
                    }
                    else
                    {
                        foreach (var respuesta in e.Respuestas)
                        {
                            respuesta.Fecha = dateofregister;
                            App.DB.RespuestaAuditor.Add(respuesta);
                        }
                        respuestas = App.DB.SaveChanges();
                    }

                    if (respuestas)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "CalificacionProductoPage_OnRespuestaAuditorComplete", "CalificacionProductoPage", $"Se guardaron las respuestas de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
                        if (!_hallazgo)
                        {
                            await DisplayAlert("PSM 360", "Se han guardado las respuestas", "Aceptar");

                            var seccionL = MainViewModel.GetInstance().Modulo.GetSeccionList();
                            MainViewModel.GetInstance().Modulo.ListSecciones = seccionL;

                            var seccionobligatoriofaltaL = MainViewModel.GetInstance().Modulo.GetSeccionListFaltante();
                            MainViewModel.GetInstance().Modulo.ListSeccionesFaltantes = seccionobligatoriofaltaL;

                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = true
                            });
                        }
                    }
                    else
                    {
                        if (!_hallazgo)
                        {
                            await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = false
                            });
                        }
                    }
                }
                catch
                {
                    await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                    if (!_hallazgo)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        OnSavePage(new SavePage
                        {
                            Page = this,
                            Success = false
                        });
                    }
                }
            }
        }

        private void RespuestasAuditorProcess()
        {
            List<RespuestaAuditor> respuestas = new List<RespuestaAuditor>();
            List<Foto> photos = new List<Foto>();
            bool notcomplete = false;
            int location = App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion : 0;
            foreach (var model in _models)
            {
                //Este tipo de seccion tiene solo tipoRespuesta o Contro RadioButton es decir idcontrol = 3
                if (model.PreguntaPadre.IdControl == 3)
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        if (string.IsNullOrEmpty(model.Respuesta))
                        {
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Ingresa una respuesta en la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            notcomplete = true;
                            btnsave.IsEnabled = true;
                            break;
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = App.DB.RespuestaAuditor.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.IdRespuesta = model.IdRespuesta;
                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        RespuestaAuditor answer = new RespuestaAuditor
                        {
                            IdProducto = model.Id,
                            IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                            IdRespuesta = model.IdRespuesta,
                            IdRuta = App.CurrentRoute.IdRuta,
                            IdSeccion = model.IdSeccion,
                            IdUbicacion = location,
                            NumerodeCaja = 0, // falta ese elemento
                            Respuesta = model.Respuesta,
                            Foto = model.Foto,
                            FotoNombre = model.FotoNombre
                        };
                        respuestas.Add(answer);
                    }
                }
            }

            if (!notcomplete)
            {
                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                {
                    IsSuccess = true,
                    Edit = EditAnswers,
                    Respuestas = respuestas
                });
            }
        }

        private event EventHandler<RespuestaAuditorComplete> OnRespuestaAuditorComplete;

        private void OnRespuestaAuditorCompleted(RespuestaAuditorComplete answer)
        {
            if (OnRespuestaAuditorComplete != null)
            {
                OnRespuestaAuditorComplete(this, answer);
            }
        }

        private void SetPreguntas(List<PreguntaModel> models)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RootLayout.Children.Clear();
                foreach (var model in _models)
                {
                    var presentacion = _presentaciones.FirstOrDefault(p => p.IdProducto == model.Id);
                    if (presentacion != null)
                    {
                        Label label = new Label
                        {
                            Text = presentacion.Nombre
                        };
                        RootLayout.Children.Add(label);

                        var pregunta = model.PreguntaPadre;
                        Label labelpregunta = new Label
                        {
                            Text = pregunta.Descripcion,
                            TextColor = Color.Black
                        };

                        RootLayout.Children.Add(labelpregunta);
                        switch (pregunta.IdControl)
                        {
                            case 3:
                                var respuestascombo = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.IdPregunta).ToList();

                                //int indexdefault = 0;
                                for (var o = 0; o < respuestascombo.Count; o++)
                                {
                                    var respuesta = respuestascombo[o];
                                    Button button = new Button
                                    {
                                        Text = respuesta.TextoOpcion
                                    };

                                    button.ClassId = model.GetHashCode().ToString();
                                    respuesta.ClassId = int.Parse(button.ClassId);
                                    button.Clicked += Button_SelectedIndexChanged;
                                    RootLayout.Children.Add(label);
                                    RootLayout.Children.Add(button);
                                    if (respuesta.TextoOpcion == model.Respuesta)
                                    {
                                        button.BackgroundColor = Color.Accent;
                                    }


                                }

                                //// obtenemos el index de la respuesta actual
                                //var selected = picker.Items.IndexOf(model.Respuesta);
                                //// si el picker tiene más de un elemento, asignamos el index al primer elemento
                                //if (picker.Items.Count > 0) { picker.SelectedIndex = 0; }
                                //// si existe un index para la respuesta actual [post-render]
                                //if (selected > -1)
                                //{
                                //    // asignamos la respuesta previa del usuario al picker
                                //    picker.SelectedIndex = selected;
                                //}
                                //else
                                //{
                                //    // en caso contrario, de no existir un index de respuesta anterior, verificamos
                                //    // si existe un default para la respuesta
                                //    if (answerdefault != null)
                                //    {
                                //        // asignamos el index por default para la respuesta
                                //        picker.SelectedIndex = indexdefault;
                                //    }
                                //}


                                break;
                        }
                    }
                }
            });
        }

        private void Button_SelectedIndexChanged(object sender, EventArgs e)
        {
            var button = sender as Button;

            if (!string.IsNullOrEmpty(button.ClassId))
            {
                int hashcode = int.Parse(button.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                var respuesta = button.Text;
                model.Respuesta = respuesta;
                var indexofmodel = _models.IndexOf(model);
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta && r.IdPregunta == model.IdPregunta);

                foreach (var val in _models)
                {
                    if (_models.IndexOf(val) == indexofmodel)
                    {
                        var buttons = RootLayout.Children.Where(s => s.ClassId == button.ClassId);
                        foreach (var btn in buttons)
                        {
                            var b = btn as Button;
                            b.BackgroundColor = Color.Default;
                        }

                        button.BackgroundColor = Color.Accent;
                    }
                }




                if (respuestaindb != null)
                {
                    model.IdRespuesta = respuestaindb.IdRespuesta;
                    if (indexofmodel > -1)
                    {
                        if (respuestaindb.Desencadena)
                        {
                            if (model.PreguntaPadre.TieneHijos)
                            {
                                foreach (var hijo in model.PreguntasHijas)
                                {
                                    hijo.Id = model.Id;
                                }
                                model.Desencadenada = true;
                                _models.InsertRange(indexofmodel + 1, model.PreguntasHijas);
                                SetPreguntas(_models);
                            }
                        }
                        else if (model.Desencadenada)
                        {
                            model.Desencadenada = false;
                            foreach (var preguntahija in model.PreguntasHijas)
                            {
                                if (preguntahija.Desencadenada)
                                {
                                    var pickerhija = preguntahija.View as Picker;
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                            _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                            SetPreguntas(_models);
                        }
                    }
                }
                else
                {
                    if (model.Desencadenada)
                    {
                        model.Desencadenada = false;
                        foreach (var preguntahija in model.PreguntasHijas)
                        {
                            if (preguntahija.Desencadenada)
                            {
                                var pickerhija = preguntahija.View as Picker;
                                pickerhija.SelectedIndex = 0;
                            }
                        }
                        _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                        SetPreguntas(_models);
                    }
                }
            }
        }

        private void Sview_OnChanged(object sender, ToggledEventArgs e)
        {
            var switchcell = sender as SwitchCell;
            int idrespuesta = int.Parse(switchcell.ClassId);
            var respuesta = App.DB.Respuesta.FirstOrDefault(r => r.IdRespuesta == idrespuesta);
            var model = _models.FirstOrDefault(m => m.IdPregunta == respuesta.IdPregunta);
            respuesta.Valor = e.Value ? 1 : 0;
            if (model.Respuestas != null)
            {
                var respuestaenmodel = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                if (respuestaenmodel != null)
                {
                    model.Respuestas.Remove(respuestaenmodel);
                }
                model.Respuestas.Add(respuesta.To<Respuesta>());
            }
            else
            {
                model.Respuestas = new List<Respuesta> { respuesta.To<Respuesta>() };
            }
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                var obj = sender as Entry;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = obj.Text;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Currency_EntryCurrencyTextChanged(object sender, EntryCurrencyTextChanged e)
        {
            if (sender != null)
            {
                var obj = sender as EntryCurrency;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = e.Currency;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void BtnSaveAnswers_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;
            btnsave.IsEnabled = false;
            SaveSection();
        }
    }
    //[XamlCompilation(XamlCompilationOptions.Compile)]
    //public partial class CalificacionProductoPage : ContentPage
    //{
    //    public CalificacionProductoPage()
    //    {
    //        InitializeComponent();
    //    }
    //}
}