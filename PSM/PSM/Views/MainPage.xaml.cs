﻿using PSM.Views;
using System;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : Xamarin.Forms.Shell
    {
        public MainPage()
        {
            InitializeComponent();
            BuildDynamicTabs();
        }

        private void BuildDynamicTabs()
        {
            if (App.DB.MenuUser.Any())
            {
                var menu = App.DB.MenuUser.OrderBy(m => m.Orden).ToList();
                foreach (var menuItem in menu)
                {
                    Type page = Type.GetType("PSM.Views." + menuItem.TargetType + ",PSM");
                    
                    if (page != null)
                    {
                        Page contentPage = (Page)Activator.CreateInstance(page);
                        tbElements.Items.Add(new ShellContent() { Icon = menuItem.Icon, Title = menuItem.Descripcion, Content = contentPage }); 
                    }
                    else if (!string.IsNullOrEmpty(menuItem.TargetType))
                    {
                        if (menuItem.TargetType.Equals("MainPage"))
                        {
                            page = Type.GetType("PSM.Views." + menuItem.TargetType.Replace("Main", "Aviso") + ",PSM");

                            if (page != null)
                            {
                                Page contentPage = (Page)Activator.CreateInstance(page);
                                tbElements.Items.Add(new ShellContent() { Icon = menuItem.Icon, Title = menuItem.Descripcion, Content = contentPage });
                            }
                        }
                        if (menuItem.TargetType.Equals("DownloadPage"))
                        {
                            page = Type.GetType("PSM.Views." + menuItem.TargetType.Replace("Download", "Descarga") + ",PSM");

                            if (page != null)
                            {
                                Page contentPage = (Page)Activator.CreateInstance(page);
                                tbElements.Items.Add(new ShellContent() { Icon = menuItem.Icon, Title = menuItem.Descripcion, Content = contentPage });
                            }
                        }
                        else if (menuItem.TargetType.Equals("Tienda"))
                        {
                            page = Type.GetType("PSM.Views." + menuItem.TargetType + "Page,PSM");

                            if (page != null)
                            {
                                Page contentPage = (Page)Activator.CreateInstance(page);
                                tbElements.Items.Add(new ShellContent() { Icon = menuItem.Icon, Title = menuItem.Descripcion, Content = contentPage });
                            }
                        }
                        else if (menuItem.TargetType.Equals("UploadPage"))
                        {
                            page = Type.GetType("PSM.Views." + menuItem.TargetType.Replace("Upload", "RutaEstatus") + ",PSM");

                            if (page != null)
                            {
                                Page contentPage = (Page)Activator.CreateInstance(page);
                                tbElements.Items.Add(new ShellContent() { Icon = "upload.png", Title = "Estatus Ruta", Content = contentPage });
                            }
                        }
                    }
                }
            }

            //Se agrega la pantalla de Perfil aunque no la incluya
            tbElements.Items.Add(new ShellContent() { Icon = "profile_icon.png", Title = "Perfil", Content = new PerfilPage() });
        }

    }
}
