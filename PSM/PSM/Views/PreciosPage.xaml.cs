﻿using Cosmic;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using PSM.Controls;
using PSM.DeviceSensors;
using PSM.Function;
using PSM.Helpers;
using PSM.Helpers.Subcontrols;
using PSM.Models;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreciosPage : ContentPage
    {
        //public PreciosPage()
        //{
        //    InitializeComponent();
        //}
        public static event EventHandler<SavePage> SavePage;
        public bool EditAnswers { get; set; }

        Button btnsave { get; set; }

        public static void OnSavePage(SavePage page)
        {
            if (SavePage != null)
            {
                SavePage.Invoke(null, page);
            }
        }

        /// <summary>
        /// Lista de presentaciones o empaques
        /// </summary>
        private List<Producto> _presentaciones;

        /// <summary>
        /// Lista de preguntas
        /// </summary>
        private List<PreguntaModel> __models;
        public List<PreguntaModel> _models
        {
            get { return __models; }
            set { __models = value; }
        }

        public bool _hallazgo { get; set; }
        private bool _promo { get; set; }
        //  HallazgoPackageViewModel
        public PreciosPage(List<Producto> presentaciones, HallazgoPackageModel hallazomodel = null, bool promo = false)
        {
            InitializeComponent();
            lblTituloSeccion.Text = App.CurrentSection.Descripcion;
            App.CurrentPage = this;
            _presentaciones = presentaciones;
            _hallazgo = hallazomodel != null;
            _promo = promo;

            if (_hallazgo)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    GridHallazgo.IsVisible = true;
                });
                hallazomodel.Page = this;
                BindingContext = hallazomodel;
                SaveAnswers.IsVisible = false;
            }
            else
            {
                // StartToolBar();
            }

            Init();

            var prueba = _models;
        }

        private void StartToolBar()
        {
            ToolbarItem salvarseccion = new ToolbarItem
            {
                Icon = "diskette.png",
                Priority = 0,
                Order = ToolbarItemOrder.Primary,
                Text = "Salvar respuestas"
            };
            salvarseccion.Clicked += Salvarseccion_Clicked;
            ToolbarItems.Add(salvarseccion);
        }

        private void Salvarseccion_Clicked(object sender, EventArgs e)
        {
            ValidateDevice.Validate("Salvarseccion_Clicked", "PreciosPage");
            SaveSection();
        }

        public void SaveSection()
        {
            OnRespuestaAuditorComplete -= PreciosPage_OnRespuestaAuditorComplete;
            OnRespuestaAuditorComplete += PreciosPage_OnRespuestaAuditorComplete;
            RespuestasAuditorProcess();
        }

        protected override bool OnBackButtonPressed()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalioDeSeccionSinGuardar, "OnBackButtonPressed", "PreciosPage", $"Salió de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} sin guardar respuestas");
            Navigation.PopAsync();
            return true;
        }

        int numerocaja = 0;
        private async void Init()
        {
            int IdRuta = App.CurrentRoute.IdRuta;
            int IdSeccion = App.CurrentSection.IdSeccion;

            int? max = App.DB.RespuestaAuditor.Where(e => e.IdSeccion == App.CurrentSection.IdSeccion && e.IdRuta == App.CurrentRoute.IdRuta).Max(e => e.NumerodeCaja);
            numerocaja = max.HasValue ? max.Value + 1 : 0;
            App.CycleNumber = numerocaja;

            // verificamos que la seccion actual no sea null
            if (App.CurrentSection == null) return;
            // obtenemos todas las preguntas de esta seccion [_seccion]
            _models = PreguntaHelperDex.GetPreguntas();
            // si la sección no tiene preguntas, regresamos a la pagina anterior
            if (_models.Count == 0)
            {
                await DisplayAlert("PSM 360", "No hay preguntas para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
                return;
            }
            // verificamos que las presentaciones no sean nullas
            if (_presentaciones != null)
            {
                var presentacionpregunta = new List<PreguntaModel>();

                var indice = 1;
                var respuestaAuditor = App.DB.RespuestaAuditor.Where(s => s.Sincronizado == false && s.IdSeccion == IdSeccion && s.IdRuta == IdRuta).ToList();


                //Bandera que designa si el cuestionario es para productos o no
                //typepage = 0 No es tipo producto, typepage = 1 Es tipo producto.
                bool typepage = true;
                foreach (var presentacion in _presentaciones.OrderBy(p => p.Orden))
                {

                    var model = _models.To<List<PreguntaModel>>();
                    PreguntaHelperDex.GetRespuestasPregunta(model, respuestaAuditor, presentacionpregunta, presentacion, typepage, indice);

                    indice = presentacionpregunta.LastOrDefault().Indice;

                    indice++;
                    #region
                    //foreach (var model in _models)
                    //{
                    //    PreguntaModel pm = model.To<PreguntaModel>();
                    //    var preguntamodel = new PreguntaModel
                    //    {
                    //        Id = presentacion.IdProducto,
                    //        IdPregunta = pm.IdPregunta,
                    //        PreguntaPadre = pm.PreguntaPadre,
                    //        PreguntasHijas = pm.PreguntasHijas,
                    //        IdSeccion = pm.IdSeccion,
                    //        IdRespuesta = pm.IdRespuesta,
                    //        Desencadenada = pm.Desencadenada,
                    //        File = pm.File,
                    //        FileName = pm.FileName,
                    //        Respuesta = pm.Respuesta,
                    //        Respuestas = pm.Respuestas.To<List<Respuesta>>(),
                    //        View = pm.View,
                    //        IdControl = pm.IdControl
                    //    };

                    //    RespuestaAuditor respuestaauditor = null;
                    //    List<RespuestaAuditor> respuestaauditorlist = new List<RespuestaAuditor>();

                    //    if (model.IdControl == 1 || model.IdControl == 4 || model.IdControl == 5 || model.IdControl == 7)
                    //    {
                    //        respuestaauditor = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == preguntamodel.IdRespuesta && ra.IdProducto == preguntamodel.Id);
                    //    }
                    //    else if (model.IdControl == 2 || model.IdControl == 3)
                    //    {
                    //        var Respuestas = model.Respuestas.Where(r => r.IdPregunta == preguntamodel.IdPregunta).Select(r => r.IdRespuesta).ToList();

                    //        foreach (var item in Respuestas)
                    //        {
                    //            var respuestaAudit = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);

                    //            if (respuestaAudit != null)
                    //            {
                    //                if (model.IdControl == 2)
                    //                {
                    //                    var respuesta = respuestaAuditor.Where(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id).ToList();
                    //                    respuestaauditorlist.AddRange(respuesta);
                    //                }
                    //                else
                    //                {
                    //                    var respuesta = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);
                    //                    if (respuesta != null)
                    //                    {
                    //                        respuestaauditor = respuesta;
                    //                        break;
                    //                    }
                    //                }
                    //            }
                    //        }
                    //    }

                    //    //Este caso aplica para las repuestas tipo checkbox ya que se pueden tener varias respuestas para una misma pregunta
                    //    if (respuestaauditorlist.Count() != 0)
                    //    {
                    //        EditAnswers = true;
                    //        //preguntamodel.Respuesta = respuestaauditor.Respuesta;
                    //        foreach (var item in preguntamodel.Respuestas)
                    //        {
                    //            var b = respuestaauditorlist.FirstOrDefault(s => s.IdRespuesta == item.IdRespuesta);
                    //            if (b != null)
                    //                item.Valor = double.Parse(b.Respuesta);

                    //            if (b != null)
                    //            {
                    //                item.IdRespuestaAuditor = b.IdAnswerAudit;
                    //            }
                    //        }
                    //        //preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                    //        /*preguntamodel.IdRespuestaAuditor = b*/;
                    //    }

                    //    if (respuestaauditor != null)
                    //    {
                    //        EditAnswers = true;
                    //        preguntamodel.Respuesta = respuestaauditor.Respuesta;
                    //        preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                    //        preguntamodel.IdRespuestaAuditor = respuestaauditor.IdAnswerAudit;

                    //    }
                    //    presentacionpregunta.Add(preguntamodel);
                    //}
                    #endregion
                }
                _models = presentacionpregunta;
                // asignamos las preguntas a los empaques
                //SetPreguntas(_models);
                await Default(_models);
            }
            else
            {
                await DisplayAlert("PSM 360", "No hay presentaciones para esta sección", "Aceptar");
                if (!_hallazgo)
                {
                    await Navigation.PopAsync();
                }
            }
        }

        private async Task Default(List<PreguntaModel> models)
        {
            try
            {
                int contador = models.Count();
                var modelsstatic = models.To<List<PreguntaModel>>();

                for (int i = 0; i < contador; i++)
                {
                    //var model = models[i];
                    var modelstatic = modelsstatic[i];
                    var modelselect = models.FirstOrDefault(d => d.IdPregunta == modelstatic.IdPregunta);
                    {
                        var child = new PreguntaModelSub(modelstatic, this, _promo, _presentaciones);
                        RootLayout.Children.Add(child);
                    }
                }
                //foreach (var model in models)
                //{
                //    var child = new PreguntaModelSub(model, this);
                //    RootLayout.Children.Add(child);
                //}
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        private async void PreciosPage_OnRespuestaAuditorComplete(object sender, RespuestaAuditorComplete e)
        {
            if (!e.IsSuccess)
            {
                await DisplayAlert("Info", e.Message, "Aceptar");
            }
            else
            {
                bool respuestas = false;
                try
                {
                    var dateofregister = DateTime.Now;
                    //if (e.Edit)
                    //{
                    //    foreach (var respuesta in e.Respuestas)
                    //    {
                    //        respuesta.Fecha = dateofregister;
                    //        App.DB.Entry(respuesta).State = DevelopersAzteca.Storage.SQLite.DataBase.EntryState.Modify;
                    //        App.DB.SaveChanges();
                    //    }
                    //    respuestas = true;
                    //}
                    //else
                    //{
                    //    foreach (var respuesta in e.Respuestas)
                    //    {
                    //        respuesta.Fecha = dateofregister;
                    //        App.DB.RespuestaAuditor.Add(respuesta);
                    //    }
                    //    respuestas = App.DB.SaveChanges();
                    //}

                    //App.DB.Execute("Delete from RespuestaAuditor Where IdSeccion =" + App.CurrentSection.IdSeccion + " and IdRuta =" + App.CurrentRoute.IdRuta + " and IdProducto =" + e.Respuestas.First().IdProducto);
                    foreach (var respuesta in e.Respuestas)
                    {
                        App.DB.Execute("Delete from RespuestaAuditor Where IdSeccion =" + App.CurrentSection.IdSeccion + " and IdRuta =" + App.CurrentRoute.IdRuta + " and IdProducto =" + respuesta.IdProducto);
                        respuesta.Fecha = dateofregister;
                        App.DB.RespuestaAuditor.Add(respuesta);
                    }
                    respuestas = App.DB.SaveChanges();


                    if (respuestas)
                    {
                        if (!_hallazgo)
                        {
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.GuardoRespuestas, "PreciosPage_OnRespuestaAuditorComplete", "PreciosPage", $"Se guardaron las respuestas de la sección: {App.CurrentSection.IdSeccion} - {App.CurrentSection.Descripcion} ");
                            await DisplayAlert("PSM 360", "Se han guardado las respuestas", "Aceptar");

                            var seccionL = MainViewModel.GetInstance().Modulo.GetSeccionList();
                            MainViewModel.GetInstance().Modulo.ListSecciones = seccionL;

                            var seccionobligatoriofaltaL = MainViewModel.GetInstance().Modulo.GetSeccionListFaltante();
                            MainViewModel.GetInstance().Modulo.ListSeccionesFaltantes = seccionobligatoriofaltaL;


                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = true
                            });
                        }
                    }
                    else
                    {
                        if (!_hallazgo)
                        {
                            await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            OnSavePage(new SavePage
                            {
                                Page = this,
                                Success = false
                            });
                        }
                    }

                }
                catch (Exception ex)
                {
                    await DisplayAlert("PSM 360", "Ocurrio un error al guardar las respuestas, intenta más tarde", "Aceptar");
                    if (!_hallazgo)
                    {
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        OnSavePage(new SavePage
                        {
                            Page = this,
                            Success = false
                        });
                    }
                }
            }
        }

        private void RespuestasAuditorProcess()
        {
            int IdRuta = App.CurrentRoute.IdRuta;
            int IdSeccion = App.CurrentSection.IdSeccion;
            var respAudit = App.DB.RespuestaAuditor.Where(s => s.IdRuta == IdRuta && s.IdSeccion == IdSeccion && s.Sincronizado == false);

            List<RespuestaAuditor> respuestas = new List<RespuestaAuditor>();
            List<Foto> photos = new List<Foto>();
            bool notcomplete = false;
            int location = App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion : 0;
            foreach (var model in _models)
            {
                if (model.PreguntaPadre.IdControl == 1 || model.PreguntaPadre.IdControl == 4 || model.PreguntaPadre.IdControl == 5 || model.PreguntaPadre.IdControl == 7 || model.PreguntaPadre.IdControl == 3)
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        if (string.IsNullOrEmpty(model.Respuesta))
                        {
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Ingresa una respuesta en la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            notcomplete = true;
                            if (btnsave != null)
                            {
                                btnsave.IsEnabled = true;
                            }
                            break;
                        }
                    }

                    if (string.IsNullOrEmpty(model.Respuesta))
                    {
                        continue;
                    }
                    else
                    {
                        if ((model.PreguntaPadre.IdControl == 5 || model.PreguntaPadre.IdControl == 7) &&
                            (model.Respuesta.Trim().Equals(".") || model.Respuesta.Trim().Equals("-") || model.Respuesta.Trim().Equals(",")))
                        {
                            if ((model.Respuesta.Trim().Equals(".")) || (model.Respuesta.Trim().Equals(",")) || (model.Respuesta.Trim().Equals("-")))
                            {
                                // lanzamos el evento de que se ha terminado de procesar respuestasauditor pero con un estado false
                                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                                {
                                    IsSuccess = false,
                                    Message = "Ingresa una cantidad en la pregunta " + model.PreguntaPadre.Descripcion
                                });
                                // la seccion no se ha completado
                                notcomplete = true;
                                if (btnsave != null)
                                {
                                    btnsave.IsEnabled = true;
                                }
                                // rompemos el loop
                                break;
                            }
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.IdRespuesta = model.IdRespuesta;
                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        if (model.IdRespuesta != 0 || !string.IsNullOrEmpty(model.Respuesta))
                        {
                            RespuestaAuditor answer = new RespuestaAuditor
                            {
                                IdProducto = model.Id,
                                IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                                IdRespuesta = model.IdRespuesta,
                                IdRuta = App.CurrentRoute.IdRuta,
                                IdSeccion = model.IdSeccion,
                                IdUbicacion = location,
                                NumerodeCaja = numerocaja,
                                Respuesta = model.Respuesta,
                                Foto = model.Foto,
                                FotoNombre = model.FotoNombre
                            };
                            respuestas.Add(answer);
                        }
                    }
                }
                else if (model.PreguntaPadre.IdControl == 2)
                {
                    foreach (var item in model.Respuestas)
                    {
                        if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                        {
                            var r = model.Respuestas.FirstOrDefault(s => s.Valor == 1);
                            if (r == null)
                            {
                                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                                {
                                    IsSuccess = false,
                                    Message = "Ingresa una respuesta en la pregunta " + (_models.IndexOf(model) + 1) + " " + model.PreguntaPadre.Descripcion
                                });
                                notcomplete = true;
                                if (btnsave != null)
                                {
                                    btnsave.IsEnabled = true;
                                }
                                break;
                            }
                        }


                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == item.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            respuestaauditorindb.IdRespuesta = item.IdRespuesta;
                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            respuestaauditorindb.Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0";
                            respuestas.Add(respuestaauditorindb);
                        }
                        else
                        {
                            RespuestaAuditor answer = new RespuestaAuditor
                            {
                                IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                                IdRespuesta = item.IdRespuesta,
                                IdRuta = App.CurrentRoute.IdRuta,
                                IdSeccion = model.IdSeccion,
                                IdUbicacion = location, // falta este elemtno
                                NumerodeCaja = numerocaja,
                                Respuesta = item.Valor.HasValue && item.Valor.Value == 1 ? "1" : "0",
                                IdProducto = model.Id
                            };
                            respuestas.Add(answer);
                        }
                    }
                }
                else if (model.PreguntaPadre.IdControl == 6)
                {
                    if (model.PreguntaPadre.SeValida.HasValue && model.PreguntaPadre.SeValida.Value)
                    {
                        if (model.File == null)
                        {
                            OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                            {
                                IsSuccess = false,
                                Message = "Toma la foto para la pregunta " + model.PreguntaPadre.Descripcion
                            });
                            notcomplete = true;
                            if (btnsave != null)
                            {
                                btnsave.IsEnabled = true;
                            }
                            break;
                        }
                    }

                    if (model.IdRespuestaAuditor != null)
                    {
                        var respuestaauditorindb = respAudit.FirstOrDefault(ra => ra.IdAnswerAudit == model.IdRespuestaAuditor);
                        if (respuestaauditorindb != null)
                        {
                            //Si model FotoNombre es null quiere decir que solo se entro a la seccion sin modificar la foto por lo que se trae el nombre de la foto guardara originalmente
                            //si existe FotoNombre quiere decir que es nueva la foto 
                            if (string.IsNullOrEmpty(model.FotoNombre))
                                respuestaauditorindb.FotoNombre = model.FileName;
                            else
                                respuestaauditorindb.FotoNombre = model.FotoNombre;


                            if (model.Foto == null)
                                respuestaauditorindb.Foto = model.FotoEdit;
                            else
                                respuestaauditorindb.Foto = model.Foto;

                            respuestaauditorindb.Respuesta = model.Respuesta;
                            respuestaauditorindb.NumerodeCaja = numerocaja;
                            respuestas.Add(respuestaauditorindb);
                        }
                    }
                    else
                    {
                        RespuestaAuditor respuestaAuditor = new RespuestaAuditor
                        {
                            IdProyectoTienda = App.CurrentRoute.IdProyectoTienda,
                            IdRespuesta = model.IdRespuesta,
                            IdRuta = App.CurrentRoute.IdRuta,
                            IdSeccion = model.IdSeccion,
                            IdUbicacion = location,
                            NumerodeCaja = numerocaja,
                            Respuesta = model.Respuesta,
                            Foto = model.Foto,
                            FotoNombre = model.FotoNombre,
                            IdProducto = model.Id
                        };
                        respuestas.Add(respuestaAuditor);
                    }
                }
            }

            if (!notcomplete)
            {
                OnRespuestaAuditorCompleted(new RespuestaAuditorComplete
                {
                    IsSuccess = true,
                    Edit = EditAnswers,
                    Respuestas = respuestas
                });
            }
        }

        private event EventHandler<RespuestaAuditorComplete> OnRespuestaAuditorComplete;

        private void OnRespuestaAuditorCompleted(RespuestaAuditorComplete answer)
        {
            if (OnRespuestaAuditorComplete != null)
            {
                OnRespuestaAuditorComplete(this, answer);
            }
        }

        private void SetPreguntas(List<PreguntaModel> models)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                RootLayout.Children.Clear();
                foreach (var model in _models)
                {
                    var presentacion = _presentaciones.FirstOrDefault(p => p.IdProducto == model.Id);
                    if (presentacion != null)
                    {
                        Label label = new Label
                        {
                            Text = presentacion.Nombre
                        };
                        StackLayout stackLayout = new StackLayout();
                        Frame frame = new Frame
                        {
                            BackgroundColor = Color.FromHex("#F2F2F2"),
                            CornerRadius = 15f,
                            HasShadow = false
                        };
                        stackLayout.BackgroundColor = Color.FromHex("#F2F2F2");
                        stackLayout.Children.Add(label);

                        var pregunta = model.PreguntaPadre;
                        Label labelpregunta = new Label
                        {
                            Text = (_models.IndexOf(model) + 1) + ".- " + pregunta.Descripcion,
                            TextColor = Color.Black
                        };

                        stackLayout.Children.Add(labelpregunta);
                        switch (pregunta.IdControl)
                        {
                            case 1:
                            case 5:
                                Entry entry = new Entry
                                {
                                    Placeholder = "Ingresa tu respuesta"
                                };
                                if (pregunta.IdControl != 1)
                                {
                                    entry.Keyboard = Keyboard.Numeric;
                                }
                                entry.ClassId = model.GetHashCode().ToString();
                                entry.TextChanged += Entry_TextChanged;
                                model.View = entry;

                                // Asignamos los datos de respuesta realizados antes de cualquier cambio en la UI
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    entry.Text = model.Respuesta;
                                }
                                stackLayout.Children.Add(label);
                                stackLayout.Children.Add(entry);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);
                                break;
                            case 7:
                                EntryCurrency currency = new EntryCurrency();
                                currency.ClassId = model.GetHashCode().ToString();
                                currency.EntryCurrencyTextChanged += Currency_EntryCurrencyTextChanged;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    currency.Text = model.Respuesta.TextToMoney();
                                }
                                else
                                {
                                    if (_promo)
                                    {
                                        if (presentacion.Precio.HasValue)
                                        {
                                            var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.Precio.Value);
                                            currency.Text = textmoney.TextToMoney();
                                            model.Respuesta = currency.Text.MoneyToText();
                                        }
                                    }
                                }
                                model.View = currency;
                                stackLayout.Children.Add(currency);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);
                                break;

                            case 10:
                                EntryCurrency currencypromo = new EntryCurrency();
                                currencypromo.ClassId = model.GetHashCode().ToString();
                                currencypromo.EntryCurrencyTextChanged += Currency_EntryCurrencyTextChanged;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    currencypromo.Text = model.Respuesta.TextToMoney();
                                }
                                else
                                {
                                    if (_promo)
                                    {
                                        if (presentacion.PrecioPromo.HasValue)
                                        {
                                            var textmoney = string.Format(CultureInfo.CurrentCulture, "{0:F2}", presentacion.PrecioPromo.Value);
                                            currencypromo.Text = textmoney.TextToMoney();
                                            model.Respuesta = currencypromo.Text.MoneyToText();
                                        }
                                    }
                                }
                                model.View = currencypromo;
                                stackLayout.Children.Add(currencypromo);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);
                                break;
                            case 2:
                                var respuestascheck = App.DB.Respuesta.Where(e => e.IdPregunta == model.IdPregunta).ToList();
                                TableRoot root = new TableRoot();
                                TableView table = new TableView(root) { Intent = TableIntent.Form };
                                table.HeightRequest = respuestascheck.Count * 55;
                                TableSection section = new TableSection();
                                if (model.Respuestas == null)
                                {
                                    model.Respuestas = new List<Respuesta>();
                                }
                                foreach (var respuesta in respuestascheck)
                                {
                                    int respuestaint = 0;
                                    bool respuestabool = false;
                                    if (model.Respuestas != null)
                                    {
                                        // buscamos la respuesta previamente realizada
                                        var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                        // si la respuesta existe
                                        if (respuestafound != null)
                                        {
                                            // parseamos a boolean
                                            if (respuestafound.Valor.HasValue)
                                            {
                                                respuestaint = (int)respuestafound.Valor.Value;
                                            }
                                            respuestabool = respuestaint == 1;
                                        }
                                    }
                                    SwitchCell sview = new SwitchCell
                                    {
                                        Text = respuesta.TextoOpcion,
                                        On = respuestabool,
                                        ClassId = respuesta.IdRespuesta.ToString(),
                                        StyleId = model.Id.ToString()
                                    };
                                    sview.OnChanged += Sview_OnChanged;
                                    section.Add(sview);
                                    if (model.Respuestas != null)
                                    {
                                        var respuestafound = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                                        if (respuestafound != null)
                                        {
                                            //respuestafound.Valor = respuesta.Valor;
                                            //model.Respuestas.Remove(respuestafound);
                                        }
                                        //respuesta.Valor = respuestaint;
                                        //model.Respuestas.Add(respuesta);
                                    }
                                }
                                root.Add(section);
                                stackLayout.Children.Add(label);
                                stackLayout.Children.Add(table);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);
                                break;
                            case 3:
                                var respuestascombo = App.DB.Respuesta.Where(e => e.IdPregunta == pregunta.IdPregunta).ToList();
                                Picker picker = new Picker();
                                picker.ClassId = model.GetHashCode().ToString();

                                var answerdefault = respuestascombo.FirstOrDefault(e => e.ValorDefault.HasValue && e.ValorDefault.Value);
                                if (answerdefault != null)
                                {

                                }
                                else
                                {
                                    picker.Items.Add("-- Selecciona una opción --");
                                }

                                int indexdefault = 0;
                                for (var o = 0; o < respuestascombo.Count; o++)
                                {
                                    var respuesta = respuestascombo[o];
                                    if (answerdefault != null)
                                    {
                                        if (respuesta.IdRespuesta == answerdefault.IdRespuesta)
                                        {
                                            indexdefault = o;
                                        }
                                    }
                                    picker.Items.Add(respuesta.TextoOpcion);
                                }

                                // obtenemos el index de la respuesta actual
                                var selected = picker.Items.IndexOf(model.Respuesta);
                                // si el picker tiene más de un elemento, asignamos el index al primer elemento
                                if (picker.Items.Count > 0) { picker.SelectedIndex = 0; }
                                // si existe un index para la respuesta actual [post-render]
                                if (selected > -1)
                                {
                                    // asignamos la respuesta previa del usuario al picker
                                    picker.SelectedIndex = selected;

                                    //foreach (var item in pregunta.Respuestas)
                                    //{
                                    //    if (item.Desencadena && item.TextoOpcion == pregunta.Respuesta)
                                    //    {
                                    //        foreach (var item2 in pregunta.PreguntasHijas)
                                    //        {
                                    //            RenderSigleLayout(item2, stackHijo);
                                    //        }
                                    //    }

                                    //}
                                }
                                else
                                {
                                    // en caso contrario, de no existir un index de respuesta anterior, verificamos
                                    // si existe un default para la respuesta
                                    if (answerdefault != null)
                                    {
                                        // asignamos el index por default para la respuesta
                                        picker.SelectedIndex = indexdefault;
                                    }
                                }
                                picker.SelectedIndexChanged += Picker_SelectedIndexChanged;
                                model.View = picker;
                                RootLayout.Children.Add(label);
                                RootLayout.Children.Add(picker);
                                break;
                            case 4:
                                DatePicker datepicker = new DatePicker();
                                datepicker.ClassId = model.GetHashCode().ToString();

                                datepicker.Date = new DateTime(DateTime.Now.Year, 1, 1);
                                datepicker.DateSelected += Datepicker_DateSelected;
                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    datepicker.Date = DateTime.ParseExact(model.Respuesta, "yyyy-MM-dd", CultureInfo.CurrentCulture);
                                }
                                model.View = datepicker;
                                stackLayout.Children.Add(label);
                                stackLayout.Children.Add(datepicker);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);
                                break;
                            // foto
                            case 6:
                                Button btnfoto = new Button
                                {
                                    Text = "Tomar foto"
                                };
                                btnfoto.ClassId = model.GetHashCode().ToString();

                                btnfoto.Clicked += Btnfoto_Clicked;
                                stackLayout.Children.Add(label);
                                stackLayout.Children.Add(btnfoto);
                                frame.Content = stackLayout;
                                RootLayout.Children.Add(frame);

                                if (!string.IsNullOrEmpty(model.Respuesta))
                                {
                                    btnfoto.BackgroundColor = Color.Accent;
                                    btnfoto.TextColor = Color.White;
                                }
                                else if (!string.IsNullOrEmpty(model.FotoNombre))
                                {
                                    btnfoto.BackgroundColor = Color.Accent;
                                    btnfoto.TextColor = Color.White;
                                }
                                break;
                        }
                        /*
                        Button button = TakePicture(model);
                        if (button != null)
                        {
                            RootLayout.Children.Add(button);
                        }
                        */
                    }
                }
            });
        }

        private async void Btnfoto_Clicked(object sender, EventArgs e)
        {
            var btntakepicture = sender as Button;
            int hashcode = int.Parse(btntakepicture.ClassId);
            var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
            //var modelnumber = _models.IndexOf(model) + 1;
            if (model != null)
            {
                var idpregunta = model.PreguntaPadre.IdPregunta;
                var idproducto = model.Id;
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + idpregunta + "-" + idproducto + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                var result = await task.TakePhoto();
                if (!result.Success)
                {
                    await DisplayAlert("PSM 360", result.Message, "Aceptar");
                }
                else if (result.Photo != null)
                {
                    model.File = result;
                    btntakepicture.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Datepicker_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (sender != null)
            {
                var obj = sender as DatePicker;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = obj.Date.ToString("yyyy-MM-dd");
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Picker_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = sender as Picker;
            /*
            if (!string.IsNullOrEmpty(picker.ClassId))
            {
                int hashcode = int.Parse(picker.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                var respuesta = picker.Items[picker.SelectedIndex];
                if (model == null) return;
                model.Respuesta = respuesta;
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta);
                if (respuestaindb == null) return;
                model.IdRespuesta = respuestaindb.IdRespuesta;
            }
            */

            if (!string.IsNullOrEmpty(picker.ClassId))
            {
                int hashcode = int.Parse(picker.ClassId);

                //realizar busqueda por id producto

                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                //var model = _models.FirstOrDefault(m => m.Id == );
                var respuesta = picker.Items[picker.SelectedIndex];
                model.Respuesta = respuesta;
                var indexofmodel = _models.IndexOf(model);
                var respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.TextoOpcion == respuesta && r.IdPregunta == model.IdPregunta);
                if (respuestaindb != null)
                {
                    model.IdRespuesta = respuestaindb.IdRespuesta;
                    if (indexofmodel > -1)
                    {
                        if (respuestaindb.Desencadena)
                        {
                            if (model.PreguntaPadre.TieneHijos)
                            {
                                foreach (var hijo in model.PreguntasHijas)
                                {
                                    hijo.Id = model.Id;
                                }
                                model.Desencadenada = true;
                                _models.InsertRange(indexofmodel + 1, model.PreguntasHijas);
                                SetPreguntas(_models);
                            }
                        }
                        else if (model.Desencadenada)
                        {
                            model.Desencadenada = false;
                            foreach (var preguntahija in model.PreguntasHijas)
                            {
                                if (preguntahija.Desencadenada)
                                {
                                    var pickerhija = preguntahija.View as Picker;
                                    pickerhija.SelectedIndex = 0;
                                }
                            }
                            _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                            SetPreguntas(_models);
                        }
                    }
                }
                else
                {
                    if (model.Desencadenada)
                    {
                        model.Desencadenada = false;
                        foreach (var preguntahija in model.PreguntasHijas)
                        {
                            if (preguntahija.Desencadenada)
                            {
                                var pickerhija = preguntahija.View as Picker;
                                pickerhija.SelectedIndex = 0;
                            }
                        }
                        _models.RemoveRange(indexofmodel + 1, model.PreguntasHijas.Count);
                        SetPreguntas(_models);
                    }
                }
            }
        }

        private void Sview_OnChanged(object sender, ToggledEventArgs e)
        {
            var switchcell = sender as SwitchCell;
            int idrespuesta = int.Parse(switchcell.ClassId);
            int idproducto = int.Parse(switchcell.StyleId);
            var respuesta = App.DB.Respuesta.FirstOrDefault(r => r.IdRespuesta == idrespuesta);
            var model = _models.FirstOrDefault(m => m.IdPregunta == respuesta.IdPregunta && m.Id == idproducto);
            //respuesta.Valor = e.Value ? 1 : 0;
            int val = e.Value ? 1 : 0;
            if (model.Respuestas != null)
            {
                var respuestaenmodel = model.Respuestas.FirstOrDefault(r => r.IdRespuesta == respuesta.IdRespuesta);
                if (respuestaenmodel != null)
                {
                    //model.Respuestas.Remove(respuestaenmodel);
                    respuestaenmodel.Valor = val;
                }
                //model.Respuestas.Add(respuesta);
            }
            else
            {
                model.Respuestas = new List<Respuesta> { respuesta.To<Respuesta>() };
            }
        }

        /*
        private Button TakePicture(PreguntaModel model)
        {
            Button btntakepicture = null;
            if (model.PreguntaPadre.EsFotoRequerida && model.PreguntaPadre.IdControl != 6)
            {
                btntakepicture = new Button
                {
                    Text = "Tomar foto"
                };
                btntakepicture.ClassId = (model.Id + model.IdPregunta + model.IdRespuesta).ToString();
                btntakepicture.Clicked += Btntakepicture_Clicked;

                if (model.File != null)
                {
                    btntakepicture.BackgroundColor = Color.Accent;
                }
            }

            return btntakepicture;
        }
        */

        private async void Btntakepicture_Clicked(object sender, EventArgs e)
        {
            var btntakepicture = sender as Button;
            var modelatsender = JsonConvert.DeserializeObject<PreguntaModel>(btntakepicture.ClassId);
            var model = _models.FirstOrDefault(m => m.Equals(modelatsender));
            if (model != null)
            {
                var idpregunta = model.PreguntaPadre.IdPregunta;
                var filename = App.CurrentUser.Alias + "-" + (App.CurrentRoute.IdProyectoTienda == 0 ? "0" : App.CurrentRoute.IdProyectoTienda.ToString()) + "-" + model.IdSeccion + "-" + idpregunta + "-" + (App.CycleNumber == 0 ? "0" : App.CycleNumber.ToString()) + "-" + (App.CurrentPackage != null ? App.CurrentPackage.IdPresentacionEmpaque.ToString() : "0") + "-" + (App.CurrentLocation != null ? App.CurrentLocation.IdUbicacion.ToString() : "0") + "-" + App.CurrentRoute.IdRuta + ".jpg";
                CameraCaptureTask task = new CameraCaptureTask
                {
                    FileName = filename,
                    FolderName = "PSM360Photos"
                };
                var result = await task.TakePhoto();
                if (!result.Success)
                {
                    await DisplayAlert("PSM 360", result.Message, "Aceptar");
                }
                else if (result.Photo != null)
                {
                    model.File = result;
                    btntakepicture.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                var obj = sender as Entry;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = obj.Text;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void Currency_EntryCurrencyTextChanged(object sender, EntryCurrencyTextChanged e)
        {
            if (sender != null)
            {
                var obj = sender as EntryCurrency;
                int hashcode = int.Parse(obj.ClassId);
                var model = _models.FirstOrDefault(m => m.GetHashCode() == hashcode);
                if (model != null)
                {
                    model.Respuesta = e.Currency;
                    obj.BackgroundColor = Color.Accent;
                }
            }
        }

        private void BtnSaveAnswers_Clicked(object sender, EventArgs e)
        {
            btnsave = sender as Button;

            //var prueba = _models;
            btnsave.IsEnabled = false;
            SaveSection();

        }

        async void ImageButton_Clicked(System.Object sender, System.EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }

    public class SavePage
    {
        public ContentPage Page { get; set; }
        public bool Success { get; set; }
    }
}