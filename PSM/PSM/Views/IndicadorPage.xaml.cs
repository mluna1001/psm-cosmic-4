﻿using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IndicadorPage : ContentPage
    {
        public IndicadorPage()
        {
            InitializeComponent();
        }

        private void lvIndicadores_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lvIndicadores.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            MainViewModel.GetInstance().Indicador = new IndicadorViewModel();
            slIndicador.BindingContext = MainViewModel.GetInstance().Indicador;
        }
    }
}