﻿using PCLStorage;
using PSM.API;
using PSM.Controls;
using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PSM.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MultimediaPage : ContentPage
    {
        private Multimedia _multimedia { get; set; }
        private Abordaje _abordaje { get; set; }
        private AbordajeMultimedia _abordajemultimedia { get; set; }

        public MultimediaPage(Multimedia multimedia, Abordaje abordaje)
        {
            InitializeComponent();
            App.CurrentPage = this;
            _multimedia = multimedia;
            _abordaje = abordaje;
        }

        protected override bool OnBackButtonPressed()
        {
            try
            {
                var array = _multimedia.Nombre.Split('.');
                if (array.Length == 2)
                {
                    if (array[1] == "mp4")
                    {
                        if (_player != null)
                        {
                            _player.Stop();
                        }
                    }
                }
            }
            catch
            {

            }

            AbordajeToDB();

            return base.OnBackButtonPressed();
        }

        public bool Render()
        {
            var array = _multimedia.Nombre.Split('.');
            if (array.Length < 2)
            {
                return false;
            }
            else
            {
                var extension = array[1];
                Title = $"Multimedia: {_multimedia.Nombre}";
                switch (extension)
                {
                    case "jpg":
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RenderMultimedia, "Render", "jpg", _multimedia);
                        RenderImage();
                        break;

                    case "mp4":
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RenderMultimedia, "Render", "mp4", _multimedia);
                        RenderVideo();
                        break;

                    case "pdf":
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RenderMultimedia, "Render", "pdf", _multimedia);
                        RenderPdf();
                        break;

                    default: return false;
                }
                return true;
            }
        }

        #region RenderPDF
        private async void RenderPdf()
        {
            var downloadfile = await DownloadFile();
            if (downloadfile.Status)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Content = new ActivityIndicator { IsEnabled = true, IsVisible = true, IsRunning = true, VerticalOptions = LayoutOptions.Center };
                });

                PdfElement pdfelement = null;
                CustomWebView pdfElement = null;

                await Task.Run(async () =>
                {
                    var path = await GetPath();
                    //pdfelement = new PdfElement
                    //{
                    //    Uri = path,
                    //    HorizontalOptions = new LayoutOptions(LayoutAlignment.Fill, true),
                    //    VerticalOptions = new LayoutOptions(LayoutAlignment.Fill, true)
                    //};
                    pdfElement = new CustomWebView
                    {
                        Uri = path,
                        HorizontalOptions = LayoutOptions.FillAndExpand,
                        VerticalOptions = LayoutOptions.FillAndExpand
                    };

                });
                SetAbordaje();
                Device.BeginInvokeOnMainThread(() =>
                {
                    //Content = pdfelement;
                    Content = pdfElement;
                });
                return;
            }

            MessageService.ShowMessage("No es posible mostrar el documento PDF en este momento, intenta de nuevo");
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopAsync();
            else
                await Shell.Current.Navigation.PopAsync();
        }
        #endregion

        #region RenderVideo
        private Controls.MediaElement _player { get; set; }
        private async void RenderVideo()
        {
            var downloadfile = await DownloadFile();
            if (downloadfile.Status)
            {
                _player = new Controls.MediaElement
                {
                    AutoPlay = true,
                    Extension = ".mp4",
                };

                _player.MediaFailed += async (s, a) =>
                {
                    await Navigation.PopAsync();
                };

                _player.MediaOpened += (s, a) =>
                {
                    if (_abordajemultimedia != null)
                    {
                        _abordajemultimedia.Inicio = DateTime.Now;
                    }
                };

                _player.MediaEnded += async (s, a) =>
                {
                    AbordajeToDB();
                    if (Device.RuntimePlatform == Device.UWP)
                        await App.Current.MainPage.Navigation.PopModalAsync();
                    else
                        await Shell.Current.Navigation.PopModalAsync();
                };

                var stream = await GetStream();
                _player.Source = VideoSource.FromStream(() => stream, _multimedia.Nombre);
                SetAbordaje();
                Content = _player;
                return;
            }

            //await DisplayAlert("PSM 360", "No es posible mostrar la imagen, intenta de nuevo", "Aceptar");
            MessageService.ShowMessage("No es posible reproducir el video en este momento, intenta de nuevo");
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopModalAsync();
            else
                await Shell.Current.Navigation.PopModalAsync();
        }

        #endregion

        #region RenderImage

        private async void RenderImage()
        {
            var scrollview = new ScrollView();
            var downloadfile = await DownloadFile();
            if (downloadfile.Status)
            {
                try
                {
                    var stream = await GetStream();
                    var image = new Image
                    {
                        Source = ImageSource.FromStream(() => stream)
                    };
                    scrollview.Content = new Image
                    {
                        Source = _multimedia.url,
                        Aspect = Aspect.AspectFit
                    };
                    Content = scrollview;
                    SetAbordaje();
                    return;
                }
                catch (Exception ex)
                {
                }
            }

            MessageService.ShowMessage("No es posible mostrar la imagen en este momento, intenta de nuevo");
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopModalAsync();
            else
                await Shell.Current.Navigation.PopModalAsync();
        }

        #endregion

        private void SetAbordaje()
        {
            _abordajemultimedia = new AbordajeMultimedia
            {
                Number = _abordaje.Number,
                IdMultimedia = _multimedia.IdMultimedia,
                IdSeccionMenu = _multimedia.IdSeccionMenu,
                IdRuta = App.CurrentRoute.IdRuta,
                Inicio = DateTime.Now
            };
        }

        private void AbordajeToDB()
        {
            if (_abordajemultimedia != null)
            {
                _abordajemultimedia.Fin = DateTime.Now;
                App.DB.Add(_abordajemultimedia);
                App.DB.SaveChanges();
            }
        }

        #region DownloadFile/SearchFile
        private async Task<DownloadFile> DownloadFile()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (LabelStatus != null)
                {
                    LabelStatus.Text = "Descargando o buscando archivo...";
                }
            });
            IFolder rootFolder = FileSystem.Current.LocalStorage;
            ExistenceCheckResult exists = await rootFolder.CheckExistsAsync(_multimedia.Nombre);
            bool success = true;
            if (exists == ExistenceCheckResult.NotFound)
            {
                IFile file = null;
                try
                {
                    PSMClient client = new PSMClient();
                    var filestream = await client.Download(_multimedia.url, () => { });

                    if (filestream != null)
                    {
                        file = await rootFolder.CreateFileAsync(_multimedia.Nombre, CreationCollisionOption.OpenIfExists);
                        using (var fileforwrite = await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite))
                        {
                            await filestream.CopyToAsync(fileforwrite);
                            filestream.Dispose();
                        }
                        return new DownloadFile { Status = success, Path = file.Path };
                    }
                    else
                    {
                        success = false;
                    }
                }
                catch (Exception ex)
                {
                    if (file != null)
                    {
                        await file.DeleteAsync();
                    }
                    System.Diagnostics.Debug.WriteLine(ex.StackTrace);
                    success = false;
                }
            }
            else
            {
                var fileondisk = await rootFolder.GetFileAsync(_multimedia.Nombre);
                return new DownloadFile { Status = success, Path = fileondisk.Path };
            }
            return new DownloadFile { Status = success, Path = string.Empty };
        }

        private async Task<string> GetPath()
        {
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    if (LabelStatus != null)
                    {
                        LabelStatus.Text = "Cargando archivo...";
                    }
                });
                IFolder rootFolder = FileSystem.Current.LocalStorage;
                var fileondisk = await rootFolder.GetFileAsync(_multimedia.Nombre);
                return fileondisk.Path;
            }
            catch
            {

            }
            return null;
        }

        private async Task<Stream> GetStream()
        {
            try
            {
                IFolder rootFolder = FileSystem.Current.LocalStorage;
                var fileondisk = await rootFolder.GetFileAsync(_multimedia.Nombre);
                var stream = await fileondisk.OpenAsync(PCLStorage.FileAccess.Read);
                return stream;
            }
            catch
            {

            }
            return null;
        }
        #endregion

    }

    public class DownloadFile
    {
        public string Path { get; set; }
        public bool Status { get; set; }
    }
}