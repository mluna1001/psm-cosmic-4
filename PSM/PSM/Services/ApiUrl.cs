﻿namespace PSM.Services
{
    using Org.BouncyCastle.Bcpg;
    using PSM.Helpers;
    using Xamarin.Forms;

    public class ApiUrl
    {
        public static string BaseApi { get; set; }

        public static string sLogin { get { return $"{BaseApi}/OAuth/LoginPSM4"; } }
        public static string sSavePhone { get { return $"{BaseApi}/User/SaveUserPhone"; } }
        public static string sGetIndicadores { get { return $"{BaseApi}/Catalogo/GetIndicador"; } }
        public static string sGetImpersonalLogin { get { return $"{BaseApi}/OAuth/ImpersonaLogin"; } }
        public static string sGetAllProjectUsers { get { return $"{BaseApi}/User/GetAllProjectUsers"; } }

        public async static void SetUrl()
        {
            string urlLocalApi = "http://localhost:45332";
            // Instancia de API Producción
            BaseApi = "http://api.cosmic.mx";
            //BaseApi = "http://localhost:45332";

#if DEBUG
            var isr2 = await ConnectTools.IsRemoteReachable(urlLocalApi);
            if (Device.RuntimePlatform == Device.UWP && isr2.Success)
                BaseApi = urlLocalApi;
#endif
        }
    }
}
