﻿using PSM.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PSM.Services
{
    public class StorageServices
    {
        public static string TotalStorageDevice()
        {
            var storage = DependencyService.Get<IStorageDevice>();
            return storage.GetFullStorageDevice();
        }

        public static string FreeStorageDevice()
        {
            var storage = DependencyService.Get<IStorageDevice>();
            return storage.GetFreeStorageDevice();
        }

        //public static string InfoBatteryLevel()
        //{
        //    var battery = DependencyService.Get<IStorageDevice>();
        //    return battery.GetBatteryLevel();
        //}
    }
}
