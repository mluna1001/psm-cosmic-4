﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Services
{
    public class StoreService
    {
        public static double Distancia(decimal lat1, decimal long1, decimal lat2, decimal long2)
        {
            decimal theta = long1 - long2;
            double distancia = Math.Sin(Radianes((double)lat1)) * Math.Sin(Radianes((double)lat2)) + Math.Cos(Radianes((double)lat1)) * Math.Cos(Radianes((double)lat2)) * Math.Cos(Radianes((double)theta));
            distancia = (Grados(Math.Acos(distancia))) * 60 * 1.1515 * 1.609344;
            return distancia;
        }

        static private double Radianes(double grados)
        {
            return grados * Math.PI / 180;
        }

        static private double Grados(double radianes)
        {
            return radianes * 180 / Math.PI;
        }
    }
}
