﻿using PSM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PSM.Helpers.Services
{
    public class CheckAutomaticTime
    {
        public static bool IsAutomaticTime()
        {
            var automatic = DependencyService.Get<ICheckAutomaticTime>();
            try
            {
                bool bandera = automatic.IsAutomaticTime();
                return bandera;
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public static bool IsAutomaticZone()
        {
            var automatic = DependencyService.Get<ICheckAutomaticTime>();
            try
            {
                bool bandera = automatic.IsAutomaticZone();
                return bandera;
            }
            catch (Exception ex)
            {
                return true;
            }
        }
    }
}
