﻿using PSM.Models;
using PSM.ViewModels;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace PSM.Services
{
    public class NavigationService
    {
        public async void Navigate(string PageName)
        {
            //App.Master.IsPresented = false;

            Type page = Type.GetType("PSM.Views." + PageName + ",PSM");
            Page pageinstance = null;

            try
            {
                if (PageName.Equals("LoginPage"))
                {
                    var existingPages = Shell.Current.Navigation.NavigationStack;

                    Helpers.Settings.IsRemembered = "false";
                    Helpers.Settings.Alias = string.Empty;
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    App.Current.MainPage = new NavigationPage(new LoginPage());
                    return;
                }
                else
                {
                    pageinstance = (Page)Activator.CreateInstance(page);
                    if (!PageName.Equals("MainPage"))
                    {
                        InstanceViewModel(PageName);
                        if (Device.RuntimePlatform == Device.UWP)
                            await App.Current.MainPage.Navigation.PushAsync(pageinstance);
                        else
                            await Shell.Current.Navigation.PushAsync(pageinstance);
                    }
                    else
                    {
                        App.Current.MainPage = new MainPage();
                    }
                }

            }
            catch (Exception ex)
            {
                if (PageName.Equals("CloseSessionPage"))
                {
                    var res = await MessageService.ShowDialog("¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");

                    if (res)
                    {
                        //Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.CerrarSesionApp, "Ha cerrado sesión en la aplícación", "MasterPage");
                        Helpers.Settings.IsRemembered = "false";
                        Helpers.Settings.Alias = string.Empty;
                        MainViewModel.GetInstance().Login = new LoginViewModel();
                        App.Current.MainPage = new NavigationPage(new LoginPage());
                        return;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (PageName.Equals("DeleteDatabasePage"))
                {
                    bool answer = await MessageService.ShowDialog("¿Estás seguro de eliminar la base de datos?", "Si", "No");
                    if (!answer)
                    {
                        //Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.PrimerPreguntaEliminarBD, $"DBDelete - {(answer ? "Si" : "No")}", "HomePage");

                        bool answer2 = await MessageService.ShowDialog("Al eliminar la base se elimina toda tu información. ¿Deseas eliminarla?", "No", "Si");

                        if (answer2)
                        {
                            //Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.SegundaPreguntaEliminarBD, $"DBDelete - {(!answer2 ? "Si" : "No")}", "HomePage");

                            // Revisar pendientes al subir asistencias
                            var asistencias = App.DB.Asistencia.Where(a => !a.Sincronizado).ToList();

                            if (asistencias.Count > 0)
                            {
                                bool answer3 = await MessageService.ShowDialog("Tienes asistencias sin subir, con esta acción se eliminarán permanentemente. ¿Estás seguro de eliminar la base de datos?", "Si", "No");
                                if (!answer3)
                                {
                                    //Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.TercerPreguntaEliminarBD, $"DBDelete - {(answer3 ? "Si" : "No")}", "HomePage");
                                }
                            }
                            App.DB.DropTables();
                            App.DB.CreateTables();
                            // Se redirige a la pantalla de Login como instancia nueva ALV
                            Helpers.Settings.IsRemembered = "false";
                            Helpers.Settings.Alias = string.Empty;
                            MainViewModel.GetInstance().Login = new LoginViewModel();
                            App.Current.MainPage = new NavigationPage(new LoginPage());
                        }
                    }
                    return;
                }
                else
                {
                    MessageService.ShowMessage("No podemos abrir este menu, contacta a tu supervisor");
                    return;
                }
            }
        }

        private async void InstanceViewModel(string pageName)
        {
            Type page = Type.GetType("PSM.Views." + pageName.Replace("Page", "ViewModel") + ",PSM");
            if (page != null)
            {
                switch (pageName)
                {
                    case "AboutPage":
                        MainViewModel.GetInstance().About = (AboutViewModel)Activator.CreateInstance(page);
                        break;
                    case "LogOut":
                        //var res = await App.Current.MainPage.DisplayAlert("Mi Asistencia", "¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");
                        var res = await MessageService.ShowDialog("¿Estás seguro que deseas cerrar sesión en la aplicación?", "No", "Si");
                        if (res)
                        {
                            //Bitacora.SaveBitacora((int)Bitacora.AccionBitacora.CerrarSesionApp, "Ha cerrado sesión en la aplícación", "MasterPage");
                            Helpers.Settings.IsRemembered = "false";
                            Helpers.Settings.Alias = string.Empty;
                            MainViewModel.GetInstance().Login = new LoginViewModel();
                            App.Current.MainPage = new NavigationPage(new LoginPage());
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
