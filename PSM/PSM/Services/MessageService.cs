﻿namespace PSM.Services
{
    using System.Threading.Tasks;
    using Xamarin.Forms;

    public class MessageService
    {
        public static async void ShowMessage(string message)
        {
            if (Application.Current.MainPage != null)
            {
                await Application.Current.MainPage.DisplayAlert("PSM", message, "Aceptar");
            }
        }

        public static async Task<bool> ShowDialog(string message, string buttonTrue, string buttonFalse = "")
        {
            var res = await Application.Current.MainPage.DisplayAlert("PSM", message,
                string.IsNullOrEmpty(buttonFalse) ? null : buttonFalse, buttonTrue);

            return res;
        }
    }
}
