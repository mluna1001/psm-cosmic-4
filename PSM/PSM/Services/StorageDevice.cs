﻿using PSM.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PSM.Helpers.Services
{
    public class StorageDevice
    {
        public static string InfoStorageDevice()
        {
            var storage = DependencyService.Get<IStorageDevice>();
            return storage.GetFullStorageDevice();
        }

        public static string InfoBatteryLevel()
        {
            var battery = DependencyService.Get<IStorageDevice>();
            return battery.GetBatteryLevel();
        }
    }
}