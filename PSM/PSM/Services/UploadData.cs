﻿using Microsoft.AppCenter.Crashes;
using PSM.Helpers;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static PSM.API.PSMClient;

namespace PSM.Services
{
    public class UploadData
    {
        private static string message = string.Empty;

        public static UploadRespAudit seccionFaltante = null;


        //      ██
        //     ███
        //    █ ██
        //      ██ 
        //    ██████
        //Se obtiene la informacion de la ruta que se subira al servidor
        public static async Task<bool> Upload(Ruta ruta, bool isFlagUserStatus)
        {

            bool statusPhoto = false;
            int idruta = ruta.IdRuta;
            bool bandera = false;

            try
            {
                if (ruta.IdRutaDinamica != 0)
                {
                    idruta = ruta.IdRutaDinamica;
                }

                var respuestasauditor = App.DB.RespuestaAuditor.Where(r => r.Sincronizado == false && r.IdRuta == idruta).ToList();
                if (respuestasauditor.Count == 0)
                {
                    message = "No hay respuestas pendientes para la tienda: " + ruta.TiendaNombre + " para el día: " + ruta.NombreDelDia;
                }
                var abordajes = App.DB.Abordaje.Where(a => a.Sincronizado == false && a.IdRuta == idruta).ToList();
                var abordajesmultimedia = App.DB.AbordajeMultimedia.Where(am => am.Sincronizado == false && am.IdRuta == idruta).ToList();
                var asistencias = App.DB.Asistencia.Where(a => a.Sincronizado == false && a.IdRuta == idruta).ToList();

                var fotos = respuestasauditor.Where(r => r.Foto != null).ToList();
                var respuestassinfoto = respuestasauditor.Where(r => r.Foto == null).ToList();

                //Si se ejectuta el respaldo de la base de datos en estatus 2 isFlagUserStatus
                if (isFlagUserStatus)
                {
                    //Consulto las secciones que son obligatorias
                    var seccionesid = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();

                    foreach (var id in seccionesid)
                    {
                        if (!App.DB.RespuestaAuditor.Exists(ra => ra.IdSeccion == id.IdSeccion && ra.IdRuta == idruta))
                        {
                            App.CurrentRespAudit.Add(new UploadRespAudit { Key = false, IdSeccion = id.IdSeccion, IdRuta = idruta, Value = id.Descripcion });
                        }
                        else
                        {
                            App.CurrentRespAudit.Add(new UploadRespAudit { Key = true, IdSeccion = id.IdSeccion, IdRuta = idruta, Value = id.Descripcion });
                        }
                    }
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "Upload", "UploadData", $"Se comienza a cargar información de la ruta: { idruta } - { ruta.TiendaNombre}");

                if (App.CurrentRespAudit != null)
                    seccionFaltante = App.CurrentRespAudit.FirstOrDefault(s => !s.Key && s.IdRuta == idruta);

                if (ruta.IdRuta > 0)
                {
                    bandera = await updateinfo(respuestassinfoto, ruta, abordajes, abordajesmultimedia, asistencias, fotos, isFlagUserStatus);
                }
                else
                {
                    try
                    {
                        int idRuta = ruta.IdRutaDinamica;
                        if (ruta.IdRutaDinamica == 0)
                        {
                            //Sube a el API la informacion necesaria para que realice el insert de ruta y nos devuelva el idRuta
                            idRuta = await ObtenerIdRutaDinamico(ruta);
                        }

                        if (ruta.IdRuta < 0 && idRuta > 0)
                        {
                            UpdateIdRutaObjects(respuestassinfoto, ruta, abordajes, abordajesmultimedia, asistencias, fotos, idRuta);
                            bandera = await updateinfo(respuestassinfoto, ruta, abordajes, abordajesmultimedia, asistencias, fotos, isFlagUserStatus);
                        }
                    }
                    catch (Exception e)
                    {

                    }
                }

                if (bandera)
                {
                    MessageService.ShowMessage(ruta.TiendaNombre + "\n" + message);
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "Upload", "UploadData", $"La información de la ruta: { ruta.IdRuta } - { ruta.TiendaNombre} fue cargada con éxito");
                }
            }
            catch (Exception ex)
            {

            }
            return bandera;
        }

        //    █████ 
        //   █    ██
        //      ███ 
        //    ██    
        //    ██████
        //Metodo que sube las asistencias obtenidas de "Upload" que es las informacion de la ruta que tiene que subir, metodo nuevo
        public static async Task<bool> SubirRespuestasAsistencias(Ruta ruta, List<Asistencia> asistencias, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;

            Auditoria asistencia = new Auditoria
            {
                Ruta = ruta,
                Asistencia = asistencias
            };

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirRespuestasAsistencias", "UploadData", asistencia);

            bool isconnected = true;

            var response = await App.API.UploadEndPoint.UploadAuditsAsistencia(asistencia, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    if (asistencias.Any())
                    {
                        foreach (var item in asistencias)
                        {
                            item.Sincronizado = true;
                            App.DB.Entry(item).State = DataBase.EntryState.Modify;
                        }

                        //var bitacoraSincMovil = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                        if (bitacoraSincMovil == null)
                        {
                            bitacoraSincMovil = new BitacoraSincronizacion();
                            bitacoraSincMovil.IdRuta = ruta.IdRuta;
                            App.DB.BitacoraSincronizacion.Add(bitacoraSincMovil);
                            App.DB.SaveChanges();
                        }
                        bitacoraSincMovil.Entrada = true;
                        bitacoraSincMovil.Salida = true;
                        App.DB.Entry(bitacoraSincMovil).State = DataBase.EntryState.Modify;
                        App.DB.SaveChanges();
                    }
                    bandera = true;
                    message = response.Message;
                    //Se guarda en bitacora que se subio la tienda con exito
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirRespuestasAsistencias", "UploadData", "Se ha guardado la asistencia con exito");
                    //Actualizamos los regitros subidos y les ponemos la bandera sincronizado
                }
                else
                {
                    bandera = false;
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirRespuestasAsistencias", "UploadData", "No fue posible subir las asistencias por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirRespuestasAsistencias", "UploadData", "No fue posible subir las asistencias, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }

            if (isconnected)
            {
                await Bitacora.Upload();
            }

            return bandera;
        }


        //    █████       ██ 
        //   █    ██     ███
        //      ███     █ ██
        //    ██          ██
        //    ██████ ██ ██████
        //Metodo que sube las respuestas obtenidas de "Upload" que es las informacion de la ruta que tiene que subir, metodo nuevo
        public static async Task<bool> SubirRespuestas(Ruta ruta, List<RespuestaAuditor> respuestas, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;

            Auditoria respuesta = new Auditoria
            {
                Ruta = ruta,
                RespuestaAuditor = respuestas
            };

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirRespuestas", "UploadData", respuesta);

            bool isconnected = true;

            //Realiza la peticion post para subir las respuestas
            var response = await App.API.UploadEndPoint.UploadAuditsRespuesta(respuesta, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    //Actualizamos los regitros subidos y les ponemos la bandera sincronizado
                    if (respuestas.Any())
                    {
                        foreach (var item in respuestas)
                        {
                            item.Sincronizado = true;
                            App.DB.Entry(item).State = DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                    }

                    //var bitacoraSincMovil = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                    bitacoraSincMovil.Respuestas = true;

                    App.DB.Entry(bitacoraSincMovil).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                    bandera = true;


                    if (response.SendRate)
                    {
                        message = message + "Con una calificacion de: " + response.Rate;
                    }
                    else
                    {
                        message = response.Message;
                    }

                    //Se guarda en bitacora que se subieron las respuestas de la tienda con exito
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirRespuestas", "UploadData", "Se han guardado las respuestas con exito");

                }
                else
                {
                    bandera = false;
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirRespuestas", "UploadData", "No fue posible subir respuestas por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                    {

                    });

                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirRespuestas", "UploadData", "NO fue posible subir las respuestas, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }

            if (isconnected)
            {
                await Bitacora.Upload();
            }

            return bandera;
        }

        //    █████      █████ 
        //   █    ██    █    ██
        //      ███        ███ 
        //    ██         ██    
        //    ██████ ██  ██████
        //Metodo que sube las respuestas obtenidas de "Upload" que es las informacion de la ruta que tiene que subir, metodo nuevo
        public static async Task<bool> SubirAbordajes(Ruta ruta, List<Abordaje> abordajes, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;

            Auditoria respuesta = new Auditoria
            {
                Ruta = ruta,
                Abordaje = abordajes
            };

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirAbordajes", "UploadData", respuesta);

            bool isconnected = true;

            //Realiza la peticion post para subir los abordajes
            var response = await App.API.UploadEndPoint.UploadAuditsAbordaje(respuesta, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    // Actualizamos los regitros subidos y les ponemos la bandera sincronizado

                    if (abordajes.Any())
                    {
                        foreach (var item in abordajes)
                        {
                            item.Sincronizado = true;
                            App.DB.Entry(item).State = DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                    }

                    //var bitacoraSincMovil = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                    bitacoraSincMovil.Abordaje = true;
                    App.DB.Entry(bitacoraSincMovil).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                    bandera = true;

                    message = response.Message;
                    //Se guarda en bitacora que se subieron las respuestas de la tienda con exito
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirAbordajes", "UploadData", "Se han guardado los abordajes con exito");

                }
                else
                {
                    bandera = false;
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirAbordajes", "UploadData", "No fue posible subir abordajes por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirAbordajes", "UploadData", "NO fue posible subir los abordajes, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }

            if (isconnected)
            {
                await Bitacora.Upload();
            }

            return bandera;
        }

        //     █████      ██████ 
        //    █    ██         ██
        //       ███        ████ 
        //     ██             ██    
        //     ██████ ██  ██████
        //Metodo que sube las respuestas obtenidas de "Upload" que es las informacion de la ruta que tiene que subir, metodo nuevo
        public static async Task<bool> SubirAbordajesMultimedia(Ruta ruta, List<AbordajeMultimedia> abordajesmultimedia, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;

            Auditoria respuesta = new Auditoria
            {
                Ruta = ruta,
                AbordajeMultimedia = abordajesmultimedia
            };

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirAbordajesMultimedia", "UploadData", respuesta);

            bool isconnected = true;

            //Realiza la peticion post para subir los abordajes multimedia
            var response = await App.API.UploadEndPoint.UploadAuditsAbordajeMultimedia(respuesta, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    // Actualizamos los regitros subidos y les ponemos la bandera sincronizado
                    if (abordajesmultimedia.Any())
                    {
                        foreach (var item in abordajesmultimedia)
                        {
                            item.Sincronizado = true;
                            App.DB.Entry(item).State = DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                    }

                    //var bitacoraSincMovil = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                    bitacoraSincMovil.AbordajeMultimedia = true;
                    App.DB.Entry(bitacoraSincMovil).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                    bandera = true;

                    message = response.Message;
                    //Se guarda en bitacora que se subieron las respuestas de la tienda con exito
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirAbordajesMultimedia", "UploadData", "Se han guardado los abordajes multimedia con exito");

                }
                else
                {
                    bandera = false;
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirAbordajesMultimedia", "UploadData", "No fue posible subir los abordajes multimedia por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirAbordajesMultimedia", "UploadData", "NO fue posible subir subir los abordajes multimedia, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }

            if (isconnected)
            {
                await Bitacora.Upload();
            }

            return bandera;
        }

        //     █████      ██  ██ 
        //    █    ██     ██  ██
        //       ███      ██████ 
        //     ██             ██    
        //     ██████ ██      ██
        //Metodo que sube las respuestas obtenidas de "Upload" que es las informacion de la ruta que tiene que subir, metodo nuevo
        public static async Task<bool> SubirRuta(Ruta ruta, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirRuta", "UploadData", ruta);

            bool isconnected = true;


            var response = await App.API.UploadEndPoint.UploadRoute(ruta, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    //se verificara si en las tiendas de este idtiempo que se sube existe alguna del tipo estatuscheck para  
                    //saber si es del tipo de proyecto que se suben de un jalon todas las tiendas
                    bool tipotienda = verificatipotienda(ruta);

                    if (!tipotienda)
                    {
                        //Actualizamos los regitros subidos y les ponemos la bandera sincronizado
                        ruta.Sincronizado = true;
                        App.DB.Entry(ruta).State = DataBase.EntryState.Modify;
                        App.DB.SaveChanges();

                        //var bitacoraSincMovil = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                        bitacoraSincMovil.Ruta = true;
                        App.DB.Entry(bitacoraSincMovil).State = DataBase.EntryState.Modify;
                        App.DB.SaveChanges();
                        bandera = true;
                        message = response.Message;

                        //Se guarda en bitacora que se subieron las respuestas de la tienda con exito
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirRuta", "UploadData", "Se han actualizado las banderas de Ruta");
                    }
                    else
                    {
                        //var bitacoraSincronizacion = (from r in App.DB.Ruta.Where(s => s.IdTiempo == ruta.IdTiempo)
                        //                              join bt in App.DB.BitacoraSincronizacion on r.IdRuta equals bt.IdRuta into ps
                        //                              from bt in ps.DefaultIfEmpty()
                        //                              select new { idruta = r.IdRuta, ruta = bt.Ruta }).ToList();


                        var rutaSinc = App.DB.Ruta.Where(s => s.IdTiempo == ruta.IdTiempo && s.IdUsuario == ruta.IdUsuario).ToList();
                        //Verificar el idRUta ya que se esta haciendo la busqueda por idruta y como es negaviot el count nunca cuadra
                        List<int> rutadinamica = rutaSinc.Select(s => s.IdRutaDinamica).ToList();
                        List<int> idrutaSinc = rutaSinc.Where(s => s.IdRuta > 0).Select(s => s.IdRuta).ToList();
                        idrutaSinc.AddRange(rutadinamica);
                        var bitaSinc = App.DB.BitacoraSincronizacion.Where(d => idrutaSinc.Contains(d.IdRuta)).ToList();



                        if (bitaSinc.Count() == rutaSinc.Count())
                        {
                            foreach (var item in bitaSinc)
                            {
                                item.Ruta = true;
                                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                                App.DB.SaveChanges();

                            }

                            foreach (var item in rutaSinc)
                            {
                                item.Sincronizado = true;
                                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                                App.DB.SaveChanges();

                            }
                        }
                        bandera = true;
                    }
                }
                else
                {
                    bandera = false;
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirRuta", "UploadData", "No fue posible actualizar ruta por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirRuta", "UploadData", "NO fue posible actualizar ruta, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }

            if (isconnected)
            {
                await Bitacora.Upload();
            }

            return bandera;
        }

        //     █████      ██████ 
        //    █    ██     ██  
        //       ███      ██████ 
        //     ██             ██    
        //     ██████ ██  ██████ RUTA DINAMICA
        //Metodo que solo lo usa la ruta externa para obtener el idruta del servidor y actualizar todos los idruta del movil
        public static async Task<int> ObtenerIdRutaDinamico(Ruta ruta)
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ObteniendoIdRutaDinamico, "ObtenerIdRutaDinamico", "UploadData", ruta);
            int idRuta = 0;

            bool isconnected = true;

            //Realiza la peticion post para subir los abordajes multimedia
            var response = await App.API.UploadEndPoint.UploadRouteDinamica(ruta, () =>
            {
                isconnected = false;
            });

            if (response != null)
            {
                if (response.Status)
                {
                    idRuta = int.Parse(response.IdRuta);
                    message = response.Message;
                }
                else
                {
                    message = response.Message;
                }
            }
            else
            {
                //Esta conectado
                if (!isconnected)
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "ObtenerIdRutaDinamico", "UploadData", "No fue posible obtener el IdRuta dinamico por falta de internet");
                    // marcamos error de server
                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                }
                else
                {
                    // 
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "ObtenerIdRutaDinamico", "UploadData", "No fue posible obtener el IdRuta dinamico, inténtalo nuevamente");
                    // mostramos error de internet
                    MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                }
            }
            if (isconnected)
            {
                await Bitacora.Upload();
            }
            return idRuta;
        }

        //Metodo que verifica que la ruta que se va a subir es del tipo donde se sube las rutas del dia de un solo jalon
        public static bool verificatipotienda(Ruta ruta)
        {
            bool status = false;
            var rutasdia = App.DB.Ruta.Where(s => s.IdTiempo == ruta.IdTiempo && s.IdUsuario == s.IdUsuario).ToList();
            List<int> idtiendas = rutasdia.Select(s => s.IdTienda).ToList();
            var infotienda = App.DB.InfoTienda.Where(s => idtiendas.Contains(s.IdTienda)).FirstOrDefault(r => r.EstatusCheck != null);

            if (infotienda != null)
            {
                status = true;
            }
            return status;
        }

        public static bool UpdateIdRutaObjects(List<RespuestaAuditor> respuestas,
            Ruta ruta,
            List<Abordaje> abordajes,
            List<AbordajeMultimedia> abordajesmultimedia,
            List<Asistencia> asistencias,
            List<RespuestaAuditor> fotos,
            int idRuta)
        {
            bool status = false;

            ruta.IdRutaDinamica = idRuta;
            App.DB.Entry(ruta).State = DataBase.EntryState.Modify;
            App.DB.SaveChanges();

            foreach (var item in respuestas)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }
            foreach (var item in abordajes)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }
            foreach (var item in abordajesmultimedia)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }
            foreach (var item in asistencias)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }
            foreach (var item in fotos)
            {
                string nameupdate = string.Empty;

                nameupdate = item.FotoNombre.Substring(0, item.FotoNombre.LastIndexOf(item.IdRuta.ToString())).ToString();
                nameupdate += idRuta.ToString();

                item.IdRuta = idRuta;
                item.FotoNombre = nameupdate;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
                App.DB.SaveChanges();
            }
            return status;
        }

        public static bool RollbackIdRutaObjects(List<RespuestaAuditor> respuestas,
         Ruta ruta,
         List<Abordaje> abordajes,
         List<AbordajeMultimedia> abordajesmultimedia,
         List<Asistencia> asistencias,
         List<RespuestaAuditor> fotos,
         int idRuta)
        {
            bool status = false;

            ruta.IdRuta = ruta.IdRutaDinamica;
            idRuta = ruta.IdRuta;
            App.DB.Entry(ruta).State = DataBase.EntryState.Modify;
            App.DB.SaveChanges();

            foreach (var item in respuestas)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
            }
            foreach (var item in abordajes)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
            }
            foreach (var item in abordajesmultimedia)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
            }
            foreach (var item in asistencias)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
            }
            foreach (var item in fotos)
            {
                item.IdRuta = idRuta;
                App.DB.Entry(item).State = DataBase.EntryState.Modify;
            }

            App.DB.SaveChanges();

            return status;
        }

        public static async Task<bool> updateinfo(List<RespuestaAuditor> respuestassinfoto,
            Ruta ruta,
            List<Abordaje> abordajes,
            List<AbordajeMultimedia> abordajesmultimedia,
            List<Asistencia> asistencias,
            List<RespuestaAuditor> fotos, bool isFlagUserStatus)
        {
            bool bandera = false;

            int idruta = ruta.IdRuta;
            if (ruta.IdRuta < 0)
            {
                idruta = ruta.IdRutaDinamica;
            }

            //Valida que exista una entrada y una salida
            var entrada = App.DB.Asistencia.FirstOrDefault(f => f.IdRuta == idruta && f.estatus);
            var salida = App.DB.Asistencia.FirstOrDefault(f => f.IdRuta == idruta && !f.estatus);
            //--------------------------

            if (seccionFaltante == null && entrada != null && salida != null)
            {

                //Se obtiene la bitacora de sincronizacion para saber que subira el app.
                var checkBitacoraSinc = App.DB.BitacoraSincronizacion.FirstOrDefault(r => r.IdRuta == idruta);

                if (checkBitacoraSinc == null)
                {
                    checkBitacoraSinc = new BitacoraSincronizacion();
                    checkBitacoraSinc.IdRuta = idruta;
                    App.DB.BitacoraSincronizacion.Add(checkBitacoraSinc);
                    App.DB.SaveChanges();

                }

                //Sino se han sincronizado las asistencias en bitacora checo las banderas para actualizar
                if (!checkBitacoraSinc.Entrada || !checkBitacoraSinc.Salida)
                {
                    //Este metodo sube las asistencias 
                    await SubirRespuestasAsistencias(ruta, asistencias, checkBitacoraSinc);
                }

                //Si tengo la asistencia de Entrada y Salida subo las respuestas
                if (checkBitacoraSinc.Entrada && checkBitacoraSinc.Salida && checkBitacoraSinc.Respuestas == false)
                {
                    //Este metodo sube las respuestas 
                    await SubirRespuestas(ruta, respuestassinfoto, checkBitacoraSinc);
                }

                //Si tengo las respuestas arriba quiere decir que tengo entrada y salida por lo que se procede a subir abordajes
                if (checkBitacoraSinc.Respuestas && checkBitacoraSinc.Abordaje == false)
                {
                    //Este metodo sube los abordajes
                    await SubirAbordajes(ruta, abordajes, checkBitacoraSinc);
                }

                //Si tengo los abordajes se procede a subir el abordaje multimedia
                if (checkBitacoraSinc.Abordaje && checkBitacoraSinc.AbordajeMultimedia == false)
                {
                    //Este metodo sube los abordajes multimedia
                    await SubirAbordajesMultimedia(ruta, abordajesmultimedia, checkBitacoraSinc);
                }

                //Verifica si se  han subido toda la informacion excepto las fotos 
                if (checkBitacoraSinc.Entrada && checkBitacoraSinc.Salida && checkBitacoraSinc.Respuestas && checkBitacoraSinc.Abordaje && checkBitacoraSinc.AbordajeMultimedia)
                {
                    //Este metodo actualiza las banderas de ruta en el servidor y local
                    await SubirFotos(ruta, fotos, isFlagUserStatus, checkBitacoraSinc);
                }


                //Version 4.0.8 (Parche para asegurar que todo esta arriba antes de cambiar el estatus de la ruta a sincronizado.
                bool checkFoto = VerificaRespuesta(ruta);

                if (checkBitacoraSinc.Entrada && checkBitacoraSinc.Salida && checkBitacoraSinc.Respuestas && checkBitacoraSinc.Abordaje && checkBitacoraSinc.AbordajeMultimedia && checkBitacoraSinc.Foto && checkFoto)
                {
                    //Este metodo actualiza las banderas de ruta en el servidor y local
                    bandera = await SubirRuta(ruta, checkBitacoraSinc);
                }
            }

            return bandera;
        }

        public static async Task<bool> SubirFotos(Ruta ruta, List<RespuestaAuditor> fotos, bool isFlagUserStatus, BitacoraSincronizacion bitacoraSincMovil)
        {
            bool bandera = false;
            var sa = App.CurrentRespAudit;
            //subimos la foto
            bool hasinternet = true;
            List<UploadPhoto> uploads = new List<UploadPhoto>();
            bool breaks = false;


            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SubiendoInformacion, "SubirFotos", "UploadData", $"Se comienza a cargar las fotos de la ruta");

            int last = fotos.Count - 1;
            int index = 0;

            if (fotos.Count() == 0)
            {
                var uploadheader = await App.API.UploadEndPoint.UploadFotoHeader(ruta, 0, 0, 0, "", "", () =>
                {
                    hasinternet = false;
                });
            }
            else
            {
                foreach (var item in fotos)
                {
                    if (breaks)
                    {
                        break;
                    }
                    if (item.Foto != null)
                    {
                        float length = item.Foto.Length;
                        int rangeini = 10000;
                        //int rangefin = rangeini + 1000;
                        float partes = 10000;
                        float paq = length / partes;
                        int packages = paq.numeropaquetes();


                        string fech = item.Fecha.ToString("dd-MM-yy HH:mm:ss");

                        //SE conecta a el Api para generar el encabezado.
                        var uploadheader = await App.API.UploadEndPoint.UploadFotoHeader(ruta, (int)length, packages, item.IdAnswerAudit, fech, item.FotoNombre, () =>
                        {
                            hasinternet = false;
                        });
                        UploadResponse uploadresponse = new UploadResponse();
                        if (uploadheader != null && hasinternet)
                        {
                            try
                            {
                                if (uploadheader.Status)
                                {
                                    int packup = uploadheader.Package;
                                    for (int i = packup; i < packages; i++)
                                    {
                                        int inicio = rangeini * packup;
                                        int fin = 10000;
                                        //verifica el final del arreglo
                                        var arraycontrol = (int)length - (i * 10000);
                                        if (arraycontrol < 10000)
                                        {
                                            fin = arraycontrol;
                                        }

                                        var bytearrayimga = item.Foto.ToList().GetRange(inicio, fin).ToArray();

                                        bool ultimafoto = false;
                                        if (index == last)
                                            ultimafoto = true;





                                        uploadresponse = await App.API.UploadEndPoint.UploadFoto(item, fech, bytearrayimga, ultimafoto, () =>
                                        {
                                            hasinternet = false;
                                        });

                                        if (uploadresponse != null && hasinternet)
                                        {
                                            if (uploadresponse.Status)
                                            {
                                                packup = uploadresponse.Package;

                                                if (packup == -1)
                                                {

                                                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirFotos", "UploadData", "Se cargaron mas paquetes de los esperados");
                                                    // marcamos error de server
                                                    MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                                                    bandera = false;
                                                    breaks = true;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                bandera = false;
                                                message = uploadheader.Message;
                                            }
                                        }
                                        else
                                        {
                                            //Esta conectado
                                            if (!hasinternet)
                                            {
                                                // 
                                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirFotos", "UploadData", "No fue posible subir la foto no se conecto al servidor");
                                                // marcamos error de server
                                                MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                                                bandera = false;
                                                break;
                                            }
                                            else
                                            {
                                                // 
                                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirFotos", "UploadData", "No fue posible subir la foto, inténtalo nuevamente");
                                                // mostramos error de internet
                                                MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                                                bandera = false;
                                                break;
                                            }

                                        }

                                    }
                                    if (packup == packages)
                                    {
                                        item.Sincronizado = true;
                                        App.DB.Entry(item).State = DataBase.EntryState.Modify;
                                        App.DB.SaveChanges();
                                        bandera = true;
                                    }

                                }
                                else
                                {
                                    bandera = false;
                                    message = uploadheader.Message;
                                }
                            }
                            catch (Exception ex)
                            {
                                Crashes.TrackError(ex, App.TrackData());
                            }

                        }
                        else
                        {
                            //Esta conectado
                            if (!hasinternet)
                            {
                                // 
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "SubirFotos", "UploadData", "No fue posible subir la foto no se conecto al servidor");
                                // marcamos error de server
                                MessageService.ShowMessage("Intenta subir de nuevo la tienda");
                                bandera = false;
                                break;
                            }
                            else
                            {
                                // 
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlSubirInformacion, "SubirFotos", "UploadData", "No fue posible subir la foto, inténtalo nuevamente");
                                // mostramos error de internet
                                MessageService.ShowMessage("No es posible subir tu información en estos momentos. Intenta más tarde.");
                                bandera = false;
                                break;
                            }
                        }
                        if (!hasinternet)
                        {
                            break;
                        }
                    }
                    index++;
                }
            }

            var verificafotos = fotos.Where(s => s.Sincronizado == false);
            if (verificafotos.Count() == 0)
            {
                bitacoraSincMovil.Foto = true;
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DatosCargadosEnServidor, "SubirFotos", "UploadData", "Se subieron las fotos al servidor");
            }
            if (!hasinternet)
            {
                message = "No tienes conexión a internet, intenta más tarde";
                bandera = false;
            }
            return bandera;
        }

        public static bool VerificaRespuesta(Ruta ruta)
        {
            bool checkFoto = true;

            var ra = App.DB.RespuestaAuditor.Where(s => s.IdRuta == ruta.IdRuta && s.Sincronizado == false && s.Foto != null).ToList();

            if (ra.Count() != 0)
            {
                var bi = App.DB.BitacoraSincronizacion.FirstOrDefault(s => s.IdRuta == ruta.IdRuta);
                if (bi != null)
                {
                    bi.Foto = false;
                    checkFoto = false;
                    App.DB.Entry(bi).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();

                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioInformacionNoInternet, "VerificaRespuesta", "UploadData", "No fue posible actualizar ruta por falta de internet");
                    //// marcamos error de server
                    //MessageService.ShowMessage("No cuentas con buena conexion a internet, favor de subir la tienda mas tarde " + ruta.TiendaNombre);
                }
            }
            return checkFoto;
        }
    }
}
