﻿namespace PSM.Helpers
{
    using PSM.Interfaces;
    using Xamarin.Forms;
    public class CloseApplication
    {
        public void CloseApp()
        {
            var close = DependencyService.Get<ICloseApplication>();
            close?.CloseApp();
        }
    }
}
