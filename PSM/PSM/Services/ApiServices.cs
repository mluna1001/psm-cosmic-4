﻿namespace PSM.Services
{
    using Cosmic;
    using Microsoft.AppCenter.Crashes;
    using Newtonsoft.Json;
    using Plugin.Connectivity;
    using PSM.Common;
    using PSM.Helpers;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using Xamarin.Essentials;

    public class ApiServices
    {
        public static async Task<bool> Connection()
        {
            var conn = await ConnectTools.CheckConnection("www.google.com");
            return conn.Success;
        }

        public static async Task<T> Get<T>(string url)
        {
            var conn = await ConnectTools.CheckConnection(url);
            if (conn.Success)
            {
                try
                {
                    var client = new HttpClient();
                    var response = await client.GetAsync(url);
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<T>(responsestring);
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
            }
            return default(T);
        }
        public static List<T> GetRequest<T>(string v)
        {
            var result = new List<T>();
            try
            {
                var response = HttpGet<Response>(v);

                if (response != null && response.Exito)
                {
                    result = JsonConvert
                                .DeserializeObject<List<T>>
                                    (response.Datos.ToString());
                }
            }
            catch (Exception ex)
            {
                return result;
            }
            return result;
        }
        public static T HttpGet<T>(string URI)
        {
            try
            {
                WebClient client = new WebClient();
                client.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");

                Stream data = client.OpenRead(URI);
                StreamReader reader = new StreamReader(data);
                string s = reader.ReadToEnd();
                data.Close();
                reader.Close();
                if (!s.IsNullEmpty())
                {
                    return JsonConvert.DeserializeObject<T>(s);
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData(ex));
            }
            return default(T);
        }

        public static async Task<T> Post<T>(string url, Dictionary<string, string> formdata)
        {
            //var conn = await ConnectTools.CheckConnection(url);
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    var form = new FormUrlEncodedContent(formdata);
                    var client = new HttpClient();
                    var response = await client.PostAsync(url, form);
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<T>(responsestring);
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
            }
            return default(T);
        }

        public static async Task<T> Post<T, K>(string url, K auditoria, Action<string> noconnected)
        {
            var conn = await ConnectTools.CheckConnection("www.google.com");
            if (conn.Success)
            {
                try
                {
                    var clientHandler = new HttpClientHandler();
                    string json = JsonConvert.SerializeObject(auditoria);
                    clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };


                    var client = new HttpClient(clientHandler);
                    //var json = JsonConvert.SerializeObject(auditoria);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                    {
                        Content = new StringContent(json, Encoding.UTF8, "application/json")

                    };
                    var response = await client.SendAsync(request);
                    if (response != null && response.IsSuccessStatusCode)
                    {
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<T>(responsestring, new JsonSerializerSettings
                        {
                            DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
                            DateTimeZoneHandling = DateTimeZoneHandling.Local
                        });
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                    noconnected?.Invoke(ex.Message);
                }
            }
            else
            {
                noconnected?.Invoke(conn.Message);
            }
            return default(T);
        }

        public static async Task<T> Put<T>(string url, T model)
        {
            var connect = await ConnectTools.CheckConnection("www.google.com");

            if (connect.Success)
            {
                try
                {
                    var request = JsonConvert.SerializeObject(model);
                    var content = new StringContent(request, Encoding.UTF8, "application/json");
                    var client = new HttpClient();
                    client.BaseAddress = new Uri(url);
                    var response = await client.PutAsync(url, content);

                    if (response.IsSuccessStatusCode)
                    {
                        var responsestring = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<T>(responsestring);
                    }

                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
            }

            return default;
        }

        public static T PostSinc<T, K>(string uri, K datos)
        {
            try
            {
                //string parsedContent = JsonConvert.SerializeObject(datos);
                using (var client = new WebClient())
                {
                    var dataString = JsonConvert.SerializeObject(datos);
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    client.Timeout = 600 * 60 * 1000;
                    var responsestring = client.UploadString(new Uri(uri), "POST", dataString);
                    return JsonConvert.DeserializeObject<T>(responsestring);
                }
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
        private class WebClient : System.Net.WebClient
        {
            public int Timeout { get; set; } = 300000;

            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest lWebRequest = base.GetWebRequest(uri);
                lWebRequest.Timeout = Timeout;
                ((HttpWebRequest)lWebRequest).ReadWriteTimeout = Timeout;
                return lWebRequest;
            }
        }



        public static async Task<T> Post<T>(string url, List<Param> formdata, Action noconnected)
        {
            var conn = await ConnectTools.CheckConnection(url);

            if (conn.Success)
            {
                try
                {
                    var client = new HttpClient();
                    MultipartFormDataContent content = new MultipartFormDataContent();
                    if (formdata != null)
                    {
                        foreach (var item in formdata)
                        {
                            switch (item.Type)
                            {
                                case ParamType.File:
                                    var stream = new MemoryStream(item.Bytes);
                                    content.Add(new StreamContent(stream), item.Name, item.FileName);
                                    break;
                                case ParamType.String:
                                    content.Add(new StringContent(item.Element.ToString()), item.Name);
                                    break;
                                case ParamType.Obj:
                                    var cliente = new HttpClient();
                                    var json = JsonConvert.SerializeObject(item.Element);
                                    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, url)
                                    {
                                        Content = new StringContent(json, Encoding.UTF8, "application/json")
                                    };
                                    content.Add(request.Content, item.Name);
                                    break;
                            }
                        }
                    }
                    var response = await client.PostAsync(url, content);
                    var responsestring = await response.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<T>(responsestring);
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
            }
            else
            {
                noconnected?.Invoke();
            }
            return default(T);
        }

        public async Task<TokenResponse> GetToken(string urlBase, string username, string password)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var response = await client.PostAsync("Token",
                    new StringContent(string.Format(
                    "grant_type=password&username={0}&password={1}",
                    username, password),
                    Encoding.UTF8, "application/x-www-form-urlencoded"));
                var resultJSON = await response.Content.ReadAsStringAsync();
                var result = JsonConvert.DeserializeObject<TokenResponse>(
                    resultJSON);
                return result;
            }
            catch
            {
                return null;
            }
        }

        public static async Task<Stream> DownloadFile(string url, Action noconnected)
        {
            if (CrossConnectivity.Current.IsConnected)
            {
                try
                {
                    using (var stream = await FileSystem.OpenAppPackageFileAsync(url))
                    {
                        return stream;
                        //using (var reader = new StreamReader(stream))
                        //{
                        //    var fileContents = await reader.ReadToEndAsync();
                        //}
                    }
                    //var client = new HttpClient();

                    //var stream = await client.GetStreamAsync(url);
                    //return stream;

                    //var req = System.Net.WebRequest.Create(url);
                    //using (Stream stream = req.GetResponse().GetResponseStream())
                    //{
                    //    return stream;
                    //}
                    //using (HttpClient client = new HttpClient())
                    //{

                    //    var response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);

                    //    response.EnsureSuccessStatusCode();

                    //    using (var stream = await response.Content.ReadAsStreamAsync())
                    //    {
                    //        return stream;
                    //    }
                    //}
                    //var req = WebRequest.CreateHttp(url);
                    //using (var resp = await req.GetResponseAsync())
                    //{
                    //    using (var s = resp.GetResponseStream())
                    //    {

                    //        return s;
                    //    }
                    //using (var sr = new StreamReader(s))
                    //using (var j = new JsonTextReader(sr))
                    //{
                    //    var serializer = new JsonSerializer();
                    //    //do some deserializing http://www.newtonsoft.com/json/help/html/Performance.htm
                    //}
                    //}


                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData(ex));
                }
            }
            else
            {
                noconnected?.Invoke();
            }
            return null;
        }

        public static async Task<Stream> Download(string url, Action noconnected)
        {
            return await DownloadFile(url, noconnected);
        }
    }

    public class Param
    {
        public object Element { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public ParamType Type { get; set; }

        public byte[] Bytes
        {
            get
            {
                if (Type == ParamType.File)
                {
                    if (Element is byte[])
                    {
                        return Element as byte[];
                    }
                    else if (Element is Stream)
                    {
                        var streamcontent = Element as Stream;
                        return ReadFully(streamcontent);
                    }
                }
                return null;
            }
        }

        public string ContentType { get; set; }

        public Param(string name, object value, ParamType type = ParamType.String, string filename = "file.jpg", string contenttype = "application/json")
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new Exception("El parametro NAME es null o esta vacio");
            }
            Element = value ?? throw new Exception("El parametro VALUE es null");
            Name = name;
            FileName = filename;
            Type = type;
            ContentType = contenttype;
        }

        private byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }

    public enum ParamType
    {
        File, String, Obj
    }
}