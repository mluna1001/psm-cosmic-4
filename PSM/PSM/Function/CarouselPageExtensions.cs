﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PSM.Function
{
    public static class CarouselPageExtensions
    {
        public static void ShowNext(this CarouselPage carousel)
        {
            var pageCount = carousel.Children.Count;
            if (pageCount < 2) return;
            var index = carousel.Children.IndexOf(carousel.CurrentPage);
            index++;
            if (index >= pageCount) index = 0;
            carousel.CurrentPage = carousel.Children[index];
        }

        public static void ShowPrevious(this CarouselPage carousel)
        {
            var pageCount = carousel.Children.Count;
            if (pageCount < 2) return;
            var index = carousel.Children.IndexOf(carousel.CurrentPage);
            index--;
            if (index <= 0) index = 0;
            carousel.CurrentPage = carousel.Children[index];
        }
    }
}