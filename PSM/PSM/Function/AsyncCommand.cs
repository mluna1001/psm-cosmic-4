﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Function
{
    public class AsyncCommand : AsyncCommandBase
    {
        private readonly Func<Task> _command;
        private Func<object, Task> btnBajarTodo_Click;

        public AsyncCommand(Func<Task> command)
        {
            _command = command;
        }

        public AsyncCommand(Func<object, Task> btnBajarTodo_Click)
        {
            this.btnBajarTodo_Click = btnBajarTodo_Click;
        }

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override Task ExecuteAsync(object parameter)
        {
            return _command();
        }
    }
}
