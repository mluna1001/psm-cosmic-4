﻿using PSM.Models;
using System.Collections.Generic;

namespace PSM.Function
{
    public class RespuestaAuditorComplete
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public List<RespuestaAuditor> Respuestas { get; set; }
        public bool Edit { get; internal set; }
    }
}