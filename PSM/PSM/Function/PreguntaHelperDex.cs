﻿using Cosmic;
using PSM.Models;
using PSM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSM.Function
{
    public class PreguntaHelperDex
    {
        /// <summary>
        /// Preguntas      1er nivel
        /// </summary>
        /// <returns></returns>
        public static List<PreguntaModel> GetPreguntas()
        {
            List<Pregunta> preguntaspadre = new List<Pregunta>();
            List<PreguntaModel> preguntas = new List<PreguntaModel>();

            var iTienda = App.DB.InfoTienda
                                .FirstOrDefault(i => i.IdTienda == App.CurrentRoute.IdTienda);
            var db_preguntacadena = App.DB.PreguntaCadena
                                .Where(pc =>
                                    pc.IdCadena == iTienda.IdCadena ||
                                        (pc.IdFormato != null && pc.IdFormato == iTienda.IdFormato))
                                .ToList();
            /////////////////////////////////////////////////////////////////
            List<Pregunta> preguntasindb = new List<Pregunta>();
            Seccion seccion = App.CurrentSection;

            // Obtenemos las preguntas de la sección seleccionada
            if (db_preguntacadena.Any())
            {
                preguntasindb = (from p in App.DB.Pregunta.Where(e => e.Vigencia == true && e.IdSeccion == seccion.IdSeccion)
                                 join pcad in db_preguntacadena on p.IdPregunta equals pcad.IdPregunta
                                 select p).ToList();
            }
            else
            {
                preguntasindb = App.DB.Pregunta
                                        .Where(e => e.Vigencia == true && e.IdSeccion == seccion.IdSeccion)
                                        .ToList();
            }
            //var preguntasDB = App.DB.RespuestaPreguntaHija.ToList();

            // obtenemos todas las preguntas padre primero
            preguntaspadre = preguntasindb
                                .Where(e => e.IdPregunta == e.IdParent
                                        // && e.TieneHijos
                                        )
                                .OrderBy(e => e.Numero)
                                .ToList();
            if (preguntaspadre.Count == 0)
            {
                preguntaspadre = preguntasindb;
            }

            var respuestapreguntaHijaDB = App.DB.RespuestaPreguntaHija.ToList();



            //preguntaspadre = preguntaspadre
            //    .Where(s => // s.TieneHijos && 
            //                !respuestapreguntaHijaDB
            //                    .Select(d => d.IdPregunta)
            //                    .Contains(s.IdPregunta))
            //    .ToList();



            foreach (var pregunta in preguntaspadre)
            {
                try
                {
                    Respuesta respuestaindb = null;
                    List<RespuestaModel> respuestasinDB = new List<RespuestaModel>();

                    if (pregunta.IdControl == 1 || pregunta.IdControl == 4 || pregunta.IdControl == 5 || pregunta.IdControl == 6 || pregunta.IdControl == 7)
                    {
                        respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.IdPregunta == pregunta.IdPregunta);
                    }
                    else if (pregunta.IdControl == 2 || pregunta.IdControl == 3)
                    {
                        var respuestasDinDB = App.DB.Respuesta
                            .Where(r => r.IdPregunta == pregunta.IdPregunta)
                            .ToList().To<List<Respuesta>>();
                        foreach (var respuestaDB in respuestasDinDB.Where(s => s.Desencadena))
                        {
                            var prhList = respuestapreguntaHijaDB
                                            .Where(s => s.IdRespuesta == respuestaDB.IdRespuesta)
                                            .ToList();
                            if (prhList != null)
                            {
                                var respuesta = respuestaDB.To<RespuestaModel>();
                                //foreach (var prhItem in prhList)
                                //{
                                var pmodel = new PreguntaModel
                                {
                                    //Id = prhItem.IdPregunta,
                                    IdRespuesta = respuestaDB.IdRespuesta,
                                    PreguntaPadre = new Pregunta
                                    {
                                        IdPregunta = respuestaDB.IdPregunta,
                                        //IdParent = respuestaDB.IdPregunta, // prhItem.IdPregunta
                                    }
                                };
                                PreguntasHijas(pmodel);
                                respuesta.PreguntasHijas = pmodel.PreguntasHijas;
                                //}
                                respuestasinDB.Add(respuesta);
                            }
                        }
                        respuestasinDB.AddRange(
                                respuestasDinDB
                                    .Where(s => !s.Desencadena)
                                    .To<List<RespuestaModel>>());
                    }

                    var preguntamodel = new PreguntaModel
                    {
                        IdPregunta = pregunta.IdPregunta,
                        IdControl = pregunta.IdControl,
                        IdSeccion = App.CurrentSection.IdSeccion,
                        PreguntaPadre = pregunta,
                        IdRespuesta = respuestaindb != null ? respuestaindb.IdRespuesta : 0,
                        RespuestasModel = respuestasinDB,
                        Respuestas = respuestasinDB.To<List<Respuesta>>()
                    };
                    // obtenemos las preguntas hijas de la pregunta actual
                    PreguntasHijas(preguntamodel);
                    preguntas.Add(preguntamodel);
                }
                catch (Exception ex)
                {

                }
            }
            return preguntas;
        }

        /// <summary>
        /// Inyecta a la Pregunta, las preguntas hijas
        /// </summary>
        /// <param name="padre"></param>
        private static void PreguntasHijas(PreguntaModel padre)
        {
            List<PreguntaModel> preguntashijas = new List<PreguntaModel>();
            // buscamos las preguntas hijas de esa sección
            Seccion seccion = App.CurrentSection;

            //var HIJAS = App.DB.Pregunta
            //                    .ToList();
            var phijas = (from e in App.DB.Pregunta
                          join h in App.DB.RespuestaPreguntaHija on e.IdPregunta equals h.IdPregunta
                          where e.Vigencia == true && e.IdSeccion == seccion.IdSeccion &&
                                h.IdRespuesta == padre.IdRespuesta
                          select e)
                            .ToList();

            var hijas = phijas.OrderBy(e => e.Numero).To<List<Pregunta>>().ToList();
            // iteramos las preguntas hijas del pladre
            foreach (var pregunta in hijas)
            {
                Respuesta respuestaindb = null;
                List<RespuestaModel> respuestasinDB = new List<RespuestaModel>();
                var respuestapreguntaHijaDB = App.DB.RespuestaPreguntaHija.ToList();
                if (pregunta.IdControl == 1 || pregunta.IdControl == 4 || pregunta.IdControl == 5 || pregunta.IdControl == 6 || pregunta.IdControl == 7)
                {
                    respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.IdPregunta == pregunta.IdPregunta);
                }
                else if (pregunta.IdControl == 2 || pregunta.IdControl == 3)
                {
                    var respuestasDinDB = App.DB.Respuesta.Where(r => r.IdPregunta == pregunta.IdPregunta).ToList().To<List<Respuesta>>();
                    foreach (var respuestaDB in respuestasDinDB.Where(s => s.Desencadena))
                    {
                        var prh = respuestapreguntaHijaDB
                                    .FirstOrDefault(s =>
                                        s.IdRespuesta == respuestaDB.IdRespuesta);
                        var respuesta = respuestaDB.To<RespuestaModel>();
                        if (prh != null)
                        {
                            var pmodel = new PreguntaModel
                            {
                                Id = prh.IdPregunta,
                                IdRespuesta = respuestaDB.IdRespuesta,
                                PreguntaPadre = new Pregunta
                                {
                                    IdPregunta = prh.IdPregunta
                                }
                            };
                            PreguntasHijas(pmodel);
                            respuesta.PreguntasHijas = pmodel.PreguntasHijas;
                        }
                        else
                            respuesta.PreguntasHijas = new List<PreguntaModel>();
                        respuestasinDB.Add(respuesta);
                    }
                    respuestasinDB.AddRange(
                            respuestasDinDB
                                .Where(s => !s.Desencadena)
                                .To<List<RespuestaModel>>());
                }
                var model = new PreguntaModel
                {
                    IdPregunta = pregunta.IdPregunta,
                    IdControl = pregunta.IdControl,
                    IdSeccion = App.CurrentSection.IdSeccion,
                    PreguntaPadre = pregunta,
                    IdRespuesta = respuestaindb != null ? respuestaindb.IdRespuesta : 0,
                    RespuestasModel = respuestasinDB,
                    Respuestas = respuestasinDB.To<List<Respuesta>>(),
                    Id = padre.Id
                };
                // obtenemos las preguntas hijas de la pregunta actual
                PreguntasHijas(model);
                // agregamos la pregunta hija al padre
                padre.PreguntasHijas.Add(model);
            }
        }

        public static PreguntaModel GetPreguntasModuloPhoto()
        {
            PreguntaModel pregunta = null;
            // obtenemos todas las preguntas padre primero
            var preguntasindb = App.DB.Pregunta.ToList();
            Seccion seccion = App.CurrentSection;
            var preguntaspadre = App.DB.Pregunta.FirstOrDefault(e => e.Vigencia == true && e.IdSeccion == seccion.IdSeccion);



            if (preguntaspadre != null)
            {
                Respuesta respuestaindb = null;
                List<Respuesta> respuestasinDB = new List<Respuesta>();
                respuestaindb = App.DB.Respuesta.FirstOrDefault(r => r.IdPregunta == preguntaspadre.IdPregunta);

                pregunta = new PreguntaModel
                {
                    IdPregunta = preguntaspadre.IdPregunta,
                    IdControl = preguntaspadre.IdControl,
                    IdSeccion = App.CurrentSection.IdSeccion,
                    PreguntaPadre = preguntaspadre,
                    IdRespuesta = respuestaindb != null ? respuestaindb.IdRespuesta : 0,
                    RespuestasModel = respuestasinDB.To<List<RespuestaModel>>()
                };
            }

            return pregunta;
        }

        public static void GetRespuestasPregunta(List<PreguntaModel> _models, List<RespuestaAuditor> respuestaAuditor, List<PreguntaModel> presentacionpregunta, Producto presentacion, bool typepage, int indice)
        {
            foreach (var model in _models)
            {
                var preguntamodel = new PreguntaModel
                {
                    Indice = indice,
                    IdPregunta = model.IdPregunta,
                    PreguntaPadre = model.PreguntaPadre,
                    PreguntasHijas = model.PreguntasHijas.To<List<PreguntaModel>>(),
                    IdSeccion = model.IdSeccion,
                    IdRespuesta = model.IdRespuesta,
                    Desencadenada = model.Desencadenada,
                    File = model.File,
                    FileName = model.FileName,
                    Respuesta = model.Respuesta,
                    Respuestas = model.Respuestas,
                    RespuestasModel = model.RespuestasModel,
                    View = model.View,
                    IdControl = model.IdControl
                };
                if (typepage)
                {
                    preguntamodel.Id = presentacion.IdProducto;
                }

                RespuestaAuditor respuestaauditor = null;
                List<RespuestaAuditor> respuestaauditorlist = new List<RespuestaAuditor>();

                if (model.IdControl == 1 || model.IdControl == 4 || model.IdControl == 5 || model.IdControl == 7 || model.IdControl == 6)
                {
                    if (typepage)
                    {
                        respuestaauditor = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == preguntamodel.IdRespuesta && ra.IdProducto == preguntamodel.Id);
                    }
                    else
                    {
                        respuestaauditor = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == preguntamodel.IdRespuesta);
                    }

                }
                else if (model.IdControl == 2 || model.IdControl == 3)
                {
                    var Respuestas = model.Respuestas.Where(r => r.IdPregunta == preguntamodel.IdPregunta).Select(r => r.IdRespuesta).ToList();

                    foreach (var item in Respuestas)
                    {
                        if (typepage)
                        {
                            var respuestaAudit = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);

                            if (respuestaAudit != null)
                            {
                                if (model.IdControl == 2)
                                {
                                    var respuesta = respuestaAuditor.Where(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id).ToList();
                                    respuestaauditorlist.AddRange(respuesta);
                                }
                                else
                                {
                                    var respuesta = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);
                                    if (respuesta != null)
                                    {
                                        respuestaauditor = respuesta;
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            var respuestaAudit = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item);

                            if (respuestaAudit != null)
                            {
                                if (model.IdControl == 2)
                                {
                                    var respuesta = respuestaAuditor.Where(ra => ra.IdRespuesta == item).ToList();
                                    respuestaauditorlist.AddRange(respuesta);
                                }
                                else
                                {
                                    var respuesta = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item);
                                    if (respuesta != null)
                                    {
                                        respuestaauditor = respuesta;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                //Este caso aplica para las repuestas tipo checkbox ya que se pueden tener varias respuestas para una misma pregunta
                if (respuestaauditorlist.Count() != 0)
                {

                    //preguntamodel.Respuesta = respuestaauditor.Respuesta;
                    foreach (var item in preguntamodel.Respuestas)
                    {
                        var b = respuestaauditorlist.FirstOrDefault(s => s.IdRespuesta == item.IdRespuesta);
                        if (b != null)
                            item.Valor = double.Parse(b.Respuesta);

                        if (b != null)
                        {
                            item.IdRespuestaAuditor = b.IdAnswerAudit;
                        }
                    }
                    //preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                    /*preguntamodel.IdRespuestaAuditor = b*/;
                }

                if (respuestaauditor != null)
                {
                    preguntamodel.Respuesta = respuestaauditor.Respuesta;
                    preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                    preguntamodel.IdRespuestaAuditor = respuestaauditor.IdAnswerAudit;
                    preguntamodel.FileName = respuestaauditor.FotoNombre;
                    preguntamodel.FotoEdit = respuestaauditor.Foto;
                }

                GetRespuestasPreguntaHijas(preguntamodel, respuestaAuditor, typepage);
                presentacionpregunta.Add(preguntamodel);

                indice++;
            }
        }

        public static void GetRespuestasPreguntaHijas(PreguntaModel model, List<RespuestaAuditor> respuestaAuditor, bool typepage)
        {

            //PreguntaModel pm = new PreguntaModel();
            //pm = model.To<PreguntaModel>();

            foreach (var rm in model.RespuestasModel)
            {
                foreach (var preguntamodel in rm.PreguntasHijas)
                {
                    RespuestaAuditor respuestaauditor = null;
                    preguntamodel.Id = model.Id;
                    List<RespuestaAuditor> respuestaauditorlist = new List<RespuestaAuditor>();

                    if (preguntamodel.IdControl == 1 || preguntamodel.IdControl == 4 || preguntamodel.IdControl == 5 || preguntamodel.IdControl == 7 || preguntamodel.IdControl == 6)
                    {
                        if (typepage)
                        {
                            respuestaauditor = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == preguntamodel.IdRespuesta && ra.IdProducto == preguntamodel.Id);
                        }
                        else
                        {
                            respuestaauditor = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == preguntamodel.IdRespuesta);
                        }
                    }
                    else if (preguntamodel.IdControl == 2 || preguntamodel.IdControl == 3)
                    {
                        var Respuestas = preguntamodel.Respuestas.Where(r => r.IdPregunta == preguntamodel.IdPregunta).Select(r => r.IdRespuesta).ToList();
                     
                        foreach (var item in Respuestas)
                        {
                            if (typepage)
                            {
                                var respuestaAudit = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);

                                if (respuestaAudit != null)
                                {
                                    if (model.IdControl == 2)
                                    {
                                        var respuesta = respuestaAuditor.Where(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id).ToList();
                                        respuestaauditorlist.AddRange(respuesta);
                                    }
                                    else
                                    {
                                        var respuesta = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item && ra.IdProducto == preguntamodel.Id);
                                        if (respuesta != null)
                                        {
                                            respuestaauditor = respuesta;
                                            break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var respuestaAudit = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item);

                                if (respuestaAudit != null)
                                {
                                    if (model.IdControl == 2)
                                    {
                                        var respuesta = respuestaAuditor.Where(ra => ra.IdRespuesta == item).ToList();
                                        respuestaauditorlist.AddRange(respuesta);
                                    }
                                    else
                                    {
                                        var respuesta = respuestaAuditor.FirstOrDefault(ra => ra.IdRespuesta == item);
                                        if (respuesta != null)
                                        {
                                            respuestaauditor = respuesta;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Este caso aplica para las repuestas tipo checkbox ya que se pueden tener varias respuestas para una misma pregunta
                    if (respuestaauditorlist.Count() != 0)
                    {

                        //preguntamodel.Respuesta = respuestaauditor.Respuesta;
                        foreach (var item in preguntamodel.Respuestas)
                        {
                            var b = respuestaauditorlist.FirstOrDefault(s => s.IdRespuesta == item.IdRespuesta);
                            if (b != null)
                                item.Valor = double.Parse(b.Respuesta);

                            if (b != null)
                            {
                                item.IdRespuestaAuditor = b.IdAnswerAudit;
                            }
                        }
                        //preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                        /*preguntamodel.IdRespuestaAuditor = b*/;
                    }

                    if (respuestaauditor != null)
                    {
                        preguntamodel.Respuesta = respuestaauditor.Respuesta;
                        preguntamodel.IdRespuesta = respuestaauditor.IdRespuesta;
                        preguntamodel.IdRespuestaAuditor = respuestaauditor.IdAnswerAudit;
                        preguntamodel.FileName = respuestaauditor.FotoNombre;
                        preguntamodel.FotoEdit = respuestaauditor.Foto;
                    }
                    GetRespuestasPreguntaHijas(preguntamodel, respuestaAuditor, typepage);
                }
            }
        }
    }
}