﻿using System;

namespace PSM.Function
{
    public class DistanceHelper
    {
        public static double Radianes(double grados)
        {
            return grados * Math.PI / 180;
        }

        public static double Grados(double radianes)
        {
            return radianes * 180 / Math.PI;
        }
    }
}