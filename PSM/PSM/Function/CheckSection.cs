﻿using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Function
{
    public class CheckSection
    {
        /// <summary>
        /// Metodo para asignar el estatus de la bandera en Ruta
        /// 1.- Pendiente por contestar.
        /// 2.- Cuestionario de seccion Contestado.
        /// 3.- Cuestionario de incidencias Contestado.
        /// </summary>
        /// <param name="respuestaAuditor"></param>
        /// <returns></returns>
        public static void CheckSectionRoute(int idSeccion, int idRuta)
        {
            //int idSeccion = respuestaAuditor.FirstOrDefault().IdSeccion;
            bool esSeccion = App.DB.Seccion.FirstOrDefault(s => s.IdSeccion == idSeccion).EsSeccion;
            //int idRuta = respuestaAuditor.FirstOrDefault().IdRuta;
            Ruta ruta = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == idRuta);
            if (esSeccion)
            {
                ruta.Status = 2;
            }
            else
            {
                ruta.Status = 3;
            }
            App.DB.Update(ruta);
        }
    }
}
