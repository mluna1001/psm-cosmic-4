﻿using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Function
{
    public class DownloadData
    {
        public static async Task<bool> BajaTodo(bool ShowAlert)
        {
            bool rutasuccess, preguntasuccess = false, catalogosuccess = false; bool qualificationsucces = false;

            rutasuccess = await BajarRuta(ShowAlert);
            if (rutasuccess) preguntasuccess = await BajarPreguntas(ShowAlert);
            if (preguntasuccess) catalogosuccess = await BajarCatalogos(ShowAlert);

            var r = App.DB.Ruta;
            if (r.Any())
            {
                bool be;
                bool sendRate = bool.TryParse(r.FirstOrDefault().SendQual.ToString(), out be);
                if (sendRate)
                {
                    if (be)
                    {
                        if (catalogosuccess) qualificationsucces = await BajarCalificacion();
                    }
                }

            }

            if ((rutasuccess && preguntasuccess && catalogosuccess) || qualificationsucces)
                return true;

            return false;
        }

        public static async Task<bool> BajarCatalogos(bool ShowAlert)
        {
            bool status = false;
            try
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BajarCatalogos", "DownloadData", "Inicia bajar catalogos/todo");

                bool companias = false;
                bool productos = false;
                bool ubicaciones = false;

                var chatgrupos = await App.API.ChatEndPoint.Grupos(App.CurrentUser.IdProyecto, App.CurrentUser.IdUsuario);
                if (chatgrupos != null && chatgrupos.Count > 0)
                {
                    foreach (var chatgrupo in chatgrupos)
                    {
                        var chatitem = App.DB.ChatGrupo.FirstOrDefault(e => e.IdChatGrupo == chatgrupo.IdChatGrupo);
                        if (chatitem == null)
                        {
                            App.DB.Add(chatgrupo);
                        }
                        else
                        {
                            chatitem.Baneado = chatgrupo.Baneado;
                            App.DB.Entry(chatitem).State = DataBase.EntryState.Modify;
                            App.DB.SaveChanges();
                        }
                        App.DB.SaveChanges();
                    }
                }
                bool isconnected = false;
                var catalogoresponse = await App.API.CatalogoEndPoint.UserCatalogs(App.CurrentUser.IdUsuario, async () =>
                {
                    isconnected = false;
                    //if (ShowAlert)
                    //{
                    //    await page.DisplayAlert("PSM 360", "No tienes conexion a internet, intenta mas tarde", "Aceptar");
                    //    Bitacora.Insert("Descargando catalogos", "No tienes conexion a internet, intenta mas tarde");
                    //}
                });
                if (catalogoresponse != null && catalogoresponse.Success)
                {
                    if (catalogoresponse.Companias != null)
                    {
                        foreach (var item in catalogoresponse.Companias)
                        {
                            if (!App.DB.Compania.Exists(e => e.IdCompania == item.IdCompania))
                            {
                                App.DB.Add(item);
                            }
                        }
                        companias = App.DB.SaveChanges();
                    }

                    if (catalogoresponse.Ubicaciones != null)
                    {
                        foreach (var item in catalogoresponse.Ubicaciones)
                        {
                            if (!App.DB.Ubicacion.Exists(e => e.IdUbicacion == item.IdUbicacion))
                            {
                                App.DB.Add(item);
                            }
                        }
                        ubicaciones = App.DB.SaveChanges();
                    }

                    if (catalogoresponse.Abordajes != null)
                    {
                        foreach (var item in catalogoresponse.Abordajes)
                        {
                            if (!App.DB.CatalogoSeccionMenu.Exists(e => e.IdSeccionMenu == item.IdSeccionMenu))
                            {
                                App.DB.Add(item);
                            }
                        }
                        App.DB.SaveChanges();
                    }

                    if (catalogoresponse.Archivos != null)
                    {
                        int idb = App.DB.Multimedia.Sum(s => s.IdMultimedia);
                        int srv = catalogoresponse.Archivos.Sum(s => s.IdMultimedia);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM Multimedia");
                            foreach (var item in catalogoresponse.Archivos)
                            {
                                if (!App.DB.Multimedia.Exists(e => e.IdMultimedia == item.IdMultimedia))
                                {
                                    App.DB.Add(item);
                                }
                            }
                            App.DB.SaveChanges();
                        }
                    }

                    if (catalogoresponse.Productos != null)
                    {
                        int idb = App.DB.Producto.Sum(s => s.IdProducto);
                        int srv = catalogoresponse.Productos.Sum(s => s.IdProducto);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM Producto");
                            foreach (var item in catalogoresponse.Productos)
                            {
                                if (!App.DB.Producto.Exists(e => e.IdProducto == item.IdProducto))
                                {
                                    App.DB.Add(item);
                                }
                            }
                            productos = App.DB.SaveChanges();
                        }
                    }

                    if (catalogoresponse.ProductoCadena != null)
                    {
                        int idb = App.DB.ProductoCadena.Sum(s => s.IdProductoCadena);
                        int srv = catalogoresponse.ProductoCadena.Sum(s => s.IdProductoCadena);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM ProductoCadenaDto");
                            foreach (var item in catalogoresponse.ProductoCadena)
                            {
                                if (!App.DB.ProductoCadena.Exists(e => e.IdProductoCadena == item.IdProductoCadena))
                                {
                                    App.DB.Add(item);
                                }
                            }
                            App.DB.SaveChanges();
                        }
                    }

                    if (catalogoresponse.Tiempos != null)
                    {
                        App.DB.Execute("DELETE FROM Tiempo");
                        foreach (var item in catalogoresponse.Tiempos)
                        {
                            App.DB.Add(item);
                        }
                        App.DB.SaveChanges();
                    }

                    if (catalogoresponse.ProductoClasificacion != null)
                    {
                        App.DB.Execute("DELETE FROM ProductoClasificacion");
                        foreach (var item in catalogoresponse.ProductoClasificacion)
                        {
                            App.DB.Add(item);
                        }
                        App.DB.SaveChanges();
                    }
                    if (ShowAlert)
                    {
                        //await App.Current.MainPage.DisplayAlert("PSM", "Se han descargado los catalogos", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCatalogos", "DownloadData", "Se han descargado los catalogos");
                    }
                    status = true;
                }
                else
                {
                    if (isconnected) if (ShowAlert)
                        {
                            //await App.Current.MainPage.DisplayAlert("PSM", "No fue posible descargar las tiendas, intenta mas tarde", "Aceptar");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCatalogos", "DownloadData", "No fue posible descargar los catalogos, intenta mas tarde");
                        }
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "EndDownload", "DownloadData", "Termina descarga de catalogos/todo");
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public static async Task<bool> BajarMultimedia(bool ShowAlert)
        {
            bool status = false;
            try
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "BajarCatalogos", "DownloadData", "Inicia bajar catalogos/todo");

                //bool companias = false;
                //bool productos = false;
                //bool ubicaciones = false;
                bool isconnected = false;
                var catalogoresponse = await App.API.CatalogoEndPoint.UserCatalogs(App.CurrentUser.IdUsuario, async () =>
                {
                    isconnected = false;
                    if (ShowAlert)
                    {
                        MessageService.ShowMessage("No tienes conexion a internet, intenta mas tarde");
                        //Bitacora.Insert("Descargando catalogos", "No tienes conexion a internet, intenta mas tarde");
                    }
                });
                if (catalogoresponse.Archivos != null)
                {
                    int idb = App.DB.Multimedia.Sum(s => s.IdMultimedia);
                    int srv = catalogoresponse.Archivos.Sum(s => s.IdMultimedia);
                    if (idb != srv)
                    {
                        foreach (var item in catalogoresponse.Archivos)
                        {
                            var fileDB = App.DB.Multimedia.FirstOrDefault(e => e.IdMultimedia == item.IdMultimedia);
                            if (fileDB == null)
                            {
                                App.DB.Add(item);
                            }
                            else
                            {

                            }
                        }
                        App.DB.SaveChanges();

                        if (ShowAlert)
                        {
                            status = true;
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarMultimedia", "DownloadData", "Se han descargado las preguntas");
                        }
                    }
                    else
                    {
                        status = true;
                        MessageService.ShowMessage("No hay archivos multimedia nuevos por descargar");
                    }
                }
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public static async Task<bool> BajarPreguntas(bool ShowAlert)
        {
            bool status = false;
            try
            {
                StartDownload("Descargando preguntas");

                bool catalogoseccion = false;
                bool preguntas = false;
                bool respuestas = false;
                bool ispreguntasrespuestas = false;
                bool secciones = false;

                bool isconnected = true;
                var sectionresponse = await App.API.SectionEndPoint.UserSections(App.CurrentUser.IdProyecto, async () =>
                {
                    isconnected = false;
                    if (ShowAlert)
                    {
                        MessageService.ShowMessage("No tienes conexion a internet, intenta mas tarde");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarPreguntas", "DownloadData", "No tienes conexion a internet, intenta mas tarde");
                    }
                });

                if (sectionresponse != null)
                {
                    if (sectionresponse.ClasificacionSeccion != null)
                    {
                        foreach (var item in sectionresponse.ClasificacionSeccion)
                        {
                            if (!App.DB.CatalogoSeccion.Exists(e => e.IdCatalogoSeccion == item.IdCatalogoSeccion))
                            {
                                App.DB.CatalogoSeccion.Add(item);
                            }
                        }
                        catalogoseccion = App.DB.SaveChanges();
                        App.ShowBtnMultimedia = App.DB.CatalogoSeccion.Any(s => s.IdCatalogoSeccion == 10);
                    }

                    if (sectionresponse.Preguntas != null)
                    {
                        int controlPregSrv = sectionresponse.Preguntas.Sum(p => p.IdPregunta);
                        int controlPregLocal = App.DB.Pregunta.Sum(p => p.IdPregunta);

                        if (controlPregLocal != controlPregSrv)
                        {
                            App.DB.Execute("DELETE FROM Pregunta");
                            App.DB.Execute("VACUUM");
                            App.DB.SaveChanges();

                            foreach (var pregunta in sectionresponse.Preguntas)
                            {
                                if (!App.DB.Pregunta.Exists(e => e.IdPregunta == pregunta.IdPregunta))
                                {
                                    App.DB.Add(pregunta);
                                }
                            }
                            preguntas = App.DB.SaveChanges(); 
                        }
                    }
                
                    if (sectionresponse.RespuestaPreguntaHija != null)
                    {
                        int idb = App.DB.RespuestaPreguntaHija.Sum(s => s.IdPregunta);
                        int srv = sectionresponse.RespuestaPreguntaHija.Sum(s => s.IdPregunta);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM RespuestaPreguntaHija");
                            App.DB.Execute("VACUUM");
                            foreach (var respreg in sectionresponse.RespuestaPreguntaHija)
                            {
                                if (!App.DB.RespuestaPreguntaHija.Exists(e => e.IdPregunta == respreg.IdPregunta && e.IdRespuesta == respreg.IdRespuesta))
                                {
                                    App.DB.Add(respreg);
                                }
                            }
                            ispreguntasrespuestas = App.DB.SaveChanges();
                        }
                    }

                    if (sectionresponse.Secciones != null)
                    {
                        int controlSectSrv = sectionresponse.Secciones.Sum(p => p.IdSeccion);
                        int controlSectLocal = App.DB.Seccion.Sum(p => p.IdSeccion);

                        if (controlSectLocal != controlSectSrv)
                        {
                            App.DB.Execute("DELETE FROM Seccion");
                            App.DB.Execute("VACUUM");
                            App.DB.SaveChanges();

                            foreach (var section in sectionresponse.Secciones)
                            {
                                if (!App.DB.Seccion.Exists(e => e.IdSeccion == section.IdSeccion))
                                {
                                    App.DB.Add(section);
                                }
                            }
                            secciones = App.DB.SaveChanges(); 
                        }
                    }

                    if (sectionresponse.Respuestas != null)
                    {
                        int controlRespSrv = sectionresponse.Respuestas.Sum(p => p.IdRespuesta);
                        int controlRespLocal = App.DB.Respuesta.Sum(p => p.IdRespuesta);

                        if (controlRespLocal != controlRespSrv)
                        {
                            App.DB.Execute("DELETE FROM Respuesta");
                            App.DB.Execute("VACUUM");
                            App.DB.SaveChanges();

                            foreach (var respuesta in sectionresponse.Respuestas)
                            {
                                if (!App.DB.Respuesta.Exists(e => e.IdRespuesta == respuesta.IdRespuesta))
                                {
                                    App.DB.Add(respuesta);
                                }
                            }
                            respuestas = App.DB.SaveChanges(); 
                        }
                    }

                    if (sectionresponse.SeccionCadena != null)
                    {
                        int idb = App.DB.SeccionCadena.Sum(s => s.IdSeccionCadena);
                        int srv = sectionresponse.SeccionCadena.Sum(s => s.IdSeccionCadena);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM SeccionCadena");
                            App.DB.Execute("VACUUM");
                            foreach (var seccionCadena in sectionresponse.SeccionCadena)
                            {
                                if (!App.DB.SeccionCadena.Exists(e => e.IdSeccionCadena == seccionCadena.IdSeccionCadena))
                                {
                                    App.DB.Add(seccionCadena);
                                }
                            }
                            App.DB.SaveChanges();
                        }
                    }

                    if (sectionresponse.SeccionRegion != null)
                    {
                        int idb = App.DB.SeccionRegion.Sum(s => s.IdSeccionRegion);
                        int srv = sectionresponse.SeccionRegion.Sum(s => s.IdSeccionRegion);
                        if (idb != srv)
                        {
                            App.DB.Execute("DELETE FROM SeccionRegion");
                            App.DB.Execute("VACUUM");
                            foreach (var seccionRegion in sectionresponse.SeccionRegion)
                            {
                                if (!App.DB.SeccionRegion.Exists(e => e.IdSeccionRegion == seccionRegion.IdSeccionRegion))
                                {
                                    App.DB.Add(seccionRegion);
                                }
                            }
                            App.DB.SaveChanges();
                        }
                    }

                    if (ShowAlert)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BtnBajarPreguntas_Click", "DownloadPage", "Se han descargado las preguntas");
                    }
                    status = true;
                }
                else
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarPreguntas", "DownloadData", "No fue posible descargar las preguntas, intenta mas tarde");
                }
                EndDownload();
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public static async Task<bool> BajarRuta(bool ShowAlert)
        {
            bool status = false;
            try
            {
                StartDownload("Descargando ruta");
                bool isconnected = true;
                var routeresponse = await App.API.RouteEndPoint.UserRoutes(App.CurrentUser.IdUsuario, async () =>
                {
                    isconnected = false;
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarRuta", "DownloadData", "No tienes conexión a internet, intenta mas tarde");
                });
                if (routeresponse != null)
                {
                    if (routeresponse.Rutas != null && routeresponse.Rutas.Count > 0)
                    {
                        foreach (var route in routeresponse.Rutas)
                        {
                            if (!App.DB.Ruta.Exists(e => e.IdRuta == route.IdRuta))
                            {
                                App.DB.Add(route);
                                App.DB.SaveChanges();
                            }
                        }
                    }

                    if (routeresponse.Informacion != null && routeresponse.Informacion.Count > 0)
                    {
                        foreach (var infotienda in routeresponse.Informacion)
                        {
                            if (!App.DB.InfoTienda.Exists(e => e.IdTienda == infotienda.IdTienda))
                            {
                                App.DB.Add(infotienda);
                            }
                        }
                        App.DB.SaveChanges();
                    }

                    if (routeresponse.Indicadores != null && routeresponse.Indicadores.Count > 0)
                    {
                        App.DB.Execute("DELETE FROM Indicador");
                        App.DB.Execute("VACUUM");
                        App.DB.Execute("VACUUM");
                        App.DB.SaveChanges();

                        foreach (var indicador in routeresponse.Indicadores)
                        {
                            App.DB.Add(indicador);
                        }
                        App.DB.SaveChanges();
                    }

                    if (routeresponse.Tiempos != null)
                    {
                        App.DB.Execute("DELETE FROM Tiempo");
                        App.DB.Execute("VACUUM");
                        foreach (var item in routeresponse.Tiempos)
                        {
                            App.DB.Add(item);
                        }
                        App.DB.SaveChanges();
                    }

                    if (routeresponse.CatalogoIndicador != null && routeresponse.CatalogoIndicador.Count > 0)
                    {
                        App.DB.Execute("delete from CatalogoIndicador");
                        App.DB.Execute("VACUUM");

                        foreach (var catalogoindicador in routeresponse.CatalogoIndicador)
                        {
                            App.DB.Add(catalogoindicador);
                        }
                        App.DB.SaveChanges();
                    }

                    if (routeresponse.ReporteIndicador != null && routeresponse.ReporteIndicador.Count > 0)
                    {
                        App.DB.Execute("DELETE FROM ReporteIndicador");
                        App.DB.Execute("VACUUM");

                        foreach (var reporte in routeresponse.ReporteIndicador)
                        {
                            App.DB.Add(reporte);
                        }
                        App.DB.SaveChanges();
                    }

                    // Descarga de Tiendas Cercanas a Ruta
                    var nearbyrouteresponse = await App.API.NearbyRouteStore.NearbyRouteStores(App.CurrentUser.IdUsuario, async () =>
                    {
                        isconnected = false;
                    });

                    if (nearbyrouteresponse.Tiendas != null && nearbyrouteresponse.Tiendas.Count > 0)
                    {
                        App.DB.Execute("delete from TiendasCercanasRuta");
                        App.DB.Execute("VACUUM");

                        foreach (var item in nearbyrouteresponse.Tiendas)
                        {
                            App.DB.TiendasCercanasRuta.Add(item);
                        }
                        App.DB.SaveChanges();
                    }

                    if (ShowAlert)
                    {
                        //await App.Current.MainPage.DisplayAlert("PSM 360", "Se ha descargado tu ruta", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarRuta", "DownloadData", "Se ha descargado tu ruta");
                    }
                    status = true;
                }
                else
                {
                    if (isconnected)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarRuta", "DownloadData", "Ocurrio un error al descargar ruta, intenta mas tarde");
                        
                    }
                }
                EndDownload();
            }
            catch (Exception ex)
            {
                status = false;
            }
            return status;
        }

        public static async Task<bool> BajarCalificacion()
        {
            bool status = false;
            bool isconnected = true;
            var qualiResponse = await App.API.QualiEndPoint.QualiRoute(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
                //if (ShowAlert)
                //{
                //await DisplayAlert("PSM 360", "No tienes conexión a internet, intenta mas tarde", "Aceptar");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargandoCatalogoNoInternet, "BajarCalificacion", "DownloadPage", "No tienes conexión a internet, intenta mas tarde");
                //}
            });
            if (qualiResponse != null)
            {
                if (qualiResponse.Qualification != null && qualiResponse.Qualification.Count > 0)
                {
                    foreach (var item in qualiResponse.Qualification)
                    {
                        if (!App.DB.Qualification.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.Qualification.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiSections != null && qualiResponse.QualiSections.Count > 0)
                {
                    foreach (var item in qualiResponse.QualiSections)
                    {
                        if (!App.DB.QualiSection.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.QualiSection.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiCuestion != null && qualiResponse.QualiCuestion.Count > 0)
                {
                    App.DB.Execute("DELETE FROM QualiPregunta");
                    App.DB.Execute("VACUUM");
                    foreach (var item in qualiResponse.QualiCuestion)
                    {
                        //if (!App.DB.QualiPregunta.Exists(a => a.IdRuta == item.IdRuta))
                        //{
                        App.DB.QualiPregunta.Add(item);
                        //}
                    }
                    App.DB.SaveChanges();
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCalificacion", "DownloadPage", "Se han descargado las calificaciones");

                status = true;
            }
            else
            {
                if (isconnected)
                {
                    //if (ShowAlert)
                    //{
                    //    await DisplayAlert("PSM 360", "Ocurrio un error al descargar informacion, intenta mas tarde", "Aceptar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCalificacion", "DownloadPage", "Ocurrio un error al descargar calificacion, intenta mas tarde");
                    //}
                }
            }
            EndDownload();
            return status;
        }
        private static void StartDownload(string message)
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IniciaDescargaInformacion, "StartDownload", "DownloadPage", message);
        }
        private static void EndDownload()
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TerminaDescargaInformacion, "EndDownload", "DownloadPage");
        }
    }
}