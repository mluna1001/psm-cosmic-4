﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Controls
{
    public class VideoSource
    {
        public string TempFileName { get; private set; }
        public string FilePath { get; private set; }
        public Uri Uri { get; private set; }
        public Stream Stream { get; private set; }
        public bool IsFile { get; private set; }
        public bool IsUri { get; private set; }

        public static VideoSource FromFile(string filepath)
        {
            if (string.IsNullOrEmpty(filepath)) throw new NullReferenceException("filepath no puede ser null o empty");
            if (!Path.HasExtension(filepath))
            {
                throw new FormatException("el file path no termina en un archivo...");
            }
            return new VideoSource { FilePath = filepath, IsFile = true, IsUri = false };
        }

        public static VideoSource FromUri(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new NullReferenceException("uri no puede ser null o empty");
            Uri uri = new Uri(url, UriKind.RelativeOrAbsolute);
            return new VideoSource { Uri = uri, IsUri = true, IsFile = false };
        }

        public static VideoSource FromUri(Uri uri)
        {
            if (uri == null) throw new NullReferenceException("uri no puede ser null");
            return new VideoSource { Uri = uri, IsUri = true, IsFile = false };
        }

        public static VideoSource FromStream(Func<Stream> action, string tempfilename)
        {
            var stream = action?.Invoke();
            if (stream == null) throw new NullReferenceException("Stream no puede ser null");
            return new VideoSource { Stream = stream, IsUri = false, IsFile = false, TempFileName = tempfilename };
        }

    }
}
