﻿using Cosmic;
using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class RutaEstatusViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsVisibleStack = true;
        public bool IsVisibleStack
        {
            get { return _IsVisibleStack; }
            set { SetProperty(ref _IsVisibleStack, value); }

        }
        ObservableCollection<RutaEstatusItemViewModel> _RutaEstatusL = new ObservableCollection<RutaEstatusItemViewModel>();
        #endregion

        #region Properties
        public ObservableCollection<RutaEstatusItemViewModel> rutaEstatusL
        {
            get { return _RutaEstatusL; }
            set { SetProperty(ref _RutaEstatusL, value); }
        }
        #endregion


        #region Constructors
        public RutaEstatusViewModel()
        {
            instance = this;
            Download();
        }
        #endregion

        #region Singleton
        private static RutaEstatusViewModel instance;

        public static RutaEstatusViewModel GetInstance()
        {
            if (instance == null)
                return new RutaEstatusViewModel();
            return instance;
        }
        #endregion
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(() =>
                {
                    IsRefreshing = true;

                    Download();

                    IsRefreshing = false;
                });
            }
        }

        #region Methods
        public void Download()
        {
            var rs = getRuta();

            if (rs != null)
            {
                rutaEstatusL = rs;
            }
        }

        public ObservableCollection<RutaEstatusItemViewModel> getRuta()
        {
            var ry = App.DB.Ruta.ToList();

            var r = (from p in App.DB.Ruta.Where(p => (p.Status == 3 || p.Status == 2))
                     //join t in App.DB.Tiempo on p.IdTiempo equals t.IdTiempo
                     select new RutaEstatusItemViewModel
                     {
                         IdRuta = p.IdRuta,
                         IdTiempo = p.IdTiempo,
                         Sincronizado = p.Sincronizado,
                         TiendaNombre = p.TiendaNombre,
                         Status = p.Status,
                         //FechaRuta = t.Fecha,
                         Fecha = p.FechaTerminacion.Value.ToShortDateString(),
                         NumeroSemana = p.NumeroSemana,
                         FechaInicio = p.FechaInicio,
                         FechaTerminacion = p.FechaTerminacion,
                         Estatus = p.Sincronizado == false ? "Estatus: Cerrada" : ""
                     }).Where(p => (p.Status == 3 || p.Status == 2) && p.Sincronizado == false).OrderByDescending(s => s.FechaRuta).To<ObservableCollection<RutaEstatusItemViewModel>>();

            return r;
        }
        #endregion
    }
}
