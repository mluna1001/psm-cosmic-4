﻿using Cosmic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace PSM.ViewModels
{
    public class SemaforoViewModel : BaseViewModel
    {
        #region Attributes
        ObservableCollection<SemaforoItemViewModel> _SubordinadosL = new ObservableCollection<SemaforoItemViewModel>();
        string _FechaActual = DateTime.Now.ToShortDateString();
        #endregion

        #region Properties

        public string FechaActual
        {
            get { return _FechaActual; }
            set { SetProperty(ref _FechaActual, value); }

        }
        public ObservableCollection<SemaforoItemViewModel> subordinadosL
        {
            get { return _SubordinadosL; }
            set { SetProperty(ref _SubordinadosL, value); }
        }
        #endregion


        #region Constructors
        public SemaforoViewModel()
        {
            instance = this;
            IsBusy = true;
            Download();
            IsBusy = false;
        }
        #endregion

        #region Singleton
        private static SemaforoViewModel instance;

        public static SemaforoViewModel GetInstance()
        {
            if (instance == null)
                return new SemaforoViewModel();
            return instance;
        }
        #endregion
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    await Download();

                    IsRefreshing = false;
                });
            }
        }

        #region Methods
        public async Task Download()
        {
            bool isconnected = false;
            var consulta = await Task.Run(() => App.API.UserEndPoint.GetSubordinateUsers(() => isconnected = false));
            var sub = consulta.To<ObservableCollection<SemaforoItemViewModel>>();

            sub.ForEach(x =>
            {
                if (x.Asistencia.Count != 0)
                {
                    x.ColorCard = "#98C93C";
                }
                else
                {
                    x.ColorCard = "#F6911A";
                }
            });

            subordinadosL = sub.To<ObservableCollection<SemaforoItemViewModel>>();
        }
        #endregion
    }
}
