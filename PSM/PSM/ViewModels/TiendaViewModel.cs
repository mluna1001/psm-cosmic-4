﻿using GalaSoft.MvvmLight.Command;
using Microsoft.AppCenter.Crashes;
using PSM.Function;
using PSM.Helpers;
using PSM.Models;
using PSM.Services;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class TiendaViewModel : BaseViewModel
    {
        #region Propiedades
        bool _IsVisibleStack = true;
        public bool IsVisibleStack
        {
            get { return _IsVisibleStack; }
            set { SetProperty(ref _IsVisibleStack, value); }

        }

        ListaDias _DiaSeleccionado;
        public ListaDias DiaSeleccionado
        {
            get { return _DiaSeleccionado; }
            set { SetProperty(ref _DiaSeleccionado, value); }

        }

        private List<Ruta> _RutaList;
        public List<Ruta> RutaList
        {
            get { return _RutaList; }
            set { SetProperty(ref _RutaList, value); }
        }

        private ObservableCollection<ListaDias> _Dias;
        public ObservableCollection<ListaDias> Dias
        {
            get { return _Dias; }
            set { SetProperty(ref _Dias, value); }
        }

        private string _Dia;
        public string Dia
        {
            get { return _Dia; }
            set { SetProperty(ref _Dia, value); }
        }

        private List<Informacion> _ListaTiendas = new List<Informacion>();
        public List<Informacion> ListaTiendas
        {
            get { return _ListaTiendas; }
            set { SetProperty(ref _ListaTiendas, value); }
        }

        private bool _TiendaClick;
        public bool TiendaClick
        {
            get { return _TiendaClick; }
            set { SetProperty(ref _TiendaClick, value); }
        }

        private bool _BtnUpload_Enabled;

        public bool BtnUpload_Enabled
        {
            get { return _BtnUpload_Enabled; }
            set { SetProperty(ref _BtnUpload_Enabled, value); }
        }

        #endregion
        public ICommand Select
        {
            get
            {
                return new RelayCommand(SelectDia);
            }
        }

        public void SelectDia()
        {
            Selected_Day(DiaSeleccionado.IdDia);
        }


        public TiendaViewModel()
        {
            RutaList = App.DB.Ruta.ToList();
            Dias = GetDias();
            int nodia = (int)DateTime.Now.DayOfWeek;
            DiaSeleccionado = GetDias().FirstOrDefault(a => a.IdDia == nodia);
            var dia = DiaSeleccionado.Dia;
            ListaTiendas = GetInformacion(dia);

        }

        #region Singleton
        public static TiendaViewModel instance;
        public static TiendaViewModel GetInstance()
        {
            if (instance is null)
            {
                return new TiendaViewModel();
            }
            return instance;
        }
        #endregion

        #region Methods

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(() =>
                {
                    IsRefreshing = true;

                    SelectDia();

                    IsRefreshing = false;
                });
            }
        }
        public ObservableCollection<ListaDias> GetDias()
        {
            return new ObservableCollection<ListaDias>
            {
                new ListaDias { IdDia=0, DiaAvr="Do", Dia="Domingo" },
                new ListaDias { IdDia=1, DiaAvr="Lu", Dia="Lunes" },
                new ListaDias { IdDia=2, DiaAvr="Ma", Dia="Martes" },
                new ListaDias { IdDia=3, DiaAvr="Mi", Dia="Miercoles" },
                new ListaDias { IdDia=4, DiaAvr="Ju", Dia="Jueves" },
                new ListaDias { IdDia=5, DiaAvr="Vi", Dia="Viernes" },
                new ListaDias { IdDia=6, DiaAvr="Sa", Dia="Sabado" },
            };
        }
        public void Selected_Day(int index)
        {
            Dia = Dias.FirstOrDefault(s => s.IdDia == index).Dia;
            ListaTiendas = GetInformacion(Dia);
        }
        public List<Informacion> GetInformacion(string dia)
        {
            string fecha = DateTime.Now.ToString();
            DateTime inputDate = DateTime.Parse(fecha.Trim());
            // formatea la fecha de acuerdo a la zona del computador
            var d = inputDate;

            CultureInfo cul = CultureInfo.CurrentCulture;
            // Usa la fecha formateada y calcula el número de la semana
            int NumeroSemana = cul.Calendar.GetWeekOfYear(
                 d,
                 CalendarWeekRule.FirstDay,
                 DayOfWeek.Sunday);

            var rutas = App.DB.Ruta.Where(r => r.NombreDelDia.Equals(dia) && r.NumeroSemana == NumeroSemana).ToList();
            var preg = App.DB.Pregunta.ToList();

            if (rutas.Count == 0)
            {
                if (!(preg.Count() == 0))
                {
                    descargaRutaSemanaActual();
                }

            }

            var informacion = App.DB.InfoTienda.ToList();
            var rutainfotiendalist = (from r in rutas
                                      join t in App.DB.Tiempo on r.IdTiempo equals t.IdTiempo
                                      join i in informacion on r.IdTienda equals i.IdTienda
                                      //where r.Sincronizado == false
                                      select new Informacion
                                      {
                                          Direccion = i.Direccion,
                                          IdRuta = r.IdRuta,
                                          Sincronizado = r.Sincronizado,
                                          IdTienda = i.IdTienda,
                                          NombreTienda = i.NombreTienda,
                                          Fecha = t.Fecha.ToShortDateString(),
                                          NombreDia = t.NombreDia,
                                          IdEstatus = r.Status,
                                          Estatus = r.Status == 0 ? "Estatus: Nueva ruta" :
                                                    r.Status == 1 ? "Estatus: En curso" :
                                                    r.Status == 2 && r.Sincronizado == false ? "Estatus: Cerrada" :
                                                    r.Status == 2 && r.Sincronizado == true ? "Estatus: Sincronizada" :
                                                    "Estatus: Incidencia",
                                          ColorCard = r.Status == 0 ? "#6E6E6E" :
                                                      r.Status == 1 ? "#2AAAE1" :
                                                      r.Status == 2 && r.Sincronizado == false ? "#F6911A" :
                                                      r.Status == 2 && r.Sincronizado == true ? "#98C93C" :
                                                      "#F5A9BC",
                                          UpVisible = true,
                                          OnTiendaClick = new Command<Informacion>(OnTiendaClick),
                                          OnModuloClick = new Command<Informacion>(OnModuloClick),
                                          OnUploadClick = new Command<Informacion>(OnUploadClick)
                                      }).ToList();

            rutainfotiendalist = rutainfotiendalist.GroupBy(test => test.IdRuta).Select(grp => grp.First()).ToList();

            if (App.idRuta != null)
            {
                foreach (var item in rutainfotiendalist.Where(o => App.idRuta.Contains(o.IdRuta)))
                {
                    item.Procesando = true;
                    item.UpVisible = false;
                }
            }


            return rutainfotiendalist;

        }
        public async void descargaRutaSemanaActual()
        {
            await DownloadData.BajarRuta(true);
        }
        private async void OnModuloClick(Informacion inf)
         {
            IsBusy = true;
            IsEnabled = false;
            IsVisibleStack = false;
            if (inf.Sincronizado == false && (inf.IdEstatus == 0 || inf.IdEstatus == 1))
            {
                MainViewModel.GetInstance().Modulo = new ModuloViewModel(inf);
                await MainPage.Current.Navigation.PushAsync(new ModuloPage() { Title = inf.NombreTienda });
                IsBusy = false;
                IsVisibleStack = true;
            }
            else if (inf.Sincronizado == false && inf.IdEstatus == 2)
            {
                MessageService.ShowMessage("Esta ruta ha sido terminada, favor de subir la información.");
                IsBusy = false;
                IsVisibleStack = true;
            }
            else if (inf.Sincronizado == true && inf.IdEstatus == 2)
            {
                MessageService.ShowMessage("La informacion de esta tienda ya se encuentra en el servidor.");
                IsBusy = false;
                IsVisibleStack = true;
            }
            else
            {
                //incidencia
            }

        }
        private async void OnTiendaClick(Informacion inf)
        {
            App.IdTienda = inf.IdTienda;
            if (Device.RuntimePlatform == Device.UWP)
            {
                //MainViewModel.GetInstance().SemaforoMap = new SemaforoMapViewModel();
                await App.Current.MainPage.Navigation.PushAsync(new UbicacionPage());
            }
            else
            {
                //MainViewModel.GetInstance().SemaforoMap = new SemaforoMapViewModel();
                await Shell.Current.Navigation.PushAsync(new UbicacionPage());
            }
        }
        private async void OnUploadClick(Informacion inf)
        {
            if (inf.Sincronizado == false && (inf.IdEstatus == 0 || inf.IdEstatus == 1))
            {
                MessageService.ShowMessage("Esta es una ruta nueva, presiona sobre el recuadro para acceder a la tienda.");

            }
            else if (inf.Sincronizado == true && inf.IdEstatus == 2)
            {
                MessageService.ShowMessage("La informacion de esta tienda ya se encuentra en el servidor.");

                return;
            }
            else if (inf.Sincronizado == false && inf.IdEstatus == 2)
            {
                //Subir tienda
                var RutaSelected = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == inf.IdRuta);
                List<Ruta> RutaSelectedList = new List<Ruta>();

                var InfoTienda = App.DB.InfoTienda.Where(s => s.IdTienda == inf.IdTienda).ToList();
                if (RutaSelected != null)
                {
                    var info = InfoTienda.FirstOrDefault(s => s.IdTienda == RutaSelected.IdTienda);
                    //Si la tienda para subir informacion es del tipo salida global se manda todas las tiendas para subir, sino se subira de la manera normal (una tienda)
                    if (info != null)
                    {
                        if (info.EstatusCheck != null)
                        {
                            if ((bool)info.EstatusCheck)
                            {
                                RutaSelectedList = App.DB.Ruta.Where(re => re.Status == 3 || re.Status == 2 && re.Sincronizado == false && re.IdTiempo == RutaSelected.IdTiempo).ToList();
                            }
                        }
                        else
                        {
                            RutaSelectedList.Add(RutaSelected);
                        }
                    }
                    else
                    {
                        RutaSelectedList.Add(RutaSelected);
                    }

                    ///     //
                    ///   / //
                    ///     //
                    ///     //
                    ///  //////// 
                    //Aqui se suben las rutas es el primer metodo donde entra al seleccionar una tienda pendiente por subir
                    PaquetesUpload.CargaRutaPendiente();
                    //Se procede a subir la tienda o tiendas cerradas o terminadas
                    SubiendoRuta();
                    foreach (var item in RutaSelectedList)
                    {
                        if (await UploadData.Upload(item, false))
                        {
                            try
                            {
                                //si la ruta se subio con exito se procede a actualizar las banderas para los cambios visuales
                                CargaTerminadaRuta(inf.IdRuta, false);
                                EliminaFotosSincronizadas(item);
                            }
                            catch (Exception ex)
                            {
                                CargaTerminadaRuta(inf.IdRuta, true);
                                var rutaPendienteLi = MainViewModel.GetInstance().RutaEstatus.getRuta();
                                MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaPendienteLi;
                                Crashes.TrackError(ex, App.TrackData());
                            }
                        }
                        else
                        {
                            CargaTerminadaRuta(inf.IdRuta,true);
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioTienda, "BtnUpload_Clicked", "UploadPage", $"No fue posible subir la información de la ruta: { item.IdRuta } - { item.TiendaNombre}");
                            break;
                        }
                        var r = App.DB.Ruta;
                        if (r.Any())
                        {
                            bool be;
                            bool sendRate = bool.TryParse(r.FirstOrDefault().SendQual.ToString(), out be);
                            if (sendRate)
                            {
                                if (be)
                                {
                                    bool c = await BajarCalificacion();
                                }
                            }
                        }
                    }
                   
                    PaquetesUpload.LimpiarRutaPendiente();
                    var rutaPendienteL = MainViewModel.GetInstance().RutaEstatus.getRuta();
                    MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaPendienteL;
                }
            }

        }
        public void SubiendoRuta()
        {
            //Se busca las tiendas pendientes por subir en el listado ListaTiendas que es la lista visual de tiendas por dia y APP.IdRuta donde estan las pendentes por subir o cerradas
            //Se cambian las banderas procesado y upvisible para poner el spinner a todas las tiendas cerradas para ir subiendo una por una
            foreach (var item in ListaTiendas.Where(re => re.IdEstatus == 3 || re.IdEstatus == 2 && re.Sincronizado == false && App.idRuta.Contains(re.IdRuta)).ToList())
            {
                item.Procesando = true;
                item.UpVisible = false;
            }
            MainViewModel.GetInstance().RutaEstatus.IsBusy = true;
            MainViewModel.GetInstance().RutaEstatus.IsVisibleStack = false;
        }
        public void CargaTerminadaRuta(int idruta, bool noCargada)
        {
            //Se cambia el estatus de las rutas subidas con exito para mostrar el cambio de manera visual
            foreach (var item in ListaTiendas.Where(re => (re.IdEstatus == 3 || re.IdEstatus == 2 && re.Sincronizado == false) || re.IdRuta == idruta).ToList())
            {
                item.Procesando = false;
                item.UpVisible = true;
                if (item.IdRuta == idruta && noCargada == false)
                {
                    item.IdEstatus = 2;
                    item.ColorCard = "#98C93C";
                    item.Estatus = "Estatus: Sincronizada";
                    item.Sincronizado = true;
                 
                }
            }
            //Se actualiza la variable global APP.IDRUTA donde solo estaran las pendientes por subir.
            App.idRuta.Remove(idruta);
            MainViewModel.GetInstance().RutaEstatus.IsBusy = false;
            MainViewModel.GetInstance().RutaEstatus.IsVisibleStack = true;
        }
        protected void EliminaFotosSincronizadas(Ruta ruta)
        {
            //Eliminamos las respuestas de las rutas que se suben en estos momentos
            App.DB.RespuestaAuditor.Delete(f => f.IdRuta == ruta.IdRuta && f.Foto != null && f.Sincronizado);
            //Eliminamos si en algún momento llegan a existir fotos sincronizadas de otras rutas antes subidas
            //App.DB.RespuestaAuditor.Delete(f => f.Sincronizado);
            App.DB.SaveChanges();
        }
        public async Task<bool> BajarCalificacion()
        {
            bool status = false;
            bool isconnected = true;


            var qualiResponse = await App.API.QualiEndPoint.QualiRoute(App.CurrentUser.IdUsuario, async () =>
            {
                isconnected = false;
            });

            if (qualiResponse != null)
            {
                if (qualiResponse.Qualification != null && qualiResponse.Qualification.Count > 0)
                {
                    foreach (var item in qualiResponse.Qualification)
                    {
                        if (!App.DB.Qualification.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.Qualification.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiSections != null && qualiResponse.QualiSections.Count > 0)
                {
                    foreach (var item in qualiResponse.QualiSections)
                    {
                        if (!App.DB.QualiSection.Exists(a => a.IdRuta == item.IdRuta))
                        {
                            App.DB.QualiSection.Add(item);
                        }
                    }
                    App.DB.SaveChanges();
                }
                if (qualiResponse.QualiCuestion != null && qualiResponse.QualiCuestion.Count > 0)
                {
                    App.DB.Execute("DELETE FROM QualiPregunta");

                    foreach (var item in qualiResponse.QualiCuestion)
                    {
                        //if (!App.DB.QualiPregunta.Exists(a => a.IdRuta == item.IdRuta))
                        //{
                        App.DB.QualiPregunta.Add(item);
                        //}
                    }
                    App.DB.SaveChanges();
                }

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DescargaExitosa, "BajarCalificacion", "UploadPage", "Se han descargado las calificaciones");

                status = true;
            }
            else
            {
                if (isconnected)
                {
                    await Application.Current.MainPage.DisplayAlert("PSM 360", "No tienes conexión a internet, intenta más tarde", "Aceptar");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.ErrorAlBajarInformacion, "BajarCalificacion", "UploadPage", "Ocurrio un error al descargar calificacion, intenta mas tarde");
                }
            }

            return status;
        }
        #endregion
    }
}