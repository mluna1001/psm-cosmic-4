﻿using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class DetalleIndicadorViewModel : BaseViewModel
    {
        #region Atributes
        bool _IsVisibleSMinimo;
        bool _IsVisibleSMedio;
        bool _IsVisibleSMaximo;
        string _Semaforo;
        string _Calificacion;
        string _Fecha;
        string _ValorMinimo;
        string _ValorMedio;
        string _ValorAlto;
        string _SemaforoMinimo;
        string _SemaforoMedio;
        string _SemaforoAlto;
        Color _Color;
        Color _SemaforoMinimoColor;
        Color _SemaforoMedioColor;
        Color _SemaforoAltoColor;
        Indicador _Indicador; 
        #endregion

        #region Properties
        public bool IsVisibleSMinimo
        {
            get { return _IsVisibleSMinimo; }
            set { SetProperty(ref _IsVisibleSMinimo, value); }
        }
        public bool IsVisibleSMedio
        {
            get { return _IsVisibleSMedio; }
            set { SetProperty(ref _IsVisibleSMedio, value); }
        }
        public bool IsVisibleSMaximo
        {
            get { return _IsVisibleSMaximo; }
            set { SetProperty(ref _IsVisibleSMaximo, value); }
        }
        public string Semaforo
        {
            get { return _Semaforo; }
            set { SetProperty(ref _Semaforo, value); }
        }
        public string Calificacion
        {
            get { return _Calificacion; }
            set { SetProperty(ref _Calificacion, value); }
        }
        public string Fecha
        {
            get { return _Fecha; }
            set { SetProperty(ref _Fecha, value); }
        }
        public string ValorMinimo
        {
            get { return _ValorMinimo; }
            set { SetProperty(ref _ValorMinimo, value); }
        }
        public string ValorMedio
        {
            get { return _ValorMedio; }
            set { SetProperty(ref _ValorMedio, value); }
        }
        public string ValorAlto
        {
            get { return _ValorAlto; }
            set { SetProperty(ref _ValorAlto, value); }
        }
        public string SemaforoMinimo
        {
            get { return _SemaforoMinimo; }
            set { SetProperty(ref _SemaforoMinimo, value); }
        }
        public string SemaforoMedio
        {
            get { return _SemaforoMedio; }
            set { SetProperty(ref _SemaforoMedio, value); }
        }
        public string SemaforoAlto
        {
            get { return _SemaforoAlto; }
            set { SetProperty(ref _SemaforoAlto, value); }
        }
        public Color Color
        {
            get { return _Color; }
            set { SetProperty(ref _Color, value); }
        }
        public Color SemaforoMinimoColor
        {
            get { return _SemaforoMinimoColor; }
            set { SetProperty(ref _SemaforoMinimoColor, value); }
        }
        public Color SemaforoMedioColor
        {
            get { return _SemaforoMedioColor; }
            set { SetProperty(ref _SemaforoMedioColor, value); }
        }
        public Color SemaforoAltoColor
        {
            get { return _SemaforoAltoColor; }
            set { SetProperty(ref _SemaforoAltoColor, value); }
        }
        #endregion

        public DetalleIndicadorViewModel(Indicador indicador)
        {
            this.Title = indicador.Descripcion;
            GetData(indicador);
        }

        #region Methods
        void GetData(Indicador indicador)
        {
            Semaforo = indicador.TextoIndicador == null ? indicador.Semaforo : indicador.TextoIndicador;
            Color = Color.FromHex(indicador.Color == null ? "#000000" : indicador.Color);
            Calificacion = indicador.NumeroIndicador.ToString();
            Fecha = indicador.Fecha.ToString("dd-MM-yyyy");
            var catInd = App.DB.CatalogoIndicador.FirstOrDefault(ci => ci.IdCatalogoIndicador == indicador.IdCatalogoIndicador);

            if (catInd.ValorMinimo.HasValue)
            {
                IsVisibleSMinimo = true;
                SemaforoMinimoColor = Color.FromHex(catInd.ColorMinimo == null ? "#000000" : catInd.ColorMinimo);
                ValorMinimo = catInd.ValorMinimo.Value.ToString();
                SemaforoMinimo = catInd.SemaforoMinimo;
            }
            if (catInd.ValorMedio.HasValue)
            {
                IsVisibleSMedio = true;
                SemaforoMedioColor = Color.FromHex(catInd.ColorMedio == null ? "#000000" : catInd.ColorMedio);
                ValorMedio = catInd.ValorMedio.Value.ToString();
                SemaforoMedio = catInd.SemaforoMedio;
            }
            if (catInd.ValorAlto.HasValue)
            {
                IsVisibleSMaximo = true;
                SemaforoAltoColor = Color.FromHex(catInd.ColorAlto == null ? "#000000" : catInd.ColorAlto);
                ValorAlto = catInd.ValorAlto.Value.ToString();
                SemaforoAlto = catInd.SemaforoAlto;
            }
        }
        #endregion

        public ICommand CloseCommand
        {
            get
            {
                return new Command(Close);
            }
        }

        async void Close()
        {
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopModalAsync();
            else
                await Shell.Current.Navigation.PopModalAsync();
        }
    }
}
