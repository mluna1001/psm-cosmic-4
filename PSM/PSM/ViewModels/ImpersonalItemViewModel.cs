﻿using Cosmic;
using GalaSoft.MvvmLight.Command;
using PSM.Common;
using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class ImpersonalItemViewModel : Usuario, INotifyPropertyChanged
    {
        bool _IsBusy;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsBusy
        {
            get { return _IsBusy; }
            set { SetProperty(ref _IsBusy, value); }
        }

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        async void Login()
        {
            var dictionary = new Dictionary<string, string>
            {
                { "alias", Alias }
            };

            var response = await ApiServices.Post<Response>(ApiUrl.sGetImpersonalLogin, dictionary);
            if (response != null)
            {
                MainViewModel.GetInstance().IsBusy = true;
                App.BackupUser = App.DB.Usuario.FirstOrDefault().To<BackupUsuario>();
                App.BackupMenuUser = App.DB.MenuUser.ToList().To<List<BackupMenuUser>>();
                App.BackupAvisos = App.DB.Aviso.ToList().To<List<BackupAviso>>();

                // Se elimina la base de datos
                App.DB.DropTables();
                App.DB.CreateTables();

                App.DB.BackupUsuario.Add(App.BackupUser);
                App.DB.SaveChanges();
                SaveBackupMenu(App.BackupMenuUser);
                SaveBackupAvisos(App.BackupAvisos);

                UserResponse userResponse = response.Data.To<UserResponse>();
                if (userResponse != null)
                {
                    Usuario user = userResponse.User.To<Usuario>();
                    App.CurrentUser = user;

                    if (!App.DB.Usuario.Exists(x => x.IdUsuario == user.IdUsuario))
                    {
                        App.DB.Usuario.Add(user);
                        App.DB.SaveChanges();
                    }

                    DownloadSaveMenu(userResponse.MenuUser.To<List<Models.MenuUser>>(), userResponse.CifraControl);
                    SaveAvisos(userResponse.Avisos.To<List<Aviso>>());

                    MainViewModel.GetInstance().Avisos = new AvisosViewModel();
                    var mvm = MainViewModel.GetInstance();
                    mvm.LoadMenu();
                    MainViewModel.GetInstance().About = new AboutViewModel();
                    MainViewModel.GetInstance().Tienda = new TiendaViewModel();
                    //MainViewModel.GetInstance().Modulo = new ModuloViewModel(null);
                    MainViewModel.GetInstance().Ubicacion = new UbicacionViewModel();
                    MainViewModel.GetInstance().Descarga = new DescargaViewModel();

                    MainViewModel.GetInstance().RutaEstatus = new RutaEstatusViewModel();
                    MainViewModel.GetInstance().RutaEstatusItem = new RutaEstatusItemViewModel();
                    MainViewModel.GetInstance().Perfil = new PerfilViewModel();
                    mvm.Semaforo = new SemaforoViewModel();
                    mvm.SemaforoItemViewModel = new SemaforoItemViewModel();
                    mvm.SemaforoMap = new SemaforoMapViewModel();

                    if (Device.RuntimePlatform != Device.UWP)
                    {
                        App.Current.MainPage = new MainPage();
                    }
                    else
                    {
                        App.Current.MainPage = new NavigationPage(new MainPage());
                    }
                }
            }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private static void SaveAvisos(List<Aviso> avisos)
        {
            foreach (var aviso in avisos)
            {
                if (!App.DB.Aviso.Exists(e => e.IdAviso == aviso.IdAviso))
                {
                    //aviso.Vigente = true;
                    App.DB.Aviso.Add(aviso);
                }
            }
            App.DB.SaveChanges();
        }

        private static void SaveBackupAvisos(List<BackupAviso> avisos)
        {
            foreach (var aviso in avisos)
            {
                if (!App.DB.Aviso.Exists(e => e.IdAviso == aviso.IdAviso))
                {
                    //aviso.Vigente = true;
                    App.DB.BackupAvisos.Add(aviso);
                }
            }
            App.DB.SaveChanges();
        }

        void DownloadSaveMenu(List<Models.MenuUser> menuList, int cifaControl)
        {
            int CifraControlLocal = App.DB.MenuUser.Sum(s => s.IdMenuMovil);
            if (cifaControl != CifraControlLocal)
            {
                App.DB.Execute("Delete from MenuUser");
                if (menuList != null && menuList.Count > 0)
                {
                    foreach (var item in menuList)
                    {
                        App.DB.Add(item);
                        App.DB.SaveChanges();
                    }
                }
            }
        }

        void SaveBackupMenu(List<Models.BackupMenuUser> menuList)
        {
            foreach (var item in menuList)
            {
                App.DB.BackupMenuUser.Add(item);
            }
            App.DB.SaveChanges();
        }
    }
}
