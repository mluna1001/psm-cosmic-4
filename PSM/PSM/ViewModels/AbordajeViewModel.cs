﻿using GalaSoft.MvvmLight.Command;
using PSM.Models;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class AbordajeViewModel : BaseViewModel
    {
        ObservableCollection<AbordajeModel> _abordajeL = new ObservableCollection<AbordajeModel>();
        public ObservableCollection<AbordajeModel> AbordajeL
        {
            get { return _abordajeL; }
            set { SetProperty(ref _abordajeL, value); }
        }
        public AbordajeViewModel()
        {
            instance = this;
            getListAbordaje();

        }
        private static AbordajeViewModel instance;

        public static AbordajeViewModel GetInstance()
        {
            if (instance == null)
                return new AbordajeViewModel();
            return instance;
        }
        public void getListAbordaje()
        {
            AbordajeL = new ObservableCollection<AbordajeModel>();
            var seccionmenu = App.DB.CatalogoSeccionMenu.Where(e => e.IdProyecto == App.CurrentUser.IdProyecto).ToList();

            foreach (var item in seccionmenu)
            {
                AbordajeL.Add(new AbordajeModel
                {
                    Descripcion = item.Descripcion,
                    Icono = item.Icono,
                    IdProyecto = item.IdProyecto,
                    IdSeccionMenu = item.IdSeccionMenu,
                    SelectItemAbordaje = new Command<AbordajeModel>(SelectItemAbordaje)
                });
            }
        }
        public async void SelectItemAbordaje(AbordajeModel am)
        {
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "Se ha seleccionado un abordaje", "AbordajesViewModel", am);
            var multimedialist = App.DB.Multimedia.Where(m => m.IdSeccionMenu == am.IdSeccionMenu).ToList();
            MainViewModel.GetInstance().AbordajeMultimedia = new AbordajeMultimediaViewModel(multimedialist, am.IdSeccionMenu, am.Descripcion);
            if (Device.RuntimePlatform != Device.UWP)
                await Shell.Current.Navigation.PushModalAsync(new AbordajeMultimediaPage());
            else
                await App.Current.MainPage.Navigation.PushModalAsync(new AbordajeMultimediaPage());
        }
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    getListAbordaje();

                    IsRefreshing = false;
                });
            }
        }
        public ICommand BackCommand
        {
            get
            {
                return new RelayCommand(Back);
            }
        }
        async void Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }


    public class AbordajeModel : CatalogoSeccionMenu
    {
        public string ColorCard { get; set; }
        public Command SelectItemAbordaje { get; set; }
        public ImageSource Image
        {
            get
            {
                if (string.IsNullOrEmpty(Icono))
                {
                    return "";
                }
                else
                {
                    if (Icono.Contains("http"))
                    {
                        return Icono;
                    }
                    else
                    {
                        return Xamarin.Forms.ImageSource.FromStream(() => new MemoryStream(Convert.FromBase64String(Icono)));
                    }
                }
            }
        }
    }
}
