﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class PickerView
    {
        /// <summary>
        /// Picker actual
        /// </summary>
        public Picker PickerProduct { get; set; }
        /// <summary>
        /// Label del picker actual
        /// </summary>
        public Label LabelProduct { get; set; }
        /// <summary>
        /// Lista de productos actuales para el picker
        /// </summary>
        public List<ProductoModel> Items { get; internal set; }
    }
}
