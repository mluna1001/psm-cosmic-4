﻿using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class NearbyRouteViewModel : BaseViewModel
    {
        bool _StackProgressIsVisible;
        bool _IsVisibleList;
        List<TiendasCercanasRuta> _ListaTiendas;
        public bool StackProgressIsVisible
        {
            get { return _StackProgressIsVisible; }
            set { SetProperty(ref _StackProgressIsVisible, value); }
        }

        public bool IsVisibleList
        {
            get { return _IsVisibleList; }
            set { SetProperty(ref _IsVisibleList, value); }
        }

        public List<TiendasCercanasRuta> ListaTiendas
        {
            get { return _ListaTiendas; }
            set { SetProperty(ref _ListaTiendas, value); }
        }

        public NearbyRouteViewModel()
        {
            StackProgressIsVisible = true;
            IsVisibleList = false;
            LlenaTiendasCercanasRuta();
        }
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(() =>
                {
                    IsRefreshing = true;

                    LlenaTiendasCercanasRuta();

                    IsRefreshing = false;
                });
            }
        }
        private void LlenaTiendasCercanasRuta()
        {
            ListaTiendas = App.DB.TiendasCercanasRuta.ToList();

            if (ListaTiendas == null || ListaTiendas.Count == 0)
            {
                MessageService.ShowMessage("No hay tiendas disponibles para crear rutas.");

                StackProgressIsVisible = false;
                IsVisibleList = true;
                return;
            }


            StackProgressIsVisible = false;
            IsVisibleList = true;
        }
    }
}
