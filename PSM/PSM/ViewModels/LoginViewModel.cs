﻿namespace PSM.ViewModels
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using Cosmic;
    using GalaSoft.MvvmLight.Command;
    using PSM.Common;
    using PSM.Helpers;
    using PSM.Models;
    using PSM.Services;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
        #region Attributes
        bool _IsRemembered;
        string _Alias;
        string _Password;
        string _IMEI;
        ApiServices apiService;
        #endregion

        #region Properties
        public string Alias
        {
            get { return _Alias; }
            set { SetProperty(ref _Alias, value); }
        }
        public string Password
        {
            get { return _Password; }
            set { SetProperty(ref _Password, value); }
        }
        public string IMEI
        {
            get { return _IMEI; }
            set { SetProperty(ref _IMEI, value); }
        }
        public bool IsRemembered
        {
            get { return _IsRemembered; }
            set { SetProperty(ref _IsRemembered, value); }
        }
        #endregion

        #region Constructor
        public LoginViewModel()
        {
#if DEBUG
            //  imersis01   asesorleno01
            // tfal01         jaloma01    Cosmic02       Cosmic01
            _Alias = "Cosmic01";
            _Password = "123456";
#endif
            IsVisible = false;
            IsBusy = false;
            IsRemembered = true;
        }
        #endregion

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }
        async void Login()
        {
            IsBusy = true;
            IsVisible = true;
            IsEnabled = false;

            if (string.IsNullOrEmpty(Alias))
            {
                MessageService.ShowMessage("Ingrese un usuario");
                return;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageService.ShowMessage("Ingrese una contraseña");
                return;
            }

            //var token = App.TokenDevice.Token;

            var dictionary = new Dictionary<string, string>()
            {
                { "alias", Alias },
                { "password", Password },
                { "imei", IMEI },
                { "tokenFirebase", App.Token },
                { "plataforma", "1" }
            };

            var response = await ApiServices.Post<Response>(ApiUrl.sLogin, dictionary);

            if (response != null)
            {
                if (response.Success)
                {
                    var userResponse = response.Data.To<UserResponse>();
                    if (userResponse != null)
                    {
                        Usuario user = userResponse.User.To<Usuario>();
                        App.CurrentUser = user;

                        if (!App.DB.Usuario.Exists(x => x.IdUsuario == user.IdUsuario))
                        {
                            App.DB.Usuario.Add(user);
                            App.DB.SaveChanges();
                        }

                        DownloadSaveMenu(userResponse.MenuUser.To<List<Models.MenuUser>>(), userResponse.CifraControl);
                        SaveAvisos(userResponse.Avisos.To<List<Aviso>>());

                        MainViewModel.GetInstance().Avisos = new AvisosViewModel();
                        MainViewModel.GetInstance().Tienda = new TiendaViewModel();
                        var mvm = MainViewModel.GetInstance();
                        mvm.LoadMenu();
                        MainViewModel.GetInstance().About = new AboutViewModel();

                        //MainViewModel.GetInstance().Modulo = new ModuloViewModel(null);
                        MainViewModel.GetInstance().Ubicacion = new UbicacionViewModel();
                        MainViewModel.GetInstance().Descarga = new DescargaViewModel();

                        MainViewModel.GetInstance().RutaEstatus = new RutaEstatusViewModel();
                        MainViewModel.GetInstance().RutaEstatusItem = new RutaEstatusItemViewModel();
                        MainViewModel.GetInstance().Perfil = new PerfilViewModel();
                        mvm.Semaforo = new SemaforoViewModel();
                        mvm.SemaforoItemViewModel = new SemaforoItemViewModel();
                        mvm.SemaforoMap = new SemaforoMapViewModel();
                        mvm.Tienda = new TiendaViewModel();
                        mvm.Package = new PackageViewModel();
                        mvm.Abordaje = new AbordajeViewModel();



                        if (IsRemembered)
                        {
                            Settings.IsRemembered = "true";
                            Settings.Alias = this.Alias;
                        }
                        else
                        {
                            Settings.IsRemembered = "false";
                            Settings.Alias = string.Empty;
                        }

                        if (Device.RuntimePlatform != Device.UWP)
                        {
                            App.Current.MainPage = new MainPage();
                        }
                        else
                        {
                            App.Current.MainPage = new NavigationPage(new MainPage());
                        }
                    }
                    else
                    {
                        MessageService.ShowMessage(response.Message);
                        IsBusy = false;
                        IsVisible = false;
                        IsEnabled = true;
                    }
                }
                else
                {
                    MessageService.ShowMessage(response.Message);
                    IsBusy = false;
                    IsVisible = false;
                    IsEnabled = true;
                }

            }
            else
            {
                MessageService.ShowMessage("Usuario y contraseña incorrectos. Intente nuevamente");
                IsBusy = false;
                IsVisible = false;
                IsEnabled = true;
            }
        }

        private static void SaveAvisos(List<Aviso> avisos)
        {
            try
            {
                foreach (var aviso in avisos)
                {
                    if (!App.DB.Aviso.Exists(e => e.IdAviso == aviso.IdAviso))
                    {
                        //aviso.Vigente = true;
                        App.DB.Aviso.Add(aviso);
                    }
                }
                App.DB.SaveChanges();
            }
            catch (System.Exception ex)
            {

            }
        }

        void DownloadSaveMenu(List<Models.MenuUser> menuList, int cifaControl)
        {
            int CifraControlLocal = App.DB.MenuUser.Sum(s => s.IdMenuMovil);
            if (cifaControl != CifraControlLocal)
            {
                App.DB.Execute("Delete from MenuUser");
                if (menuList != null && menuList.Count > 0)
                {
                    foreach (var item in menuList)
                    {
                        App.DB.Add(item);
                        App.DB.SaveChanges();
                    }
                }
            }

            IsBusy = false;
            IsVisible = false;
            IsEnabled = true;
        }
    }
}