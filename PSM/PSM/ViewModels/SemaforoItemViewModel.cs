﻿using GalaSoft.MvvmLight.Command;
using PSM.Services;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class SemaforoItemViewModel : usuarioModel
    {
        #region Attributes
      
        #endregion

        #region Properties
       
        #endregion

        #region Constructors
        public SemaforoItemViewModel()
        {
            instance = this;
          
        }
        #endregion

        #region Singleton
        private static SemaforoItemViewModel instance;

        public static SemaforoItemViewModel GetInstance()
        {
            if (instance == null)
                return new SemaforoItemViewModel();
            return instance;
        }
        #endregion
        #region Commands
        public ICommand Mapa
        {
            get
            {
                return new RelayCommand(MuestraMapa);
            }
        }
        #endregion

        #region Methods 

        public async void MuestraMapa()
        {
            if (Asistencia.Count != 0)
            {
                App.um = this;
                if (Device.RuntimePlatform == Device.UWP)
                {
                    //MainViewModel.GetInstance().SemaforoMap = new SemaforoMapViewModel();
                    await App.Current.MainPage.Navigation.PushAsync(new SemaforoMapPage());
                }
                else
                {
                    //MainViewModel.GetInstance().SemaforoMap = new SemaforoMapViewModel();
                    await Shell.Current.Navigation.PushAsync(new SemaforoMapPage()); 
                  
                }
            }
            else
            {
                MessageService.ShowMessage("El colaborador no tiene asistencia registrada para el dia de hoy.");
            }
        }
        #endregion
    }
}
