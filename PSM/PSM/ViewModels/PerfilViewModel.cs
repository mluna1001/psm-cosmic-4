﻿using GalaSoft.MvvmLight.Command;
using PSM.Models;
using PSM.Services;
using PSM.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Cosmic;
using PSM.Views;

namespace PSM.ViewModels
{
    public class PerfilViewModel : BaseViewModel
    {
        #region Attributes

        #endregion

        #region Properties
        private NavigationService navigationService;
        #endregion

        #region Constructors
        public PerfilViewModel()
        {
            instance = this;
            //Download();
            navigationService = new NavigationService();
        }
        #endregion

        #region Singleton
        private static PerfilViewModel instance;

        public static PerfilViewModel GetInstance()
        {
            if (instance == null)
                return new PerfilViewModel();
            return instance;
        }
        #endregion
        #region Commands
        public ICommand About
        {
            get
            {
                return new RelayCommand(AboutNav);
            }
        }
        public ICommand DeleteDB
        {
            get
            {
                return new RelayCommand(Delete);
            }
        }
        public ICommand Salir
        {
            get
            {
                return new RelayCommand(Close);
            }
        }
        #endregion

        #region Methods
        public void AboutNav()
        {
            navigationService.Navigate("AboutPage");
        }

        public async void Delete()
        {
            var r = await MessageService.ShowDialog("Va a eliminar toda la informacion, ¿Desea continuar?", "No", "Si");
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "Delete", "Perfi", "Va a eliminar toda la informacion");
            if (r)
            {
                var resp = App.DB.RespuestaAuditor.Where(s => s.Sincronizado == false).ToList();
                var asis = App.DB.Asistencia.Where(a => !a.Sincronizado).ToList();

                if (resp.Count > 0 || asis.Count > 0)
                {
                    var r2 = await MessageService.ShowDialog("¡Atencion, tienes respuestas y asistencias sin subir!, estas se eliminaran, ¿Desea continuar?", "No", "Si");
                    if (r2)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "Delete DB", "Perfil", "Eliminar con respuestas");
                        Bitacora.DeleteBitacora();
                        App.DB.DropTables();
                        App.DB.CreateTables();
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "Se ha eliminado la base de datos");

                        //Navegar a la pantalla login
                        MainViewModel.GetInstance().Login = new LoginViewModel();
                        App.Current.MainPage = new NavigationPage(new LoginPage());
                        return;
                    }
                    else
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "No se elimino la base de datos");
                    }
                }
                else
                {
                    Bitacora.DeleteBitacora();
                    App.DB.DropTables();
                    App.DB.CreateTables();
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.DBActions, "DBDelete", "Se ha eliminado la base de datos");

                    //Navegar a la pantalla login
                    MainViewModel.GetInstance().Login = new LoginViewModel();
                    App.Current.MainPage = new NavigationPage(new LoginPage());
                    return;
                }
            }
        }
        public void Close()
        {
            if (!App.DB.BackupUsuario.Any())
            {
                navigationService.Navigate("LoginPage");
                Settings.Alias = string.Empty;
                Settings.IsRemembered = "false";
            }
            else
            {
                App.BackupUser = App.DB.BackupUsuario.FirstOrDefault();
                App.BackupMenuUser = App.DB.BackupMenuUser.ToList();
                App.BackupAvisos = App.DB.BackupAvisos.ToList();

                App.DB.DropTables();
                App.DB.CreateTables();

                App.DB.CreateCommand("DELETE FROM BackupUsuario");
                App.DB.CreateCommand("DELETE FROM BackupMenuUsuario");
                App.DB.CreateCommand("DELETE FROM BackupAvisos");
                App.DB.CreateCommand("VACUUM");
                App.DB.SaveChanges();

                App.CurrentUser = App.BackupUser.To<Usuario>();

                if (!App.DB.Usuario.Exists(x => x.IdUsuario == App.BackupUser.IdUsuario))
                {
                    App.DB.Usuario.Add(App.CurrentUser);
                    App.DB.SaveChanges();
                }

                SaveBackupMenu(App.BackupMenuUser);
                SaveAvisos(App.BackupAvisos);

                MainViewModel.GetInstance().Avisos = new AvisosViewModel();
                var mvm = MainViewModel.GetInstance();
                mvm.LoadMenu();
                MainViewModel.GetInstance().About = new AboutViewModel();
                MainViewModel.GetInstance().Tienda = new TiendaViewModel();
                //MainViewModel.GetInstance().Modulo = new ModuloViewModel(null);
                MainViewModel.GetInstance().Ubicacion = new UbicacionViewModel();
                MainViewModel.GetInstance().Descarga = new DescargaViewModel();

                MainViewModel.GetInstance().RutaEstatus = new RutaEstatusViewModel();
                MainViewModel.GetInstance().RutaEstatusItem = new RutaEstatusItemViewModel();
                MainViewModel.GetInstance().Perfil = new PerfilViewModel();
                mvm.Semaforo = new SemaforoViewModel();
                mvm.SemaforoItemViewModel = new SemaforoItemViewModel();
                mvm.SemaforoMap = new SemaforoMapViewModel();

                if (Device.RuntimePlatform != Device.UWP)
                {
                    App.Current.MainPage = new MainPage();
                }
                else
                {
                    App.Current.MainPage = new NavigationPage(new MainPage());
                }
            }
        }

        void SaveBackupMenu(List<Models.BackupMenuUser> menuList)
        {
            foreach (var item in menuList)
            {
                App.DB.MenuUser.Add(item.To<MenuUser>());
                App.DB.SaveChanges();
            }
        }

        private static void SaveAvisos(List<BackupAviso> avisos)
        {
            foreach (var aviso in avisos)
            {
                if (!App.DB.Aviso.Exists(e => e.IdAviso == aviso.IdAviso))
                {
                    //aviso.Vigente = true;
                    App.DB.Aviso.Add(aviso.To<Aviso>());
                }
            }
            App.DB.SaveChanges();
        }

        #endregion
    }
}
