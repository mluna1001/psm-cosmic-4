﻿using PSM.Helpers;
using PSM.Models.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PSM.ViewModels
{
    public class SemaforoMapViewModel : BaseViewModel
    {
        #region Attributes
        public usuarioModel usuariomodel;
        //public class pointMap
        //{
        //    public int Id { get; set; }
        //    public string Name { get; set; }
        //    public string Description { get; set; }
        //    public int Rate { get; set; }
        //    public double Latitude { get; set; }
        //    public double Longitude { get; set; }
        //}

        public List<PointMapDto> _items;

        public List<PointMapDto> Items
        {
            get => _items;
            set
            {
                _items = value;
                OnPropertyChanged(nameof(Items));
            }
        }

        #endregion
        public SemaforoMapViewModel()
        {
            //usuariomodel = item;
            //if (usuariomodel.Asistencia != null)
            //{
            Default();
            OpenWebCommand = new Command(async () => await App.Current.MainPage.Navigation.PopAsync());
            //}
        }
        public ICommand OpenWebCommand { get; }
        public void Default()
        {
            var point = App.um;
            if (point != null)
            {
                List<PointMapDto> pml = new List<PointMapDto>();

                foreach (var i in point.Asistencia)
                {
                    var pm = new PointMapDto()
                    {
                        Id = i.IdRuta,
                        Name = i.TiendaNombre.Trim() + " · Programada: " + i.DiaProgramado,
                        Latitude = (double)i.Latitud,
                        Longitude = (double)i.Longitud,
                        Description = "Entrada: " + i.FechaEntrada.ToString()
                    };

                    if (i.LatitudSalida != 0)
                    {
                        var pms = new PointMapDto()
                        {
                            Id = i.IdRuta,
                            Name = i.TiendaNombre.Trim() + " · Programada: " + i.DiaProgramado,
                            Latitude = (double)i.LatitudSalida,
                            Longitude = (double)i.LongitudSalida,
                            Description = "Salida: " + i.FechaSalida.ToString()
                        };
                        pml.Add(pms);
                    }
                    pml.Add(pm);
                }
                Items = pml;
            }
        }

        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
