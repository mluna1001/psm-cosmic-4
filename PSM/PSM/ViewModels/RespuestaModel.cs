﻿using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.ViewModels
{
    public class RespuestaModel : Respuesta
    {
        public List<PreguntaModel> PreguntasHijas { get; set; }
        public RespuestaModel()
        {
            PreguntasHijas = new List<PreguntaModel>();
        }
    }
}