﻿using GalaSoft.MvvmLight.Command;
using PSM.Function;
using PSM.Models;
using PSM.Services;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace PSM.ViewModels
{
    public class DescargaViewModel : BaseViewModel
    {
        #region Attributes
        private bool _IsDownload;
        private bool _IsAllDownload;
        private bool _showBtnMultimedia;
        private string _DownloadMessageStatus;

        #endregion

        #region Properties
        public bool ShowAlert { get; private set; }
        
        public bool IsDownload
        {
            get { return _IsDownload; }
            set { SetProperty(ref _IsDownload, value); }
        }

        public bool IsAllDownload
        {
            get { return _IsAllDownload; }
            set { SetProperty(ref _IsAllDownload, value); }
        }

        public bool showBtnMultimedia
        {
            get { return _showBtnMultimedia; }
            set { SetProperty(ref _showBtnMultimedia, value); }
        }

        public string DownloadMessageStatus
        {
            get { return _DownloadMessageStatus; }
            set { SetProperty(ref _DownloadMessageStatus, value); }
        }

        #endregion

        #region Singleton
        public static DescargaViewModel instance;
        public static DescargaViewModel GetInstance()
        {
            if (instance is null)
            {
                return new DescargaViewModel();
            }

            return instance;
        }
        #endregion

        #region Constructors
        public DescargaViewModel()
        {
            instance = this;

            bool any = App.DB.Pregunta.Any();
            IsAllDownload = !any;
            IsDownload = any;
            App.ShowBtnMultimedia = App.DB.CatalogoSeccion.Any(s => s.IdCatalogoSeccion == 10);
            showBtnMultimedia = App.ShowBtnMultimedia;

            ShowAlert = true;

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.IngresoConstructor, "Constructor", "DownloadPage");
        }
        #endregion

        #region Commands
        
        private async void OkCommand_Execute(object state)
        {
            ShowAlert = false;
            IsVisible = true;
            IsEnabled = false;

            bool IsOk = await DownloadData.BajaTodo(ShowAlert);
            if (IsOk)
            {
                App.ShowBtnMultimedia = App.DB.CatalogoSeccion.Any(s => s.IdCatalogoSeccion == 10);
                showBtnMultimedia = App.ShowBtnMultimedia;
                // Mensaje: Se descargaron catalogos, ruta, y preguntas.
                MessageService.ShowMessage("Se ha descargado toda la información");

                // UpdateButtons
                IsAllDownload = false;
                IsDownload = true;
            }
            else
            {
                // Ocurrio algo, favor de reintentar conectate a una red Wifi
                MessageService.ShowMessage("No es posible descargar información en estos momentos. Intenta más tarde");
            }
            
            IsVisible = false;
            IsEnabled = true;
            ShowAlert = true;
        }
        private async void CommandRuta(bool status)
        {
            ShowAlert = true;
            IsVisible = true;
            IsEnabled = false;
            IsDownload = false;
            var isOK = await DownloadData.BajarRuta(ShowAlert);
            if (isOK)
            {
                MessageService.ShowMessage("Las rutas se han descargado correctamente");
            }
            else
            {
                MessageService.ShowMessage("No es posible descargar las rutas en estos momentos. Intenta más tarde");
            }
            IsDownload = true;
            IsVisible = false;
            IsEnabled = true;
        }
        private async void CommandPregunta(object obj)
        {
            ShowAlert = true;
            IsVisible = true;
            IsEnabled = false;
            ShowAlert = false;
            IsDownload = false;
            var isOK = await DownloadData.BajarPreguntas(ShowAlert);
            if (isOK)
            {
                MessageService.ShowMessage("Las preguntas se han descargado correctamente");
            }
            else
            {
                MessageService.ShowMessage("No es posible descargar las preguntas en estos momentos. Intenta más tarde");
            }
            IsDownload = true;
            IsVisible = false;
            IsEnabled = true;
        }
        private async void CommandCatalogo(object obj)
        {
            ShowAlert = true;
            IsVisible = true;
            IsEnabled = false;
            IsDownload = false;
            var isOK = await DownloadData.BajarCatalogos(ShowAlert);
            if (isOK)
            {
               
                MessageService.ShowMessage("Los Catalogos se han descargado correctamente");
            }
            else
            {
                MessageService.ShowMessage("No es posible descargar los catalogos en estos momentos. Intenta más tarde");
            }
            IsDownload = true;
            IsVisible = false;
            IsEnabled = true;
        }
        private async void CommandMultimedia(object obj)
        {
            ShowAlert = true;
            IsVisible = true;
            IsEnabled = false;
            IsDownload = false;
            var isOK = await DownloadData.BajarMultimedia(ShowAlert);
            if (isOK)
            {
              
                MessageService.ShowMessage("Los archivos multimedia se han descargado correctamente");
            }
            else
            {
                MessageService.ShowMessage("No es posible descargar los archivos multimedia en estos momentos. Intenta más tarde");
            }
            IsDownload = true;
            IsVisible = false;
            IsEnabled = true;
        }
        //
        public RelayCommand<object> DescargaCommand
        {
            get
            {
                return new RelayCommand<object>(OkCommand_Execute);
            }
        }
        public ICommand RutaCommand
        {
            get
            {
                return new RelayCommand<bool>(CommandRuta);
            }
        }
        public ICommand PreguntaCommand
        {
            get
            {
                return new RelayCommand<object>(CommandPregunta);
            }
        }
        public ICommand CatalogoCommand
        {
            get
            {
                return new RelayCommand<object>(CommandCatalogo);
            }
        }
        public ICommand MultimediaCommand
        {
            get
            {
                return new RelayCommand<object>(CommandMultimedia);
            }
        }
        #endregion

        #region Methods
        async void DownloadAll()
        {
            // Descarga ruta

            // Descarga preguntas

            // Descarga catalogos

            // Descarga multimedia
        }
        #endregion
    }
}