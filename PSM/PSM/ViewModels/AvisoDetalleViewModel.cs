﻿using GalaSoft.MvvmLight.Command;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class AvisoDetalleViewModel : BaseViewModel
    {
        Aviso _Aviso;

        public Aviso Aviso
        {
            get { return _Aviso; }
            set { SetProperty(ref _Aviso, value); }
        }

        public AvisoDetalleViewModel(Aviso aviso)
        {
            _Aviso = aviso;
        }

        public ICommand OpenURLCommand
        {
            get
            {
                return new RelayCommand(OpenURL);
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                return new RelayCommand(Close);
            }
        }

        async void Close()
        {
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopModalAsync();
            else
                await Shell.Current.Navigation.PopModalAsync();
        }

        async void OpenURL()
        {
            await Browser.OpenAsync(Aviso.URL, BrowserLaunchMode.External);
        }
    }
}
