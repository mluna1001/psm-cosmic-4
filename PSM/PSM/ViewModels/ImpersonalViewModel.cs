﻿using Cosmic;
using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.ViewModels
{
    public class ImpersonalViewModel : BaseViewModel
    {
        List<ImpersonalItemViewModel> _Usuarios;

        public List<ImpersonalItemViewModel> Usuarios
        {
            get { return _Usuarios; }
            set { SetProperty(ref _Usuarios, value); }
        }

        public ImpersonalViewModel()
        {
            GetAllUsers();
        }

        async void GetAllUsers()
        {
            var response = await ApiServices.Get<Response>($"{ApiUrl.sGetAllProjectUsers}?IdProyecto={App.CurrentUser.IdProyecto}");
            if (response != null)
            {
                Usuarios = response.Data.To<List<ImpersonalItemViewModel>>();
            }
        }
    }
}
