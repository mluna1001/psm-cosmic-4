﻿using System;

namespace PSM.ViewModels
{
    public class BitacoraModel
    {
        public int IdBitacora { get; set; }
        public int IdCatalogoBitacoraMovil { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public DateTime? Fecha { get; set; }
        public string Parameters { get; set; }
        public int? IdRuta { get; set; }
        public int? IdUsuario { get; set; }
    }
}
