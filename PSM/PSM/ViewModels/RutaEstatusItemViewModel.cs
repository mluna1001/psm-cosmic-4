﻿using GalaSoft.MvvmLight.Command;
using Microsoft.AppCenter.Crashes;
using PSM.Helpers;
using PSM.Models;
using PSM.Models.Dto;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PSM.ViewModels
{
    public class RutaEstatusItemViewModel : RutaDto
    {
        #region Attributes
        Ruta ruta = new Ruta();
        private List<InfoTienda> InfoTienda;
        #endregion

        #region Commands
        public ICommand Sincroniza
        {
            get
            {
                return new RelayCommand(SincronizaRuta);
            }
        }
        #endregion

        #region Methods
        public async void SincronizaRuta()
        {
            MainViewModel.GetInstance().RutaEstatus.IsBusy = true;
            MainViewModel.GetInstance().RutaEstatus.IsVisibleStack = false;
            App.CurrentRoute = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == IdRuta);

            await Upload(false);

            //var rutaSincroniza = MainViewModel.GetInstance().RutaEstatus.getRuta();
            //MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaSincroniza;

            MainViewModel.GetInstance().RutaEstatus.IsBusy = false;
            MainViewModel.GetInstance().RutaEstatus.IsVisibleStack = true;
        }

        //████████
        //██    ██
        //██    ██
        //██    ██ 
        //██    ██
        //████████
        //Se subiran las rutas por dia o una sola ruta depende de la configuracion del proyecto.
        public async Task<bool> Upload(bool isAutomatic)
        {
            bool upstatus = false;
            if (Sincronizado)
            {
                MessageService.ShowMessage("La tienda seleccionada ya se encuentra sincronizada");
            }
            else
            {
                List<Ruta> RutaSelectedList = new List<Ruta>();
                //Si la carga de las tiendas es automatica se buscan todas las tiendas pendientes. Sino solo sube la tienda seleccioanda.
                if (!isAutomatic)
                {
                    RutaSelectedList = SelectedRuta();
                }
                else
                {
                    RutaSelectedList = SelectedRutaAutomatico();
                }

                //Se busca que tiendas estan pendientes por subir y los guarda en una variable global para tener un control visual
                PaquetesUpload.CargaRutaPendiente();
                foreach (var item in RutaSelectedList)
                {
                    //Se procede a subir la tienda o tiendas cerradas o terminadas
                    MainViewModel.GetInstance().Tienda.SubiendoRuta();

                    //Se sube la ruta.
                    upstatus = await UploadData.Upload(item, false);
                    if (upstatus)
                    {
                        try
                        {
                            item.Sincronizado = true;
                            App.DB.Entry(item).State = DataBase.EntryState.Modify;
                            App.DB.SaveChanges();

                            //si la ruta se subio con exito se procede a actualizar las banderas para los cambios visuales
                            MainViewModel.GetInstance().Tienda.CargaTerminadaRuta(item.IdRuta, false);

                            EliminaFotosSincronizadas(item);
                        }
                        catch (Exception ex)
                        {
                            MainViewModel.GetInstance().Tienda.CargaTerminadaRuta(item.IdRuta, true);
                            var rutaPendienteLi = MainViewModel.GetInstance().RutaEstatus.getRuta();
                            MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaPendienteLi;
                            Crashes.TrackError(ex, App.TrackData());
                        }
                    }
                    else
                    {
                        MainViewModel.GetInstance().Tienda.CargaTerminadaRuta(item.IdRuta, true);
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoSubioTienda, "BtnUpload_Clicked", "UploadPage", $"No fue posible subir la información de la ruta: { item.IdRuta } - { item.TiendaNombre}");
                        break;
                    }
                    var r = App.DB.Ruta;
                    if (r.Any())
                    {
                        bool be;
                        bool sendRate = bool.TryParse(r.FirstOrDefault().SendQual.ToString(), out be);
                        if (sendRate)
                        {
                            if (be)
                            {
                                //bool c = await BajarCalificacion();
                            }
                        }
                    }

                }
                //Se resetean las variables de rutas pendientes cuando termina de subir las rutas cerradas.
                PaquetesUpload.LimpiarRutaPendiente();
                var rutaPendienteL = MainViewModel.GetInstance().RutaEstatus.getRuta();
                MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaPendienteL;

            }
            return upstatus;
        }
        //Metodo que genera la lista de rutas o ruta a subir dado el caso.
        public List<Ruta> SelectedRuta()
        {

            List<Ruta> RutaSelectedList = new List<Ruta>();
            try
            {
                ReseteaRutaSincronizada();

                if (App.CurrentRoute != null)
                {
                    var ruta = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == App.CurrentRoute.IdRuta);
                    var info = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.CurrentRoute.IdRuta);
                    if (info != null)
                    {
                        if (info.EstatusCheck != null)
                        {
                            if ((bool)info.EstatusCheck)
                            {
                                RutaSelectedList = App.DB.Ruta.Where(re => re.Status == 3 || re.Status == 2 && re.Sincronizado == false && re.IdTiempo == IdTiempo).ToList();
                            }
                        }
                        else
                        {
                            RutaSelectedList.Add(ruta);
                        }
                    }
                    else
                    {
                        RutaSelectedList.Add(ruta);
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
            return RutaSelectedList;
        }
        public List<Ruta> SelectedRutaAutomatico()
        {
            List<Ruta> RutaSelectedList = new List<Ruta>();
            try
            {
                //Verificar si hay respuestaAuditorPendiente, v 4.0.8 ya que se estan desincronizando banderas por las intermitencias de internet.
                ReseteaRutaSincronizada();


                var ruta = App.DB.Ruta.Where(re => re.Status == 3 || re.Status == 2 && re.Sincronizado == false).ToList();
                List<int> ids = ruta.Select(s => s.IdTienda).ToList();
                var info = App.DB.InfoTienda.Where(s => ids.Contains(s.IdTienda) && s.EstatusCheck == true).ToList();

                if (info.Count == 0)
                {
                    RutaSelectedList.AddRange(ruta);
                }


            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
            }
            return RutaSelectedList;
        }
        protected void EliminaFotosSincronizadas(Ruta ruta)
        {
            //Eliminamos las respuestas de las rutas que se suben en estos momentos
            App.DB.RespuestaAuditor.Delete(f => f.IdRuta == ruta.IdRuta && f.Foto != null && f.Sincronizado);
            //Eliminamos si en algún momento llegan a existir fotos sincronizadas de otras rutas antes subidas
            //App.DB.RespuestaAuditor.Delete(f => f.Sincronizado);
            App.DB.SaveChanges();
        }

        //Metodo que regresa el estatus de sincronizado en ruta ya que en v 4.0.7 se detecto que hay intermitencias en la red y se esta marcadno como sincronizada aunque le falten respuestaauditor
        public void ReseteaRutaSincronizada()
        {
            List<int> ids = new List<int>();

            var rs = (from r in App.DB.Ruta.Where(s => s.Sincronizado == true)
                      join ra in App.DB.RespuestaAuditor.Where(s => s.Sincronizado == false && s.Foto != null) on r.IdRuta equals ra.IdRuta
                      select r).ToList();


            if (rs.Count != 0)
            {
                rs = rs.GroupBy(c => c.IdRuta, (key, c) => c.FirstOrDefault()).ToList();
                ids = rs.Select(s => s.IdRuta).ToList();

                var ruta = App.DB.Ruta.Where(s => ids.Contains(s.IdRuta)).ToList();
                foreach (var item in ruta)
                {
                    item.Sincronizado = false;
                    App.DB.Entry(item).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();

                }


                var bit = App.DB.BitacoraSincronizacion.Where(s => ids.Contains(s.IdRuta)).ToList();
                foreach (var item in bit)
                {
                    item.Ruta = false;
                    item.Foto = false;
                    App.DB.Entry(item).State = DataBase.EntryState.Modify;
                    App.DB.SaveChanges();
                }


            }
        }
        #endregion
    }
}