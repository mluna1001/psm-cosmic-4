﻿using Cosmic;
using Microsoft.AppCenter.Crashes;
using PSM.Models;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Client.Result;

namespace PSM.ViewModels
{
    public class SeccionViewModel : BaseViewModel
    {
        #region Propiedades
        public ObservableCollection<SeccionVm> Secciones { get; set; }
        public ObservableCollection<SeccionVm> ListSecciones { get; set; }
        public ObservableCollection<SeccionVm> ListSeccionesFaltantes { get; set; }

        private bool _ListSeccionesFaltantes_IsVisible;
        public bool ListSeccionesFaltantes_IsVisible
        {
            get { return _ListSeccionesFaltantes_IsVisible; }
            set { SetProperty(ref _ListSeccionesFaltantes_IsVisible, value); }
        }

        private bool _ListSecciones_IsVisible;
        public bool ListSecciones_IsVisible
        {
            get { return _ListSecciones_IsVisible; }
            set { SetProperty(ref _ListSecciones_IsVisible, value); }
        }

        private bool _StackFaltantes_IsVisible;
        public bool StackFaltantes_IsVisible
        {
            get { return _StackFaltantes_IsVisible; }
            set { SetProperty(ref _StackFaltantes_IsVisible, value); }
        }

        private ObservableCollection<ToolbarItem> _ToolbarItems;
        public ObservableCollection<ToolbarItem> ToolbarItems
        {
            get { return _ToolbarItems; }
            set { SetProperty(ref _ToolbarItems, value); }
        }

        public bool _faltante { get; set; }

        private bool _ListSecciones_IsEnabled;
        public bool ListSecciones_IsEnabled
        {
            get { return _ListSecciones_IsEnabled; }
            set { SetProperty(ref _ListSecciones_IsEnabled, value); }
        }

        private Seccion _ListSecciones_SelectedItem;
        public Seccion ListSecciones_SelectedItem
        {
            get { return _ListSecciones_SelectedItem; }
            set { SetProperty(ref _ListSecciones_SelectedItem, value); }
        }

        private bool _stackrender_IsVisible;
        public bool stackrender_IsVisible
        {
            get { return _stackrender_IsVisible; }
            set { SetProperty(ref _stackrender_IsVisible, value); }
        }

        private bool _stackloader_IsVisible;
        public bool stackloader_IsVisible
        {
            get { return _stackloader_IsVisible; }
            set { SetProperty(ref _stackloader_IsVisible, value); }
        }

        private bool _ProgressDownloadIndicator_IsVisible;
        public bool ProgressDownloadIndicator_IsVisible
        {
            get { return _ProgressDownloadIndicator_IsVisible; }
            set { SetProperty(ref _ProgressDownloadIndicator_IsVisible, value); }
        }

        private bool _ProgressDownloadIndicator_IsRunning;
        public bool ProgressDownloadIndicator_IsRunning
        {
            get { return _ProgressDownloadIndicator_IsRunning; }
            set { SetProperty(ref _ProgressDownloadIndicator_IsRunning, value); }
        }

        private string _ProgressDownloadText;
        public string ProgressDownloadText
        {
            get { return _ProgressDownloadText; }
            set { SetProperty(ref _ProgressDownloadText, value); }
        }

        #endregion

        public SeccionViewModel(bool faltante = false)
        {
            _faltante = faltante;
            //ListSecciones.ItemSelected += ListSecciones_ItemSelected;
            //ListSecciones.ItemTapped += ListSecciones_ItemTapped;

            //App.CurrentPage = this;
            Secciones = new ObservableCollection<SeccionVm>();
            ListSecciones = Secciones;
            ListSeccionesFaltantes = Secciones;
            ToolbarItems = new ObservableCollection<ToolbarItem>();
            InitPage();

          
        }

        private void InitPage()
        {
            Secciones = new ObservableCollection<SeccionVm>();
            ListSecciones = Secciones;
            ListSeccionesFaltantes = Secciones;
            if (_faltante)
            {
                //var seccionesavalidar = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();
                var seccionesavalidar = App.AllSections.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();
                var idseccionesfaltantes = seccionesavalidar.Select(e => e.IdSeccion).ToList();
                var seccionescontestadas = App.DB.RespuestaAuditor.Where(e => e.IdRuta == App.CurrentRoute.IdRuta).GroupBy(e => e.IdSeccion).Select(e => e.First()).Select(e => e.IdSeccion).ToList();
                var seccionesfaltantes = idseccionesfaltantes.Where(e => !seccionescontestadas.Contains(e));
                var items = seccionesavalidar.Where(e => seccionesfaltantes.Contains(e.IdSeccion)).ToList();
                IsVisible = true;
                ListSeccionesFaltantes_IsVisible = true;
                ListSecciones_IsVisible = false;
                PopulateSecciones(items);
                ImageSource ImagenFondo = ImageSource.FromFile("fondonaranja.jpg");
                StackFaltantes_IsVisible = true;
            }
            else
            {
                ListSeccionesFaltantes_IsVisible = false;
                ListSecciones_IsVisible = true;
                var items = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva).OrderBy(e => e.Orden).ToList();
                Usuario user = App.CurrentUser;
                List<Seccion> filterSections = new List<Seccion>();

                filterSections = LlenaSeccionCadena(items);
                filterSections = LlenaSeccionRegion(filterSections);

                App.AllSections = filterSections;

                PopulateSecciones(filterSections);
            }
            ToolBar();
        }

        private List<Seccion> LlenaSeccionCadena(List<Seccion> items)
        {
            List<Seccion> seccCads = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secCad = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secCad == null)
                    {
                        seccCads.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        InfoTienda tienda = App.DB.InfoTienda.Where(x => x.IdTienda == r.IdTienda).FirstOrDefault();

                        var secCad2 = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion & s.IdCadena == tienda.IdCadena);

                        if (secCad2 != null)
                        {
                            if (secCad2.IdCadena == tienda.IdCadena)
                            {
                                seccCads.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccCads = items;
            }

            return seccCads;
        }

        public List<Seccion> LlenaSeccionRegion(List<Seccion> items)
        {
            List<Seccion> seccRegs = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secReg = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secReg == null)
                    {
                        seccRegs.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        Usuario usuario = App.CurrentUser;

                        var secReg2 = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion && s.IdRegion == usuario.IdRegion);

                        if (secReg2 != null)
                        {
                            if (secReg2.IdRegion == usuario.IdRegion)
                            {
                                seccRegs.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccRegs = items;
            }

            return seccRegs;
        }

        private void PopulateSecciones(List<Seccion> items)
        {
            if (items == null) return;
            foreach (var item in items)
            {
                if (item.IdCatalogoSeccion == 1)
                {
                    bool seEdita = false;

                    if (item.SeEdita != null)
                        seEdita = (bool)item.SeEdita;

                    if (!seEdita)
                    {
                        var fuecontestadalaseccion = App.DB.RespuestaAuditor.Count(ra => ra.IdSeccion == item.IdSeccion && ra.IdRuta == App.CurrentRoute.IdRuta) > 0;
                        if (fuecontestadalaseccion)
                        {
                            continue;
                        }
                    }
                }
                else if (item.IdCatalogoSeccion == 11)
                {
                    continue;
                }
                var it = item.To<SeccionVm>();
                //it.OnItemTapped = new Command<SeccionVm>(ListSecciones_ItemTapped);
                Secciones.Add(it);
            }
        }

        private void ToolBar()
        {
            if (ToolbarItems.Count == 0)
            {
                // El icono de la tabbar se programo acá debido a que en XAML no hay compatibilidad [VS2015]
                var toolbaritem = new ToolbarItemVm
                {
                    Text = "Secciones faltantes",
                    Order = ToolbarItemOrder.Primary
                };
                //toolbaritem.Clicked += SeccionesFaltantes_Clicked;
                toolbaritem.OnItemTapped = new Command<ToolbarItemVm>(OnItemTapped);
                ToolbarItems.Add(toolbaritem);
            }
        }

        //private async void SeccionesFaltantes_Clicked(object sender, EventArgs e)
        public async void OnItemTapped(ToolbarItemVm itemVm)
        {
            await MainPage.Current.Navigation.PushAsync(new SeccionesPage(/*true*/));
        }

        //private async void ListSecciones_ItemTapped(object sender, ItemTappedEventArgs e)
        public async void ListSecciones_ItemTapped(SeccionVm item)
        {
            StartDownload("Cargando Información...");
            await Task.Delay(500);
            Seccion seccion = item.To<Seccion>();
            ListSecciones_IsEnabled = false;
            if (seccion != null)
            {
                var catalogoseccion = App.DB.CatalogoSeccion.ToList();
                var tipodeseccion = catalogoseccion.FirstOrDefault(c => c.IdCatalogoSeccion == seccion.IdCatalogoSeccion);
                if (tipodeseccion != null)
                {
                    App.CurrentSection = seccion;
                    switch (tipodeseccion.Tipo())
                    {
                        case TipoSeccion.HallazgoProducto:
                        case TipoSeccion.PrecioPromo:
                        case TipoSeccion.Precio:
                            await MainPage.Current.Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CalificacionProducto:
                            await MainPage.Current.Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CodigoBarra:
                            await MainPage.Current.Navigation.PushAsync(new BarcodePage() { Title = seccion.Descripcion });
                            break;
                        default:
                            try
                            {
                                Type typepage = Type.GetType($"PSM.{seccion.TargetType},PSM");
                                var page = (Page)Activator.CreateInstance(typepage);
                                page.Title = seccion.Descripcion;
                                await MainPage.Current.Navigation.PushAsync(page);
                            }
                            catch (Exception ex)
                            {
                                await App.Current.MainPage.DisplayAlert("PSM 360", "No podemos abrir esta seccion, contacta con tu supervisor.", "Aceptar");
                            }
                            break;
                    }
                }
                else
                {
                    await App.Current.MainPage.DisplayAlert("PSM 360", "La seccion no tiene un catalogo asignado", "Aceptar");
                }
            }
            else
            {
                await App.Current.MainPage.DisplayAlert("PSM 360", "No podemos abrir esta seccion, contacta con tu supervisor.", "Aceptar");
            }
        }

        private void ListSecciones_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ListSecciones_SelectedItem = null;
        }

        private void StartDownload(string message)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender_IsVisible = false;
                stackloader_IsVisible = true;
                ProgressDownloadIndicator_IsVisible = true;
                ProgressDownloadIndicator_IsRunning = true;
                ProgressDownloadText = message;
            });
        }

        private void EndDownload()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                stackrender_IsVisible = true;
                ProgressDownloadIndicator_IsVisible = false;
                ProgressDownloadIndicator_IsRunning = false;
                ProgressDownloadText = "";
            });
        }
    }
    public class ToolbarItemVm : ToolbarItem
    {
        public Command OnItemTapped { get; set; }
    }
}