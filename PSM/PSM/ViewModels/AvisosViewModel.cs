﻿using Cosmic;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.ViewModels
{
    public class AvisosViewModel : BaseViewModel
    {
        List<AvisoItemViewModel> _AvisosCollection;

        public List<AvisoItemViewModel> AvisosCollection
        {
            get { return _AvisosCollection; }
            set { SetProperty(ref _AvisosCollection, value); }
        }

        public AvisosViewModel()
        {
            GetAvisos();
        }

        void GetAvisos()
        {
            AvisosCollection = App.DB.Aviso.Where(a => a.Vigente).OrderByDescending(a => a.FechaFinal).To<List<AvisoItemViewModel>>();
        }
    }
}
