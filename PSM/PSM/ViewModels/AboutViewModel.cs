﻿using Cosmic;
using Microsoft.AppCenter.Crashes;
using Plugin.Messaging;
using GalaSoft.MvvmLight.Command;
using PSM.Models;
using PSM.Services;
using System;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        #region Attributes
       
        #endregion

        public string Version { get { return "4.0.11"; } }

        public AboutViewModel()
        {
            Title = "About";
            OpenWebCommand = new Command( () => 
            {
                try
                {
                    var phone = CrossMessaging.Current.PhoneDialer;
                    if (phone.CanMakePhoneCall)
                    {
                        phone.MakePhoneCall("8002889353");
                    }
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                }
            });
            MainViewModel.GetInstance().AppVersion = "Versión: " + Version;
            GetPhoneData();

        }
        public ICommand OpenWebCommand { get; }
        public ICommand BackCommand
        {
            get
            {
                return new RelayCommand(Back);
            }
        }
        async void GetPhoneData()
        {
            var imei = App.DeviceIdentifier;
            Telefono telefono = App.DB.Telefono.FirstOrDefault(t => t.IMEI == imei);
            if (telefono is null)
            {
                var battery = Math.Abs(Battery.ChargeLevel * 100);
                var so = DeviceInfo.VersionString;
                var totalStorage = StorageServices.TotalStorageDevice();
                var freeStorage = StorageServices.FreeStorageDevice();
                var apps = App.InstalledApps;

                telefono = new Telefono
                {
                    IdUsuario = App.CurrentUser.IdUsuario,
                    SO = so,
                    DiscoDisponible = freeStorage,
                    DiscoTotal = totalStorage,
                    Bateria = byte.Parse(battery.ToString()),
                    IMEI = imei,
                    AppsInstaladas = apps,
                    VersionApp = this.Version,
                    Sincronizado = false,
                };
            }

            var usuario = App.CurrentUser;
            if (usuario != null)
            {
                var response = await ApiServices.Post<Response, Telefono>(ApiUrl.sSavePhone, telefono, null);
                telefono.Sincronizado = response != null;
                App.DB.Update(telefono);
                App.DB.SaveChanges();
            }

            App.ThisDevice = telefono;
        }
        async void Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }
    }
}