﻿using Cosmic;
using PSM.Models;
using PSM.Function;
using Microsoft.AppCenter.Crashes;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using PSM.Function;
//using PSM.ORM;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;
using PSM.Views;
using System.Threading.Tasks;
using PSM.Helpers;
using PSM.Services;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using static PSM.Services.DataBase;
using System.Globalization;
using Xamarin.Essentials;
using PSM.Interfaces;

namespace PSM.ViewModels
{
    public class ModuloViewModel : BaseViewModel
    {
        bool _IsVisibleStack = true;
        public bool IsVisibleStack
        {
            get { return _IsVisibleStack; }
            set { SetProperty(ref _IsVisibleStack, value); }

        }
        #region Propiedades
        ObservableCollection<SeccionVm> _ListSecciones = new ObservableCollection<SeccionVm>();
        ObservableCollection<SeccionVm> _ListSeccionesFaltantes = new ObservableCollection<SeccionVm>();

        private string _NombreTienda;
        public string NombreTienda
        {
            get { return _NombreTienda; }
            set { SetProperty(ref _NombreTienda, value); }
        }

        private double longitud, latitud;
        private DateTimeOffset? FechaGps { get; set; }

        SeccionVm _ModuleSelect;
        public SeccionVm ModuleSelect
        {
            get { return _ModuleSelect; }
            set { SetProperty(ref _ModuleSelect, value); }

        }

        SeccionVm _SeccionSelect;
        public SeccionVm SeccionSelect
        {
            get { return _SeccionSelect; }
            set { SetProperty(ref _SeccionSelect, value); }

        }

        private ObservableCollection<SeccionVm> _Modulos = new ObservableCollection<SeccionVm>();
        public ObservableCollection<SeccionVm> Modulos
        {
            get { return _Modulos; }
            set { SetProperty(ref _Modulos, value); }
        }
        public Position Position { get; set; }
        private bool Sincronized { get; set; }
        private bool Extra { get; set; }
        public ObservableCollection<SeccionVm> ListSecciones
        {
            get { return _ListSecciones; }
            set { SetProperty(ref _ListSecciones, value); }
        }
        public ObservableCollection<SeccionVm> ListSeccionesFaltantes
        {
            get { return _ListSeccionesFaltantes; }
            set { SetProperty(ref _ListSeccionesFaltantes, value); }
        }
        public bool _faltante { get; set; }
        
        bool _IsLoadingUbication;

        public bool IsLoadingUbication
        {
            get { return _IsLoadingUbication; }
            set { SetProperty(ref _IsLoadingUbication, value); }
        }

        #endregion
        public Informacion Info { get; set; }
        public ModuloViewModel(Informacion inf)
        {
            //Devuelve los modulos asignados a el proyecto 
            int IdRuta = inf.IdRuta;
            Modulos = GetModuloList(IdRuta);
            App.CurrentRoute = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == inf.IdRuta);

            if (inf == null) return;
            NombreTienda = inf.NombreTienda;
            Info = inf;

            // Si el colaborador ya realizó entrada, no ubicaraá hasta su salida
            if (Modulos.Any(m => m.IdCatalogoSeccion == (int)TypeEnum.CatalogoSeccion.Entrada))
            {
                IsVisibleStack = false;
                IsLoadingUbication = true;
            }
            else
            {
                IsVisibleStack = true;
                IsLoadingUbication = false;
            }

            //Devuelve las secciones de el proyecto
            ListSecciones = GetSeccionList();
            ListSeccionesFaltantes = GetSeccionListFaltante();
            Localizacion();


        }

        #region Methods
        private ObservableCollection<SeccionVm> GetModuloList(int IdRuta)
        {
            var listsecciones = new ObservableCollection<SeccionVm>();
            var secciones = App.DB.Seccion.ToList();
            var moduloList = secciones.Where(s => !s.EsSeccion && s.EsActiva && s.IdCatalogoSeccion != (int)TypeEnum.CatalogoSeccion.Secciones).OrderBy(s => s.Orden).ToList();

            var moduls = moduloList.To<List<SeccionVm>>();
            foreach (var item in moduls)
            {
                if (item.IdCatalogoSeccion == 8)
                {
                    var currentasistencia = App.DB.Asistencia
                        .FirstOrDefault(e =>
                            e.IdRuta == IdRuta &&
                                (e.descripcion == App.MODULO_ENTRADA ||
                                e.descripcion == App.MODULO_ENTRADAGLOB));
                    if (currentasistencia != null) continue;
                }
                else if (item.IdCatalogoSeccion == 14)
                {
                    var currentasistencia = App.DB.Asistencia
                        .FirstOrDefault(e =>
                            e.IdRuta == IdRuta &&
                                (e.descripcion == App.MODULO_SALIDA ||
                                e.descripcion == App.MODULO_SALIDAGLOB));
                    if (currentasistencia != null) continue;
                }
                else if (item.IdCatalogoSeccion == 12)
                {
                    continue;
                }
                else
                {
                }

                item.OnItemModuleTapped = new Command<SeccionVm>(OnItemModuleTapped);
                listsecciones.Add(item);
            }
            return listsecciones;
        }
        public ObservableCollection<SeccionVm> GetSeccionList()
        {
            ObservableCollection<SeccionVm> seccionL = new ObservableCollection<SeccionVm>();
            var items = App.DB.Seccion.Where(e => e.EsSeccion && e.EsActiva).OrderBy(e => e.Orden).ToList();
            Usuario user = App.CurrentUser;

            List<Seccion> filterSections = new List<Seccion>();
            filterSections = LlenaSeccionCadena(items);
            filterSections = LlenaSeccionRegion(filterSections);
            App.AllSections = filterSections;
            seccionL = PopulateSecciones(filterSections);
            return seccionL;
        }
        public ObservableCollection<SeccionVm> GetSeccionListFaltante()
        {
            ObservableCollection<SeccionVm> seccionL = new ObservableCollection<SeccionVm>();
            var seccionesavalidar = App.AllSections.Where(e => e.EsSeccion && e.EsActiva && e.SeValida).OrderBy(e => e.Orden).ToList();
            var idseccionesfaltantes = seccionesavalidar.Select(e => e.IdSeccion).ToList();
            var seccionescontestadas = App.DB.RespuestaAuditor.Where(e => e.IdRuta == App.CurrentRoute.IdRuta).GroupBy(e => e.IdSeccion).Select(e => e.First()).Select(e => e.IdSeccion).ToList();
            var seccionesfaltantes = idseccionesfaltantes.Where(e => !seccionescontestadas.Contains(e));
            var items = seccionesavalidar.Where(e => seccionesfaltantes.Contains(e.IdSeccion)).ToList();
            seccionL = PopulateSecciones(items);
            return seccionL;
        }
        private List<Seccion> LlenaSeccionCadena(List<Seccion> items)
        {
            List<Seccion> seccCads = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secCad = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secCad == null)
                    {
                        seccCads.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        InfoTienda tienda = App.DB.InfoTienda.Where(x => x.IdTienda == r.IdTienda).FirstOrDefault();

                        var secCad2 = App.DB.SeccionCadena.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion & s.IdCadena == tienda.IdCadena);

                        if (secCad2 != null)
                        {
                            if (secCad2.IdCadena == tienda.IdCadena)
                            {
                                seccCads.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccCads = items;
            }

            return seccCads;
        }
        public List<Seccion> LlenaSeccionRegion(List<Seccion> items)
        {
            List<Seccion> seccRegs = new List<Seccion>();

            try
            {
                foreach (var seccion in items)
                {
                    var secReg = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion);
                    if (secReg == null)
                    {
                        seccRegs.Add(seccion);
                    }
                    else
                    {
                        Ruta r = App.CurrentRoute;
                        Usuario usuario = App.CurrentUser;

                        var secReg2 = App.DB.SeccionRegion.FirstOrDefault(s => s.IdSeccion == seccion.IdSeccion && s.IdRegion == usuario.IdRegion);

                        if (secReg2 != null)
                        {
                            if (secReg2.IdRegion == usuario.IdRegion)
                            {
                                seccRegs.Add(seccion);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex, App.TrackData());
                seccRegs = items;
            }

            return seccRegs;
        }
        private ObservableCollection<SeccionVm> PopulateSecciones(List<Seccion> items)
        {
            ObservableCollection<SeccionVm> seccionL = new ObservableCollection<SeccionVm>();
            foreach (var item in items)
            {
                if (item.IdCatalogoSeccion == 1)
                {
                    bool seEdita = false;

                    if (item.SeEdita != null)
                        seEdita = (bool)item.SeEdita;

                    if (!seEdita)
                    {
                        var fuecontestadalaseccion = App.DB.RespuestaAuditor.Count(ra => ra.IdSeccion == item.IdSeccion && ra.IdRuta == App.CurrentRoute.IdRuta) > 0;
                        if (fuecontestadalaseccion)
                        {
                            continue;
                        }
                    }
                }
                else if (item.IdCatalogoSeccion == 11)
                {
                    continue;
                }
                var it = item.To<SeccionVm>();
                it.OnItemSelectTapped = new Command<SeccionVm>(OnItemSelectTapped);
                seccionL.Add(it);
            }

            return seccionL;
        }

        private void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            Position = e.Position;
            var dategps = e.Position.Timestamp;
            longitud = e.Position.Longitude;
            latitud = e.Position.Latitude;
            IsVisibleStack = true;
            IsLoadingUbication = false;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RecallUbicacionGps, "Current_PositionChanged", "ModuloViewModel", e);
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }
        #endregion

        #region Commands
        //public ICommand TapModule
        //{
        //    get
        //    {
        //        return new RelayCommand(OnItemModuleTapped);
        //    }
        //}
        public ICommand BackCommand
        {
            get
            {
                return new RelayCommand(Back);
            }
        }
        #endregion

        #region Funciones de cada modulo
        public async void OnItemModuleTapped(SeccionVm svm)
        {
            IsVisibleStack = false;
            IsBusy = true;
            ModuleSelect = svm;
            var seccion = ModuleSelect;
            ValidateDevice.Validate("OnItemTapped", "ModulosPage");
            var rutaM = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta && x.Sincronizado == false).FirstOrDefault();
            var tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == App.CurrentRoute.IdTienda);
            var findProyecto = App.DB.InfoTienda.FirstOrDefault(es => es.EstatusCheck != null);
            bool typeProyecto = false;
            if (findProyecto != null)
                typeProyecto = true;

            var item = seccion;
            string message = "";
            if (item != null)
            {
                IsEnabled = false;
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "Handle_ItemTapped", "ModulosPage", item);

                // Entrada
                if (item.IdCatalogoSeccion == 8)
                {
                    bool answ = false;
                    if (!tienda.Verificada)
                    {
                        answ = await MessageService.ShowDialog("Esta seguro de realizar la entrada para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Cancelar", "Aceptar");
                    }

                    if (tienda.Verificada)
                        answ = true;

                    if (answ)
                    {
                        //Et Bandera para verificar si la entrada es del tipo normal o global
                        bool et = false;
                        //Verifica si el proyecto es del tipo entrada global/salida globar
                        if (tienda.EstatusCheck != null)
                        {
                            et = true;
                            if (typeProyecto && (bool)tienda.EstatusCheck)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    IsVisibleStack = true;
                                    IsBusy = false;
                                    MessageService.ShowMessage("Debes cerrar la actividad inicial");
                                    ModuleSelect = null;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (typeProyecto)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    IsVisibleStack = true;
                                    IsBusy = false;
                                    MessageService.ShowMessage("Debes cerrar la actividad inicial");
                                    //IsEnabled = true;
                                    ModuleSelect = null;
                                    return;
                                }
                            }
                        }
                        //Realiza la entrada true Entrada Global, false Entrada normal por tienda
                        et = await Entrada(et);

                        if (et)
                        {
                            IsVisibleStack = true;
                            IsBusy = false;
                            message = this.Sincronized ? "Sus datos han sido sincronizados." : (this.Extra) ? " Esta es una ruta externa. Al terminar recuerda subir toda la información." : "";
                            MessageService.ShowMessage("Se ha realizado una entrada. " + message);
                            //App.UploadRespuestas();
                            Modulos.Remove(item);
                        }
                        else
                        {
                            IsVisibleStack = true;
                            IsBusy = false;
                            MessageService.ShowMessage("No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre);
                            //IsEnabled = true;
                            ModuleSelect = null;
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaLejosTienda, "Entrada", "ModulosViewModel", $"Intentó realizar entrada en la tienda: { rutaM.TiendaNombre} pero se encuentra lejos de ella");
                            return;
                        }
                        IsVisibleStack = true;
                        IsBusy = false;
                        ModuleSelect = null;
                        //IsEnabled = true;
                    }
                    IsVisibleStack = true;
                    IsBusy = false;
                    ModuleSelect = null;
                    //IsEnabled = true;

                }
                // Salida
                else if (item.IdCatalogoSeccion == 14)
                {
                    bool answ = await MessageService.ShowDialog("Esta seguro de realizar una salida para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Cancelar", "Aceptar");
                    //Se verifica primero que exista la entrada en la tienda
                    if (answ)
                    {
                        if (ExisteEntrada())
                        {
                            if (latitud != 0 || longitud != 0)
                            {
                                // Se verifica que la distancia donde se realizará la salida está entre los 500 mts a la redonda
                                bool cp = CumpleDistancia();
                                bool typeSalida = false;
                                //Si la cadena es tipo actividades no vamos a verificar distancia ya que es un checkin checkout global por dia
                                if (tienda.EstatusCheck != null)
                                {
                                    typeSalida = true;
                                    cp = true;
                                }
                                if (cp)
                                {
                                    if (await Salida(typeSalida))
                                    {
                                        try
                                        {
                                            IsVisibleStack = true;
                                            IsBusy = false;
                                            message = this.Sincronized ? "Sus datos han sido sincronizados." : this.Extra ? "Recuerda subir la información de la ruta." : "";

                                            Modulos.Remove(item);
                                            //Validamos que existan solamente el módulo de entrada y salida.
                                            if (ValidaModuloEntradaSalida())
                                            {
                                                //Si existe, se actualiza la ruta terminada
                                                var tl = MainViewModel.GetInstance().Tienda.ListaTiendas;
                                                var item2 = tl.FirstOrDefault(s => s.IdRuta == rutaM.IdRuta);

                                                item2.ColorCard = "#F6911A";
                                                item2.Estatus = "Estatus: Cerrada";
                                                item2.IdEstatus = 2;

                                                //Se procedea subir las tiendas que esten cerradas o terminadas.
                                                App.UploadRespuestas();
                                                var rutaPendienteL = MainViewModel.GetInstance().RutaEstatus.getRuta();
                                                MainViewModel.GetInstance().RutaEstatus.rutaEstatusL = rutaPendienteL;
                                                await App.Current.MainPage.Navigation.PopAsync();
                                            }
                                            MessageService.ShowMessage("Se ha realizado una salida");

                                        }
                                        catch (Exception ex)
                                        {
                                            Crashes.TrackError(ex);
                                        }
                                    }
                                }
                                else
                                {
                                    IsVisibleStack = true;
                                    IsBusy = false;
                                    if (tienda.Verificada)
                                    {
                                        MessageService.ShowMessage("No se puede realizar la salida debido a que estás lejos de la tienda." + rutaM.IdTienda + ".- " + rutaM.TiendaNombre);
                                    }
                                    else
                                    {
                                        MessageService.ShowMessage("No se puede realizar la salida debido a que estás lejos de tu entrada." + rutaM.IdTienda + ".- " + rutaM.TiendaNombre);
                                    }

                                    ModuleSelect = null;
                                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaLejosTienda, "Salida", "ModuloViewModel", $"Intentó realizar salida en la tienda: { rutaM.TiendaNombre} pero se encuentra lejos de ella");
                                }
                                IsVisibleStack = true;
                                IsBusy = false;
                            }
                            else
                            {
                                IsVisibleStack = false;
                                IsLoadingUbication = true;
                                MessageService.ShowMessage("No hemos encontrado aún tu ubicación.");
                                ModuleSelect = null;
#if DEBUG

                                latitud = 0.000001;
                                longitud = 0.000001;
#endif
                                Localizacion();
                            }
                        }
                        else
                        {
                            IsVisibleStack = true;
                            IsBusy = false;
                            MessageService.ShowMessage("No se ha realizado una entrada en esta tienda.");
                            ModuleSelect = null;
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaSinEntrada, "Salida", "ModuloViewModel", $"Intentó realizar salida en la tienda: { rutaM.TiendaNombre} pero no hay una entrada previa");
                        }
                    }
                    ModuleSelect = null;
                    //IsEnabled = true;

                }
                // Otro modulo
                else
                {
                    Asistencia currentasistencia;
                    bool et = false;
                    if (tienda.EstatusCheck != null)
                        et = true;

                    if (et)
                    {
                        //Verifica si tiene asistencia de entrada para la ruta seleccionada
                        currentasistencia = App.DB.Asistencia.FirstOrDefault(j => j.IdRuta == App.CurrentRoute.IdRuta && j.descripcion == App.MODULO_ENTRADAGLOB);
                    }
                    else
                    {
                        //Verifica si tiene asistencia de entrada para la ruta seleccionada
                        currentasistencia = App.DB.Asistencia.FirstOrDefault(j => j.IdRuta == App.CurrentRoute.IdRuta && j.descripcion == App.MODULO_ENTRADA);
                    }

                    //verifica que la tienda seleccionada no este verificada
                    tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == Info.IdTienda);
                    bool answ = false;

                    if (currentasistencia == null && !tienda.Verificada)
                    {
                        answ = await MessageService.ShowDialog("Esta seguro de realizar la entrada para la tienda: " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre, "Cancelar", "Aceptar");
                        ModuleSelect = null;

                    }
                    if (tienda.Verificada || currentasistencia != null)
                        answ = true;

                    if (answ)
                    {
                        //Verifica si el proyecto es del tipo entrada global/salida globar
                        if (tienda.EstatusCheck != null)
                        {
                            et = true;
                            if (typeProyecto && (bool)tienda.EstatusCheck)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    IsVisibleStack = true;
                                    IsBusy = false;
                                    MessageService.ShowMessage("Debes cerrar la actividad inicial.");
                                    //IsEnabled = true;
                                    ModuleSelect = null;
                                    return;
                                }
                            }
                        }
                        else
                        {
                            if (typeProyecto)
                            {
                                if (!verificaSalidaGlobal())
                                {
                                    IsVisibleStack = true;
                                    IsBusy = false;
                                    MessageService.ShowMessage("Debes cerrar la actividad inicial.");
                                    //IsEnabled = true;
                                    ModuleSelect = null;
                                    return;
                                }
                            }
                        }

                        //Realiza la entrada true Entrada Global, false Entrada normal por tienda
                        et = await Entrada(et);

                        if (et)
                        {
                            IsVisibleStack = true;
                            IsBusy = false;
                            var entrada = Modulos.FirstOrDefault(m => m.IdCatalogoSeccion == 8);

                            Modulos.Remove(entrada);
                        }
                        else
                        {
                            if (tienda.Verificada)
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                MessageService.ShowMessage("No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre);
                            }
                            else
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                MessageService.ShowMessage("No te encuentras cerca de la tienda para realizar entrada. " + rutaM.IdTienda + ".- " + rutaM.TiendaNombre);
                            }
                            //IsEnabled = true;
                            ModuleSelect = null;
                            return;
                        }

                        Type page = Type.GetType($"PSM.{item.TargetType},PSM");
                        if (item.TargetType == "SeccionesPage")
                        {
                            try
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                MainViewModel.GetInstance().Seccion = new SeccionViewModel();
                                ModuleSelect = null;
                                await MainPage.Current.Navigation.PushAsync((Page)Activator.CreateInstance(page, false), true);
                            }
                            catch (Exception ex)
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                MessageService.ShowMessage("No podemos mostrar esta seccion, contacta con tu supervisor");
                                ModuleSelect = null;
                            }
                        }
                        else if (item.TargetType.Equals("IndicadorPage"))
                        {
                            try
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                IndicadorPage pageInd = new IndicadorPage();
                                MainViewModel.GetInstance().Indicador = new IndicadorViewModel(App.CurrentRoute.IdTienda);
                                ModuleSelect = null;
                                await App.Current.MainPage.Navigation.PushAsync(pageInd);
                            }
                            catch (Exception ex)
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                MessageService.ShowMessage("No podemos mostrar esta seccion, contacta con tu supervisor");
                                ModuleSelect = null;
                            }
                        }
                        else
                        {
                            if (item.IdCatalogoSeccion == 17 || item.IdCatalogoSeccion == 11)
                            {
                                App.CurrentSection = item.To<Seccion>();
                            }
                            //Type page2 = Type.GetType("PSM.QuestionPage,PSM");
                            var question = (Page)Activator.CreateInstance(page);
                            if (item.IdCatalogoSeccion == 11)
                            {
                                question.Title = App.CurrentSection.Descripcion;
                                var questionpage = question as QuestionPage;
                                if (questionpage != null)
                                {
                                    questionpage.InicidenciaCompleta += Questionpage_InicidenciaCompleta;
                                }
                            }
                            try
                            {
                                MainViewModel.GetInstance().Abordaje = new AbordajeViewModel();
                                IsVisibleStack = true;
                                IsBusy = false;
                                ModuleSelect = null;

                                //await App.Current.MainPage.Navigation.PushAsync(question, true);
                                await MainPage.Current.Navigation.PushAsync((Page)Activator.CreateInstance(page, false), true);
                            }
                            catch (Exception ex)
                            {
                                IsVisibleStack = true;
                                IsBusy = false;
                                ModuleSelect = null;
                                MessageService.ShowMessage("No podemos mostrar esta seccion, contacta con tu supervisor");
                            }
                        }
                        IsVisibleStack = true;
                        IsBusy = false;
                    }
                    IsVisibleStack = true;
                    IsBusy = false;
                    ModuleSelect = null;
                    //IsEnabled = true;
                }
                IsVisibleStack = true;
                IsBusy = false;
                var resp = await Bitacora.Upload();

                var lt = MainViewModel.GetInstance().Tienda.GetInformacion(rutaM.NombreDelDia);
                MainViewModel.GetInstance().Tienda.ListaTiendas = lt;


            }
            IsVisibleStack = true;
            IsBusy = false;
        }
        private bool ExisteEntrada()
        {
            var RutaAct = App.DB.Ruta.Where(x => x.IdRuta == Info.IdRuta).FirstOrDefault();
            var asistencia = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == RutaAct.IdRuta && a.estatus == true);

            if (asistencia != null)
            {
                return true;
            }
            return false;
        }
        private bool CumpleDistancia(double value = 350)
        {
            double dist = 0, latitudAsis = 0, longitudAsis = 0;

            // Se obtienen los datos de la ruta actual para obtener información de la coordenada
            var RutaAct = App.DB.Ruta.Where(x => x.IdRuta == App.CurrentRoute.IdRuta).FirstOrDefault();
            var asistencia = App.DB.Asistencia.FirstOrDefault(a => a.IdRuta == RutaAct.IdRuta && a.estatus == true);

            if (asistencia != null)
            {
                latitudAsis = (double)asistencia.Latitud;
                longitudAsis = (double)asistencia.Longitud;
            }
            else
            {
                InfoTienda infoTienda = App.DB.InfoTienda.FirstOrDefault(t => t.IdTienda == Info.IdTienda);
                if (infoTienda != null)
                {
                    latitudAsis = (double)infoTienda.Latitud;
                    longitudAsis = (double)infoTienda.Longitud;
                }
            }

            // Se calcula las variables de distancia
            decimal theta = (decimal)longitud - (decimal)longitudAsis;
            double distancia = (Math.Sin(DistanceHelper.Radianes(latitud)) * Math.Sin(DistanceHelper.Radianes(latitudAsis)))
                    + Math.Cos(DistanceHelper.Radianes(latitud)) * Math.Cos(DistanceHelper.Radianes(latitudAsis)) * Math.Cos(DistanceHelper.Radianes((double)theta));

            //var a = Math.Sin(difLatitud / 2).AlCuadrado() + Math.Cos(posOrigen.Latitud.EnRadianes()) * Math.Cos(posDestino.Latitud.EnRadianes()) * Math.Sin(difLongitud / 2).AlCuadrado();

            // se obtiene la distancia en KM
            dist = (DistanceHelper.Grados(Math.Acos(distancia))) * 60 * 1.1515 * 1.609344;

            // se convierte la distancia obtenida a metros
            dist = dist * 1000;

            if (dist <= value)
            {
                return true;
            }

            return false;
        }
        private async void Localizacion()
        {
            var location = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Location);
            switch (location[Permission.Location])
            {
                case Plugin.Permissions.Abstractions.PermissionStatus.Denied:
                    MessageService.ShowMessage("Debes permitir el uso del GPS");
                    break;

                case Plugin.Permissions.Abstractions.PermissionStatus.Granted:
                    break;

                case Plugin.Permissions.Abstractions.PermissionStatus.Unknown:
                case Plugin.Permissions.Abstractions.PermissionStatus.Disabled:
                case Plugin.Permissions.Abstractions.PermissionStatus.Restricted:
                default:
                    MessageService.ShowMessage("Debes permitir el uso del GPS");
                    break;
            }

            if (CrossGeolocator.Current.IsGeolocationAvailable)
            {
                if (CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    try
                    {
                        Position = await CrossGeolocator.Current.GetPositionAsync();
                        var dategps = Position.Timestamp;
                        FechaGps = dategps.Year < DateTime.Now.Year ? DateTime.Now : dategps;
                        longitud = Position.Longitude;
                        latitud = Position.Latitude;

#if DEBUG
                        Console.WriteLine("Nada");
#else
                        var request = new Xamarin.Essentials.GeolocationRequest(GeolocationAccuracy.Medium);
                        var location2 = await Geolocation.GetLocationAsync(request);

                        if (location2.IsFromMockProvider)
                        {
                            MessageService.ShowMessage("Se ha detectado que tu dispositivo utiliza una aplicación que provee ubicación y no utiliza GPS. Por lo tanto no puedes utlizar esta aplicación");
                            var close = DependencyService.Get<ICloseApplication>();
                            close.CloseApp();
                            return;
                        }
#endif
                    }
                    catch { }
                    IsVisibleStack = true;
                    IsLoadingUbication = false;
                }
                else
                {
                    MessageService.ShowMessage("No tienes iniciado el GPS, por favor activalo");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "Constructor", "ModulosPage", "GPS no iniciado");
                }
            }
            else
            {
                MessageService.ShowMessage("Debes permitir el uso del GPS para poder usar esta app");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SinPermisosGps, "Constructor", "ModulosPage", "GPS sin permisos");
            }

            if (Position == null)
            {
                if (!CrossGeolocator.Current.IsListening)
                {
                    try
                    {
                        CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
                        if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 0))
                        {
                        }
                        else
                        {
                            MessageService.ShowMessage("No se pudo iniciar el GPS...");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "Constructor", "ModulosPage", "GPS no iniciado");
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }
        private bool ValidaModuloEntradaSalida()
        {
            var modulos = App.DB.Seccion.Where(x => x.EsSeccion == false).ToList();

            var entradaexists = modulos.Exists(x => x.IdCatalogoSeccion == (int)TypeEnum.CatalogoSeccion.Entrada);
            var salidaexists = modulos.Exists(x => x.IdCatalogoSeccion == (int)TypeEnum.CatalogoSeccion.Salida);

            if ((entradaexists && salidaexists))
            {
                return true;
            }

            return false;
        }
        private async void Questionpage_InicidenciaCompleta(object sender, EventArgs e)
        {
            if (await Salida(false, true))
            {
                MessageService.ShowMessage("Se ha realizado una salida");
                try { await MainPage.Current.Navigation.PopAsync(); } catch { }
            }
        }
        private bool verificaSalidaGlobal()
        {
            var idTiempo = App.CurrentRoute.IdTiempo;

            var asistenciaEntrada = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_ENTRADAGLOB).ToList();
            var ruta = App.DB.Ruta.Where(s => s.IdTiempo == idTiempo).ToList();

            var asistenciadiaEntrada = (from a in asistenciaEntrada
                                        join r in ruta on a.IdRuta equals r.IdRuta
                                        select a).ToList();

            var asistenciaSalida = App.DB.Asistencia.Where(e => e.descripcion == App.MODULO_SALIDAGLOB).ToList();
            var asistenciadiaSalida = (from a in asistenciaSalida
                                       join r in ruta on a.IdRuta equals r.IdRuta
                                       select a).ToList();

            //Valida que se realizo entrada y salida del check por dia
            if (asistenciadiaEntrada.Count() != 0 && asistenciadiaSalida.Count() != 0)
            {
                return true;
            }

            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.VerificaEntradaSalidaGlobal, "EntradaSalidaGlobal", "ModuloViewModel");

            return false;
        }
        public async Task<bool> Entrada(bool isTypeEntrada)
        {
            bool isconnected = true;
            Asistencia currentasistencia;
            string descripcion = "";

            int IdRuta = App.CurrentRoute.IdRuta;

            if (isTypeEntrada)
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_ENTRADAGLOB);
                descripcion = App.MODULO_ENTRADAGLOB;
            }
            else
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_ENTRADA);
                descripcion = App.MODULO_ENTRADA;
            }

            if (currentasistencia != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoEntrada, "Entrada", "ModulosPage", currentasistencia);
                return true;
            }
            else
            {
                var tienda = App.DB.InfoTienda.FirstOrDefault(it => it.IdTienda == App.CurrentRoute.IdTienda);

                if (!isTypeEntrada)
                {
                    if (tienda.Verificada)
                    {
                        if (!CumpleDistancia(350))
                        {
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaLejosTienda, "Entrada", "ModulosPage", "No se encuentra cerca de 350 mts. de la tienda");
                            return false;
                        }
                    }
                }


                Asistencia asistencia = new Asistencia()
                {
                    IdRuta = IdRuta,
                    IdUsuario = App.CurrentUser.IdUsuario,
                    Fecha = FechaGps != null ? DateTime.Parse(FechaGps.ToString()) : DateTime.Now,
                    Longitud = (decimal)longitud,
                    Latitud = (decimal)latitud,
                    estatus = true,
                    descripcion = descripcion
                };
                App.DB.Asistencia.Add(asistencia);
                App.DB.SaveChanges();

                var currentroute = App.DB.Ruta.FirstOrDefault(e => e.IdRuta == IdRuta);
                currentroute.FechaInicio = DateTime.Now;
                currentroute.Status = 1;
                currentroute.GpsLatitude = (decimal)latitud;
                currentroute.GpsLongitude = (decimal)longitud;

                App.DB.Entry(currentroute).State = EntryState.Modify;
                App.DB.SaveChanges();

                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoEntrada, "Entrada", "ModulosPage", asistencia);

                if (App.CurrentRoute.IdRuta > 0)
                {
                    //verifica que va a iniciar la sincronizacion

                    var response = await App.API.UploadEndPoint.UploadEntry(asistencia, () =>
                    {
                        isconnected = false;
                    });

                    if (response != null)
                    {
                        if (response.Status)
                        {
                            if (isconnected)
                            {
                                var bitacoraSinc = App.DB.BitacoraSincronizacion.FirstOrDefault(a => a.IdRuta == IdRuta);

                                if (bitacoraSinc == null)
                                {
                                    bitacoraSinc = new BitacoraSincronizacion();
                                    bitacoraSinc.IdRuta = IdRuta;
                                    App.DB.Add(bitacoraSinc);
                                }
                                bitacoraSinc.Entrada = true;

                                asistencia.Sincronizado = true;
                                App.DB.SaveChanges();
                                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaEnServidor, "Entrada", "ModulosPage", "La entrada ha sido subida al servidor");
                            }
                        }
                    }

                    if (!isconnected)
                    {
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaGuardadaLocalmente, "Entrada", "ModulosPage", "La entrada no se pudo subir al servidor, se guardó en base de datos");
                        this.Sincronized = false;
                        this.Extra = false;
                    }
                    else
                    {
                        this.Sincronized = true;
                        this.Extra = false;
                    }
                }
                else
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.EntradaExternaGuardadaLocalmente, "Entrada", "ModulosPage", "La entrada se guardó en base de datos, es externa");
                    this.Sincronized = false;
                    this.Extra = true;
                }

                App.AllSections = new List<Seccion>();
            }

            return true;
        }
        public async Task<bool> Salida(bool isTypeSalida, bool salidaporinicidencia = false)
        {
            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
            bool isconnected = true;
            int IdRuta = App.CurrentRoute.IdRuta;

            Asistencia currentasistencia;
            string descripcion = "";

            if (isTypeSalida)
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_SALIDAGLOB);
                descripcion = App.MODULO_SALIDAGLOB;
            }
            else
            {
                currentasistencia = App.DB.Asistencia.FirstOrDefault(e => e.IdRuta == IdRuta && e.descripcion == App.MODULO_SALIDA);
                descripcion = App.MODULO_SALIDA;
            }


            if (currentasistencia != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalida, "Salida", "ModulosPage", currentasistencia);
                return true;
            }

            if (salidaporinicidencia)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalidaIncidencia, "Salida", "ModulosPage", "Salida con incidencia");
            }
            else
            {
                var secciones = App.DB.Seccion.Where(e => e.EsActiva && e.EsSeccion).ToList();

                var lstCad = LlenaSeccionCadena(secciones);
                var lstAllSection = LlenaSeccionRegion(lstCad);

                //var seccionesid = secciones.Where(e => e.SeValida).Select(e => e.IdSeccion).ToList();

                var seccionesid = lstAllSection.Where(e => e.SeValida).Select(e => e.IdSeccion).ToList();

                foreach (var id in seccionesid)
                {
                    if (!App.DB.RespuestaAuditor.Exists(ra => ra.IdSeccion == id && ra.IdRuta == IdRuta))
                    {
                        MessageService.ShowMessage("Te hacen falta secciones por llenar");
                        Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaFaltanSecciones, "Salida", "ModuloViewModel", "Salida sin incidencia, te hacen falta secciones por llenar");
                        return false;
                    }
                }
            }

            Asistencia asistencia = new Asistencia()
            {
                IdRuta = App.CurrentRoute.IdRuta,
                IdUsuario = App.CurrentUser.IdUsuario,
                //Fecha = FechaGps != null ? DateTime.Parse(FechaGps.ToString()) : DateTime.Now,
                Fecha = DateTime.Now,
                Longitud = (decimal)longitud,
                Latitud = (decimal)latitud,
                estatus = false,
                descripcion = descripcion
            };

            App.DB.Asistencia.Add(asistencia);
            App.DB.SaveChanges();

            var currentroute = App.DB.Ruta.FirstOrDefault(e => e.IdRuta == IdRuta);
            currentroute.FechaTerminacion = DateTime.Now;
            if (salidaporinicidencia)
            {
                currentroute.Status = 3;
            }
            else
            {
                currentroute.Status = 2;
            }

            App.DB.Entry(currentroute).State = EntryState.Modify;
            App.DB.SaveChanges();
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RealizoSalida, "Salida", "ModuloViewModel", asistencia);

            if (App.CurrentRoute.IdRuta > 0)
            {
                var response = await App.API.UploadEndPoint.UploadEntry(asistencia, () =>
                {
                    isconnected = false;
                });

                if (response != null)
                {
                    if (response.Status)
                    {
                        if (isconnected)
                        {
                            var bitacoraSinc = App.DB.BitacoraSincronizacion.FirstOrDefault(a => a.IdRuta == IdRuta);

                            if (bitacoraSinc == null)
                            {
                                bitacoraSinc = new BitacoraSincronizacion();
                                bitacoraSinc.IdRuta = IdRuta;
                                App.DB.Add(bitacoraSinc);

                            }

                            bitacoraSinc.Salida = true;
                            App.DB.Entry(bitacoraSinc).State = EntryState.Modify;
                            App.DB.SaveChanges();

                            asistencia.Sincronizado = true;
                            App.DB.Entry(asistencia).State = EntryState.Modify;
                            App.DB.SaveChanges();
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaEnServidor, "Salida", "ModuloViewModel", "La salida ha sido subida al servidor");
                        }
                    }
                }

                if (!isconnected)
                {
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaGuardadaLocalmenta, "Salida", "ModuloViewModel", "La salida no se pudo subir al servidor, se guardó en base de datos. Sube esta tienda cuando tengas conexión a internet");
                    this.Sincronized = false;
                }
                else
                {
                    this.Sincronized = true;
                }
            }
            else
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SalidaExternaGuadadaLocalmente, "Salida", "ModuloViewModel", "La salida se guardó en base de datos, es externa");
                this.Sincronized = false;
                this.Extra = true;
            }
            App.AllSections.Clear();
            return true;
        }
        #endregion Funciones de cada modulo

        #region Funciones para secciones
        public async void OnItemSelectTapped(SeccionVm svm)
        {

            IsVisibleStack = false;
            IsBusy = true;
            SeccionSelect = svm;
            var asistencia = App.DB.Asistencia.Where(s => s.IdRuta == App.CurrentRoute.IdRuta).ToList();
            if (asistencia.Count == 0)
            {
                //Para ruta dinamica
                var ruta = App.DB.Ruta.FirstOrDefault(s => s.IdRuta == App.CurrentRoute.IdRuta);
                var ard = App.DB.Asistencia.Where(s => s.IdRuta == ruta.IdRutaDinamica).ToList();
                if (ard.Count == 0)
                {
                    IsVisibleStack = true;
                    IsBusy = false;
                    MessageService.ShowMessage("Primero debe realizar su asistencia a tienda, con el boton de Entrada");
                    return;
                }
            }

            var item = SeccionSelect;

            //StartDownload("Cargando Información...");
            await Task.Delay(500);
            Seccion seccion = item.To<Seccion>();
            //ListSecciones_IsEnabled = false;
            if (seccion != null)
            {
                var catalogoseccion = App.DB.CatalogoSeccion.ToList();
                var tipodeseccion = catalogoseccion.FirstOrDefault(c => c.IdCatalogoSeccion == seccion.IdCatalogoSeccion);
                if (tipodeseccion != null)
                {
                    App.CurrentSection = seccion;
                    switch (tipodeseccion.Tipo())
                    {
                        case TipoSeccion.HallazgoProducto:
                        case TipoSeccion.PrecioPromo:
                        case TipoSeccion.Precio:
                            await MainPage.Current.Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CalificacionProducto:
                            await MainPage.Current.Navigation.PushAsync(new PackagePage(tipodeseccion.Tipo()) { Title = seccion.Descripcion });
                            break;
                        case TipoSeccion.CodigoBarra:
                            await MainPage.Current.Navigation.PushAsync(new BarcodePage() { Title = seccion.Descripcion });
                            break;
                        default:
                            try
                            {
                                Type typepage = Type.GetType($"PSM.{seccion.TargetType},PSM");
                                var page = (Page)Activator.CreateInstance(typepage);
                                page.Title = seccion.Descripcion;
                                await MainPage.Current.Navigation.PushAsync(page);
                            }
                            catch (Exception ex)
                            {
                                MessageService.ShowMessage("No podemos abrir esta seccion, contacta con tu supervisor.");
                            }
                            break;
                    }
                    IsVisibleStack = true;
                    IsBusy = false;
                }
                else
                {
                    IsVisibleStack = true;
                    IsBusy = false;
                    MessageService.ShowMessage("La seccion no tiene un catalogo asignado");
                }
                IsVisibleStack = true;
                IsBusy = false;
            }
            else
            {
                IsVisibleStack = true;
                IsBusy = false;
                MessageService.ShowMessage("No podemos abrir esta seccion, contacta con tu supervisor.");
            }
            IsVisibleStack = true;
            IsBusy = false;
        }

        #endregion Funciones para secciones
        async void Back()
        {
            await App.Current.MainPage.Navigation.PopAsync();
        }

    }

    public class SeccionVm
    {
        public int IdSeccion { get; set; }
        public int IdProyecto { get; set; }
        public int IdCatalogoSeccion { get; set; }
        public int? Orden { get; set; }
        public string Icono { get; set; }
        public string Descripcion { get; set; }
        public double Porcentaje { get; set; }
        public string TargetType { get; set; }
        public bool EsSeccion { get; set; }
        public bool SeValida { get; set; }
        public bool EsActiva { get; set; }
        public bool? SeEdita { get; set; }
        public Command OnItemSelectTapped { get; set; }
        public Command OnItemModuleTapped { get; set; }
    }
}