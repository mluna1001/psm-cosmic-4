﻿using GalaSoft.MvvmLight.Command;
using PSM.Models;
using PSM.Services;
using PSM.TypeEnum;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class AvisoItemViewModel : Aviso
    {
        public ICommand ShowCommand
        {
            get
            {
                return new RelayCommand(Show);
            }
        }

        async void Show()
        {
            var aviso = this;
            switch (aviso.IdTipoAviso)
            {
                case (int)TipoAviso.Informativo:
                    MessageService.ShowMessage(aviso.Mensaje);
                    break;
                case (int)TipoAviso.EnlaceExterno:
                    MainViewModel.GetInstance().AvisoDetalle = new AvisoDetalleViewModel(this);
                    if (Device.RuntimePlatform != Device.UWP)
                        await Shell.Current.Navigation.PushModalAsync(new AvisoDetallePage());
                    else
                        await App.Current.MainPage.Navigation.PushModalAsync(new AvisoDetallePage());
                    break;
            }
        }
    }
}
