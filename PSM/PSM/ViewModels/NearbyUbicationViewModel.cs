﻿using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using PSM.Models;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class NearbyUbicationViewModel : BaseViewModel
    {
        #region Attributes
        bool _StackProgressIsVisible;
        bool _IsVisibleList;
        double _Latitud;
        double _Longitud;
        List<TiendasCercanasUbicacion> _ListaTiendas;
        Position Position;
        #endregion

        #region Properties
        public bool StackProgressIsVisible
        {
            get { return _StackProgressIsVisible; }
            set { SetProperty(ref _StackProgressIsVisible, value); }
        }

        public bool IsVisibleList
        {
            get { return _IsVisibleList; }
            set { SetProperty(ref _IsVisibleList, value); }
        }

        public List<TiendasCercanasUbicacion> ListaTiendas
        {
            get { return _ListaTiendas; }
            set { SetProperty(ref _ListaTiendas, value); }
        }
        #endregion

        #region Constructors
        public NearbyUbicationViewModel()
        {
            StackProgressIsVisible = true;
            IsVisibleList = false;
            DeterminaUbicacion();
        }
        #endregion

        #region Methods
        async void DeterminaUbicacion()
        {
            if (CrossGeolocator.Current.IsGeolocationAvailable)
            {
                if (CrossGeolocator.Current.IsGeolocationEnabled)
                {
                    try
                    {
                        Position = await CrossGeolocator.Current.GetPositionAsync();
                        var dategps = Position.Timestamp;
                        _Longitud = Position.Longitude;
                        _Latitud = Position.Latitude;
                        GetTiendasGPS();
                    }
                    catch { }
                }
                else
                {
                    MessageService.ShowMessage("No tienes iniciado el GPS, por favor activalo");
                    Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "DeterminaUbicacion", "NearbyUbicationPage", "GPS no iniciado");
                }
            }
            else
            {
                MessageService.ShowMessage("Debes permitir el uso del GPS para poder usar esta app");
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.SinPermisosGps, "DeterminaUbicacion", "NearbyUbicationPage", "GPS sin permisos");
            }

            if (Position == null)
            {
                if (!CrossGeolocator.Current.IsListening)
                {
                    try
                    {
                        CrossGeolocator.Current.PositionChanged += Current_PositionChanged; ;
                        if (await CrossGeolocator.Current.StartListeningAsync(TimeSpan.FromSeconds(5), 0))
                        {
                        }
                        else
                        {
                            MessageService.ShowMessage("No se pudo iniciar el GPS...");
                            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.NoGpsIniciado, "DeterminaUbicacion", "NearbyUbicationPage", "GPS no iniciado");
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }


        private void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            Position = e.Position;
            var dategps = e.Position.Timestamp;
            _Longitud = e.Position.Longitude;
            _Latitud = e.Position.Latitude;
            Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.RecallUbicacionGps, "Current_PositionChanged", "NearbyUbicationPage", e);
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }

        async void GetTiendasGPS()
        {

            bool isconnected = true;

            Dictionary<string, string> form = new Dictionary<string, string>
            {
                { "idusuario", App.CurrentUser.IdUsuario.ToString() },
                { "coordenaday", _Latitud.ToString() },
                { "coordenadax", _Longitud.ToString() }
            };

            ListaTiendas = await Task.Run(() => App.API.NearbyUbicationEndPoint.NearbyUbicationStores(App.CurrentUser.IdUsuario, _Latitud, _Longitud, () =>
                isconnected = false));

            StackProgressIsVisible = false;
            IsVisibleList = true;

            //var response = await ApiServices.Post<Tienda>;
        }
        public ICommand RefreshCommand
        {
            get
            {
                return new Command(() =>
                {
                    IsRefreshing = true;

                    GetTiendasGPS();

                    IsRefreshing = false;
                });
            }
        }
        #endregion
    }
}
