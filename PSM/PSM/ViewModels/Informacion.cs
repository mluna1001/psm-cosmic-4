﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class Informacion : INotifyPropertyChanged
    {
        public int IdRuta { get; set; }
        public int IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Direccion { get; set; }
        public Command OnTiendaClick { get; set; }
        public Command OnModuloClick { get; set; }
        public Command OnUploadClick { get; set; }
        public string Fecha { get; set; }
        public string Cadena { get; set; }
        public string NombreDia { get; set; }
        public int IdEstatus { get; set; }

        string estatus;
        public string Estatus
        {
            get { return estatus; }
            set { SetProperty(ref estatus, value); }
        }
       
        public bool Sincronizado { get; set; }



        string colorCard;
        public string ColorCard
        {
            get { return colorCard; }
            set { SetProperty(ref colorCard, value); }
        }
      

        bool procesando = false;
        public bool Procesando
        {
            get { return procesando; }
            set { SetProperty(ref procesando, value); }
        }

        bool upVisible = false;
        public bool UpVisible
        {
            get { return upVisible; }
            set { SetProperty(ref upVisible, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }


}
