﻿using PSM.Helpers;
using PSM.Models;
using PSM.Models.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace PSM.ViewModels
{
    public class UbicacionViewModel : BaseViewModel
    {
        public List<PointMapDto> _items;

        public List<PointMapDto> Items
        {
            get => _items;
            set
            {
                _items = value;
                OnPropertyChanged(nameof(Items));
            }
        }
        public UbicacionViewModel()
        {
            Default();
            OpenWebCommand = new Command(async () => await App.Current.MainPage.Navigation.PopAsync());

        }
        public ICommand OpenWebCommand { get; }
        #region Propiedades
        private int _IdTienda;
        public int IdTienda
        {
            get { return _IdTienda; }
            set { SetProperty(ref _IdTienda, value); }
        }

        #endregion

        #region Methods
        public void Default()
        {
            var it = App.DB.InfoTienda.FirstOrDefault(s => s.IdTienda == App.IdTienda);

          
            if (it != null)
            {
                List<PointMapDto> pml = new List<PointMapDto>();

                var pm = new PointMapDto()
                {
                    Id = it.IdTienda,
                    Name = it.NombreTienda.Trim(),
                    Latitude = (double)it.Latitud,
                    Longitude = (double)it.Longitud,
                    //Description = "Entrada: " + i.FechaEntrada.ToString()
                };

                pml.Add(pm);

                Items = pml;
            }
        }
        #endregion
    }
}