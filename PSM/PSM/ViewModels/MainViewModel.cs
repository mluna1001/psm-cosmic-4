﻿using PSM.Models;
using System;
namespace PSM.ViewModels
{
    using PSM.Services;

    public class MainViewModel : BaseViewModel
    {
        #region Properties
        public NavigationService navigationService;
        public string AppVersion { get; set; }
        public string NombreUsuario { get; set; }
        public string AliasUsuario { get; set; }
        public string FechaIngreso { get; set; }
        public bool IsVisibleUsers { get; set; }

        #endregion

        #region ViewModels
        public LoginViewModel Login { get; set; }
        public TiendaViewModel Tienda { get; set; }
        public ModuloViewModel Modulo { get; set; }
        public IndicadorViewModel Indicador { get; set; }
        public DescargaViewModel Descarga { get; set; }
        public UbicacionViewModel Ubicacion { get; set; }
        public DetalleIndicadorViewModel DetalleIndicador { get; set; }
        public AboutViewModel About { get; set; }
        public RutaEstatusViewModel RutaEstatus { get; set; }
        public RutaEstatusItemViewModel RutaEstatusItem { get; set; }
        public PerfilViewModel Perfil { get; set; }
        public SemaforoViewModel Semaforo { get; set; }
        public SemaforoMapViewModel SemaforoMap { get; set; }
        public SemaforoItemViewModel SemaforoItemViewModel { get; set; }
        public AvisosViewModel Avisos { get; set; }
        public AvisoDetalleViewModel AvisoDetalle { get; set; }
        public NearbyUbicationViewModel NearbyUbication { get; set; }
        public NearbyRouteViewModel NearbyRoute { get; set; }
        public PackageViewModel Package { get; set; }
        public SeccionViewModel Seccion { get; set; }
        public ImpersonalViewModel Impersonal { get; set; }
        public AbordajeViewModel Abordaje { get; set; }
        public AbordajeMultimediaViewModel AbordajeMultimedia { get; set; }
        #endregion

        #region Singleton
        public static MainViewModel instance;
        public static MainViewModel GetInstance()
        {
            if (instance is null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            navigationService = new NavigationService();
            LoadMenu();
            IsVisibleUsers = true;
        }
        #endregion

        #region Commands

        #endregion

        #region Methods
        public void LoadMenu()
        {
            if (App.CurrentUser != null)
            {
                Bitacora.CanInsert = true;
                this.NombreUsuario = App.CurrentUser.Nombre;
                this.AliasUsuario =  App.CurrentUser.Alias;
                this.FechaIngreso = DateTime.Now.ToString();
            }
        }
        #endregion
    }
}