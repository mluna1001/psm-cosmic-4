﻿using Cosmic;
using PSM.Models.Dto;
using PSM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PSM.ViewModels
{
    public class IndicadorViewModel : BaseViewModel
    {
        #region Attributes
        string _Alias;
        string _Nombre;
        List<IndicadorItemViewModel> _Indicadores;
        #endregion

        #region Properties
        public string Alias
        {
            get { return _Alias; }
            set { SetProperty(ref _Alias, value); }
        }

        public string Nombre
        {
            get { return _Nombre; }
            set { SetProperty(ref _Nombre, value); }
        }

        public List<IndicadorItemViewModel> Indicadores
        {
            get { return _Indicadores; }
            set { SetProperty(ref _Indicadores, value); }
        } 
        #endregion

        public IndicadorViewModel(int idTienda = 0)
        {
            GetInfoUser();
            GetIndicatorData(idTienda);
        }

        #region Methods
        void GetInfoUser()
        {
            Nombre = "Usuario";
            Alias = App.CurrentUser.Alias;
            Nombre = string.IsNullOrEmpty(App.CurrentUser.Nombre) ? "" : App.CurrentUser.Nombre;
        }

        async void GetIndicatorData(int IdTienda = 0)
        {
            if (!App.DB.Indicador.Any(u => u.IdUsuario == App.CurrentUser.IdUsuario))
            {
                var dictionary = new Dictionary<string, string>
                {
                    { "idUsuario", App.CurrentUser.IdUsuario.ToString() }
                };

                var response = await ApiServices.Post<Response>(ApiUrl.sGetIndicadores, dictionary);

                if (response != null)
                {
                    IndicadorDto indicador = response.Data.To<IndicadorDto>();
                    if (indicador != null && (indicador.Indicadores.Count > 0))
                    {
                        foreach (var item in indicador.Indicadores)
                        {
                            App.DB.Indicador.Add(item);
                            App.DB.SaveChanges();
                        }

                        foreach (var item in indicador.CatalogoIndicador)
                        {
                            App.DB.CatalogoIndicador.Add(item);
                            App.DB.SaveChanges();
                        }
                    }
                }
            }

            if (IdTienda == 0)
                Indicadores = App.DB.Indicador.Where(i => i.IdUsuario == App.CurrentUser.IdUsuario).To<List<IndicadorItemViewModel>>();
            else
                Indicadores = App.DB.Indicador.Where(i => i.IdUsuario == App.CurrentUser.IdUsuario && i.IdTienda == IdTienda).To<List<IndicadorItemViewModel>>();

            if (Indicadores == null || Indicadores.Count == 0)
            {
                if (IdTienda == 0)
                {
                    MessageService.ShowMessage("En estos momentos no hay indicadores registrados para tu usuario.");
                }
                else
                {
                    MessageService.ShowMessage("No hay indicador aplicable para esta tienda.");
                }
            }
        }
        #endregion

    }
}
