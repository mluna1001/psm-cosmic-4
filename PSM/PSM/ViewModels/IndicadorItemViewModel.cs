﻿using GalaSoft.MvvmLight.Command;
using Xamarin.Forms;
using PSM.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using PSM.Views;

namespace PSM.ViewModels
{
    public class IndicadorItemViewModel : Indicador
    {
        public ICommand DetalleCommand
        {
            get
            {
                return new RelayCommand(Detalle);
            }
        }

        async void Detalle()
        {
            MainViewModel.GetInstance().DetalleIndicador = new DetalleIndicadorViewModel(this);
            if (Device.RuntimePlatform != Device.UWP)
                await Shell.Current.Navigation.PushAsync(new DetalleIndicadorPage());
            else
                await App.Current.MainPage.Navigation.PushAsync(new DetalleIndicadorPage());
        }
    }
}
