﻿using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class HallazgoPackageModel
    {
        public Page Page { get; set; }
        public Command BtnBackCommand { get; set; }
        public Command BtnNextCommand { get; set; }
        public string BtnBackText { get; set; }
        public string BtnNextText { get; set; }
        public int Index { get; internal set; }
    }
}
