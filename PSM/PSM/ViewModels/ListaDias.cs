﻿namespace PSM.ViewModels
{
    public class ListaDias
    {
        public int IdDia { get; set; }
        public string Dia { get; set; }
        public string DiaAvr { get; set; }
    }

}
