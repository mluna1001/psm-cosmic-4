﻿using Android.Content.Res;
using GalaSoft.MvvmLight.Command;
using PSM.Models;
using PSM.Services;
using PSM.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class AbordajeMultimediaViewModel : BaseViewModel
    {

        ObservableCollection<MultimediaModel> _abordajeL = new ObservableCollection<MultimediaModel>();
        public ObservableCollection<MultimediaModel> AbordajeL
        {
            get { return _abordajeL; }
            set { SetProperty(ref _abordajeL, value); }
        }
        Abordaje _abordaje = new Abordaje();
        public Abordaje Abordaje
        {
            get { return _abordaje; }
            set { SetProperty(ref _abordaje, value); }
        }
        string _Titulo = "";
        public String Titulo
        {
            get { return _Titulo; }
            set { SetProperty(ref _Titulo, value); }
        }

        public AbordajeMultimediaViewModel(List<Multimedia> multimedia, int idseccionmenu, string descripcion)
        {

            Titulo = descripcion;
            getListAbordaje(multimedia, idseccionmenu);

        }

        public void getListAbordaje(List<Multimedia> multimedia, int idseccionmenu)
        {
            Abordaje = new Abordaje
            {
                IdSeccionMenu = idseccionmenu,
                Inicio = DateTime.Now,
                IdRuta = App.CurrentRoute.IdRuta,
                Number = App.DB.Abordaje.Count(e => e.IdRuta == App.CurrentRoute.IdRuta)
            };

            AbordajeL = new ObservableCollection<MultimediaModel>();
            foreach (var item in multimedia)
            {
                string[] ext = item.Nombre.Split('.');

                var iconsource = item.GetIcon();
                AbordajeL.Add(new MultimediaModel
                {
                    Categoria = item.Categoria,
                    IdMultimedia = item.IdMultimedia,
                    IdSeccionMenu = item.IdSeccionMenu,
                    Nombre = item.Nombre,
                    url = item.url,
                    Vigencia = item.Vigencia,
                    Source = $"localpath/{item.Nombre}",
                    Icon = ext[1] == "mp4" ? "video_icon.png" :
                           ext[1] == "pdf" ? "pdf_icon.png" :
                           ext[1] == "jpg" ? "jpg_icon.png" :
                           "#F5A9BC",
                    ColorCard = ext[1] == "mp4" ? "#2AAAE1" :
                                ext[1] == "pdf" ? "#F6911A" :
                                ext[1] == "jpg" ? "#98C93C" :
                                "#F5A9BC",
                    SelectItemAbordaje = new Command<MultimediaModel>(SelectItemAbordaje)
                });
            }
        }
        public async void SelectItemAbordaje(MultimediaModel am)
        {

            if (am != null)
            {
                Bitacora.Insert((int)TypeEnum.IdCatalogoBitacora.TapDisplayAction, "ListMultimedia_ItemTapped", "MultimediaList");
                MultimediaPage visualizer = new MultimediaPage(am, Abordaje);
                if (visualizer.Render())
                {
                    if (Device.RuntimePlatform != Device.UWP)
                        await Shell.Current.Navigation.PushModalAsync(visualizer);
                    else
                        await App.Current.MainPage.Navigation.PushModalAsync(visualizer);
                }
                else
                {
                    MessageService.ShowMessage("No se pudo mostrar el archivo, contacta con tu supervisor");
                }
            }
        }

        public ICommand BackAbordajeCommand
        {
            get
            {
                return new RelayCommand(Back);
            }
        }
        async void Back()
        {  
            if (Abordaje != null)
            {
                Abordaje.Fin = DateTime.Now;
                App.DB.Add(Abordaje);
                App.DB.SaveChanges();
                Abordaje = null;
            }
            if (Device.RuntimePlatform == Device.UWP)
                await App.Current.MainPage.Navigation.PopModalAsync();
            else
                await Shell.Current.Navigation.PopModalAsync();
        }


        public class MultimediaModel : Multimedia
        {
            public object Icon { get; set; }
            public string Source { get; set; }
            public Abordaje Abordaje { get; set; }
            public string ColorCard { get; set; }
            public Command SelectItemAbordaje { get; set; }
        }
    }
}

