﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Interfaces
{
    public interface IStorageDevice
    {
        string GetFreeStorageDevice();

        string GetFullStorageDevice();

        string GetBatteryLevel();
    }
}
