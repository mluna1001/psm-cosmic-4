﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Interfaces
{
    public interface ICheckAutomaticTime
    {
        bool IsAutomaticTime();

        bool IsAutomaticZone();
    }
}
