﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Interfaces
{
    public interface IQrCodeScanningService
    {
        Task<string> ScanAsync();
    }
}
