﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Interfaces
{
    public interface IFileProcess
    {
        JsonSerializer GetTextWriter(object objeto);
    }
}
