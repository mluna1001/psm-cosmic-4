﻿namespace PSM.Interfaces
{
    using System;
    using System.Collections.Generic;

    public interface IUniqueAndroidId
    {
        string GetIdentifier();

        List<Tuple<string, string>> GetInstalledApps();
    }
}
