﻿namespace PSM.Interfaces
{
    using PSM.Common;

    public interface IDbService
    {
        MobileResponse GetDataBase();
    }
}
