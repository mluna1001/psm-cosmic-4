﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Interfaces
{
    public interface ICloseApplication
    {
        void CloseApp();
    }
}
