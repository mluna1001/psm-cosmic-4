﻿namespace PSM.Interfaces
{
    using PSM.Common;
    using PSM.Models;

    public interface IBackground
    {
        Background GetDataBase();
    }
}
