﻿using GalaSoft.MvvmLight.Command;
using PSM.Services;
using PSM.ViewModels;
using SQLite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PSM.Models
{
    public class TiendasCercanasRuta
    {
        [PrimaryKey]
        public int IdTienda { get; set; }
        public int IdProyectoTienda { get; set; }
        public int IdCadena { get; set; }
        public string TiendaNombre { get; set; }
        public string Direccion { get; set; }
        public Nullable<decimal> CoordenadaX { get; set; }
        public Nullable<decimal> CoordenadaY { get; set; }
        public bool Verificada { get; set; }

        public ICommand SaveRouteCommand
        {
            get
            {
                return new RelayCommand(SaveRoute);
            }
        }

        void SaveRoute()
        {
            DateTime fecha = DateTime.Now;
            DateTimeFormatInfo dfy = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfy.Calendar;
            int nodia = (int)DateTime.Now.DayOfWeek;
            string nombreDia = GetDias().Where(d => d.IdDia == nodia).First().Dia;

            int semanaActual = cal.GetWeekOfYear(fecha, dfy.CalendarWeekRule, dfy.FirstDayOfWeek);

            var idRuta = App.DB.Ruta.Where(r => r.IdRuta < 0).OrderBy(r => r.IdRuta).FirstOrDefault();
            var idTiempo = App.DB.Tiempo.Where(r => r.NombreDia.Equals(nombreDia)).FirstOrDefault();

            if (idTiempo != null)
            {
                int difSemana = (semanaActual - idTiempo.SemanadelAño) * 7;

                Ruta rutaAlta = new Ruta();
                rutaAlta.IdProyecto = App.CurrentUser.IdProyecto;
                rutaAlta.IdUsuario = App.CurrentUser.IdUsuario;
                rutaAlta.IdTiempo = idTiempo.IdTiempo + difSemana;
                rutaAlta.Status = 0;
                rutaAlta.IdTienda = this.IdTienda;
                rutaAlta.IdProyectoTienda = this.IdProyectoTienda;
                rutaAlta.TiendaNombre = this.TiendaNombre;
                rutaAlta.NumeroSemana = semanaActual;
                rutaAlta.NombreDelDia = nombreDia;
                rutaAlta.FechaInicio = null;
                rutaAlta.FechaTerminacion = null;
                rutaAlta.GpsLatitude = null;
                rutaAlta.GpsLongitude = null;
                rutaAlta.Sincronizado = false;
                rutaAlta.IdRuta = idRuta != null ? (idRuta.IdRuta - 1) : -1;

                App.DB.Ruta.Add(rutaAlta);
                App.DB.SaveChanges();

                ExisteInfoTienda(this);

                string dia = MainViewModel.GetInstance().Tienda.Dia;
                var rutaL = MainViewModel.GetInstance().Tienda.GetInformacion(dia);
                MainViewModel.GetInstance().Tienda.ListaTiendas = rutaL;

                MessageService.ShowMessage($"Se ha agregado la tienda {this.TiendaNombre} para visitarla el día de hoy");
            }
            else
            {
                MessageService.ShowMessage($"Es necesario descargar catálogos antes de la creación de esta ruta");
                return;
            }
        }

        void ExisteInfoTienda(TiendasCercanasRuta tienda)
        {
            InfoTienda it = App.DB.InfoTienda.FirstOrDefault(t => t.IdTienda == tienda.IdTienda);

            if (it == null)
            {
                InfoTienda info = new InfoTienda()
                {
                    //IdInfoTienda = idInfoTienda,
                    IdTienda = tienda.IdTienda,
                    Latitud = tienda.CoordenadaY,
                    Longitud = tienda.CoordenadaX,
                    IdCadena = tienda.IdCadena,
                    NombreTienda = tienda.TiendaNombre,
                    ImagenUrl = null,
                    Direccion = "",
                    Verificada = tienda.Verificada
                };

                App.DB.InfoTienda.Add(info);
                App.DB.SaveChanges();
            }
        }

        private IList<ListaDias> GetDias()
        {
            return new List<ListaDias>
            {
                new ListaDias { IdDia=0, Dia="Domingo" },
                new ListaDias { IdDia=1, Dia="Lunes" },
                new ListaDias { IdDia=2, Dia="Martes" },
                new ListaDias { IdDia=3, Dia="Miercoles" },
                new ListaDias { IdDia=4, Dia="Jueves" },
                new ListaDias { IdDia=5, Dia="Viernes" },
                new ListaDias { IdDia=6, Dia="Sabado" },
            };
        }
    }
}
