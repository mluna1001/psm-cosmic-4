﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Models.Dto
{
    public class PointMapDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rate { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
