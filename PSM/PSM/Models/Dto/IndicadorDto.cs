﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Models.Dto
{
    public class IndicadorDto
    {
        public List<Indicador> Indicadores { get; set; }
        public List<CatalogoIndicador> CatalogoIndicador { get; set; }
    }
}
