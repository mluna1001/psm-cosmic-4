﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Models.Dto
{
    public class RutaDto
    {
        public int IdRuta { get; set; }
        public int IdTiempo { get; set; }
        public string TiendaNombre { get; set; }
        public int NumeroSemana { get; set; }
        public string NombreDelDia { get; set; }
        public string Estatus { get; set; }
        public int Status { get; set; }
        public string BackgroundColor { get; set; }
        public string Fecha { get; set; }
        public DateTime? FechaRuta { get; set; }
        public DateTime? FechaInicio { get; set; }
        public DateTime? FechaTerminacion { get; set; }
        public bool Sincronizado { get; set; }
    }
}
