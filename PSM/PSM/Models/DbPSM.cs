﻿namespace PSM.Models
{
    using PSM.Interfaces;
    using PSM.Services;
    using System;
    using Xamarin.Forms;

    public class DbPSM : DataBase
    {
        public DbPSM(string databasePath, bool storeDateTimeAsTicks = true) : base(databasePath, storeDateTimeAsTicks)
        {
            CreateTables();
        }

        private static DbPSM instance;
        public static DbPSM GetInstance()
        {
            if (instance == null)
            {
                var srv = DependencyService.Get<IDbService>();
                if (srv == null) throw new NullReferenceException("La dependencia de servicio es null");
                var db = srv.GetDataBase();
                if (db.Status)
                    instance = (DbPSM)db.Objeto;
            }
            return instance;
        }

        public void CreateTables()
        {
            Abordaje = DBSet<Abordaje>();
            AbordajeMultimedia = DBSet<AbordajeMultimedia>();
            Asistencia = DBSet<Asistencia>();
            Compania = DBSet<Compania>();
            CatalogoSeccionMenu = DBSet<CatalogoSeccionMenu>();
            CatalogoSeccion = DBSet<CatalogoSeccion>();
            CatalogoIndicador = DBSet<CatalogoIndicador>();
            Usuario = DBSet<Usuario>();
            Ruta = DBSet<Ruta>();
            Indicador = DBSet<Indicador>();
            InfoTienda = DBSet<InfoTienda>();
            Producto = DBSet<Producto>();
            PreguntaCadena = DBSet<PreguntaCadena>();
            Seccion = DBSet<Seccion>();
            SeccionCadena = DBSet<SeccionCadena>();
            SeccionRegion = DBSet<SeccionRegion>();
            ProductoClasificacion = DBSet<ProductoClasificacion>();
            Pregunta = DBSet<Pregunta>();
            RespuestaPreguntaHija = DBSet<RespuestaPreguntaHija>();
            Multimedia = DBSet<Multimedia>();
            Ubicacion = DBSet<Ubicacion>();
            Respuesta = DBSet<Respuesta>();
            Asistencia = DBSet<Asistencia>();
            Aviso = DBSet<Aviso>();
            Imagen = DBSet<Imagen>();
            MenuUser = DBSet<MenuUser>();
            RespuestaAuditor = DBSet<RespuestaAuditor>();
            Foto = DBSet<Foto>();
            Compania = DBSet<Compania>();
            ProductoCadena = DBSet<ProductoCadenaDto>();
            Producto = DBSet<Producto>();
            Ubicacion = DBSet<Ubicacion>();
            SeccionRuta = DBSet<SeccionRuta>();
            CatalogoSeccionMenu = DBSet<CatalogoSeccionMenu>();
            Multimedia = DBSet<Multimedia>();
            CatalogoSeccion = DBSet<CatalogoSeccion>();
            Abordaje = DBSet<Abordaje>();
            AbordajeMultimedia = DBSet<AbordajeMultimedia>();
            ChatGrupo = DBSet<ChatGrupo>();
            SeccionCadena = DBSet<SeccionCadena>();
            SeccionRegion = DBSet<SeccionRegion>();
            Qualification = DBSet<Qualification>();
            QualiSection = DBSet<QualiSection>();
            QualiPregunta = DBSet<QualiPregunta>();
            TiendasCercanasRuta = DBSet<TiendasCercanasRuta>();
            Tiempo = DBSet<Tiempo>();
            PreguntaCadena = DBSet<PreguntaCadena>();
            BitacoraSincronizacion = DBSet<BitacoraSincronizacion>();
            BitacoraSincFoto = DBSet<BitacoraSincFoto>();
            CatalogoIndicador = DBSet<CatalogoIndicador>();
            Indicador = DBSet<Indicador>();
            ProductoClasificacion = DBSet<ProductoClasificacion>();
            ReporteIndicador = DBSet<ReporteIndicador>();
            Telefono = DBSet<Telefono>();
            BackupUsuario = DBSet<BackupUsuario>();
            BackupMenuUser = DBSet<BackupMenuUser>();
            BackupAvisos = DBSet<BackupAviso>();
            TokenDevice = DBSet<TokenDevice>();
        }
        public void DropTables()
        {
            DropTable<Abordaje>();
            DropTable<AbordajeMultimedia>();
            DropTable<Compania>();
            DropTable<CatalogoSeccionMenu>();
            DropTable<CatalogoIndicador>();
            DropTable<Usuario>();
            DropTable<Ruta>();
            DropTable<Indicador>();
            DropTable<Producto>();
            DropTable<Ubicacion>();
            DropTable<InfoTienda>();
            DropTable<Multimedia>();
            DropTable<ProductoClasificacion>();
            DropTable<Seccion>();
            DropTable<Pregunta>();
            DropTable<RespuestaPreguntaHija>();
            DropTable<ProductoCadenaDto>();
            DropTable<PreguntaCadena>();
            DropTable<Respuesta>();
            DropTable<Asistencia>();
            DropTable<Aviso>();
            DropTable<Imagen>();
            DropTable<MenuUser>();
            DropTable<RespuestaAuditor>();
            DropTable<Foto>();
            DropTable<Compania>();
            DropTable<Producto>();
            DropTable<Ubicacion>();
            DropTable<SeccionRuta>();
            DropTable<CatalogoSeccionMenu>();
            DropTable<Multimedia>();
            DropTable<CatalogoSeccion>();
            DropTable<Abordaje>();
            DropTable<AbordajeMultimedia>();
            DropTable<SeccionCadena>();
            DropTable<SeccionRegion>();
            DropTable<Qualification>();
            DropTable<QualiSection>();
            DropTable<QualiPregunta>();
            DropTable<TiendasCercanasRuta>();
            DropTable<Tiempo>();
            DropTable<PreguntaCadena>();
            DropTable<BitacoraSincronizacion>();
            DropTable<BitacoraSincFoto>();
            DropTable<CatalogoIndicador>();
            DropTable<Indicador>();
            DropTable<ProductoClasificacion>();
            DropTable<ReporteIndicador>();
            DropTable<Telefono>();
            DropTable<BackupUsuario>();
            DropTable<BackupMenuUser>();
            DropTable<BackupAviso>();
            DropTable<TokenDevice>();
        }

        #region Tables
        public Table<Usuario> Usuario { get; set; }
        public Table<BackupUsuario> BackupUsuario { get; set; }
        public Table<Abordaje> Abordaje { get; set; }
        public Table<AbordajeMultimedia> AbordajeMultimedia { get; set; }
        public Table<Asistencia> Asistencia { get; set; }
        public Table<Compania> Compania { get; set; }
        public Table<CatalogoSeccionMenu> CatalogoSeccionMenu { get; set; }
        public Table<CatalogoIndicador> CatalogoIndicador { get; set; }
        public Table<Ruta> Ruta { get; set; }
        public Table<InfoTienda> InfoTienda { get; set; }
        public Table<Producto> Producto { get; set; }
        public Table<Pregunta> Pregunta { get; set; }
        public Table<RespuestaPreguntaHija> RespuestaPreguntaHija { get; set; }
        public Table<Seccion> Seccion { get; set; }
        public Table<Ubicacion> Ubicacion { get; set; }
        public Table<Multimedia> Multimedia { get; set; }
        public Table<ProductoClasificacion> ProductoClasificacion { get; set; }
        public Table<ProductoCadenaDto> ProductoCadena { get; set; }
        public Table<PreguntaCadena> PreguntaCadena { get; set; }
        public Table<SeccionCadena> SeccionCadena { get; set; }
        public Table<SeccionRegion> SeccionRegion { get; set; }
        public Table<Indicador> Indicador { get; set; }
        public Table<Respuesta> Respuesta { get; set; }
        public Table<MenuUser> MenuUser { get; set; }
        public Table<BackupMenuUser> BackupMenuUser { get; set; }
        public Table<Aviso> Aviso { get; set; }
        public Table<BackupAviso> BackupAvisos { get; set; }
        public Table<Imagen> Imagen { get; set; }
        public Table<RespuestaAuditor> RespuestaAuditor { get; set; }
        public Table<Foto> Foto { get; set; }
        public Table<TituloCombo> TituloCombo { get; set; }
        public Table<PresentacionEmpaque> PresentacionEmpaque { get; set; }
        public Table<SeccionRuta> SeccionRuta { get; set; }
        public Table<CatalogoSeccion> CatalogoSeccion { get; set; }
        public Table<ChatGrupo> ChatGrupo { get; set; }
        public Table<Qualification> Qualification { get; set; }
        public Table<QualiPregunta> QualiPregunta { get; set; }
        public Table<QualiSection> QualiSection { get; set; }
        public Table<TiendasCercanasRuta> TiendasCercanasRuta { get; set; }
        public Table<Tiempo> Tiempo { get; set; }
        public Table<BitacoraSincronizacion> BitacoraSincronizacion { get; set; }
        public Table<BitacoraSincFoto> BitacoraSincFoto { get; set; }
        public Table<ReporteIndicador> ReporteIndicador { get; set; }
        public Table<Telefono> Telefono { get; set; }
        public Table<TokenDevice> TokenDevice { get; set; }
        #endregion
    }
}