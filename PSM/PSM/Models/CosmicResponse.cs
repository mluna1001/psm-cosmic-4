﻿using PSM.Services;

namespace PSM.Models
{
    public class CosmicResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public object Objeto { get; set; }
        public Table<Usuario> Usuario { get; set; }
    }
}