﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.Models
{
    public class TokenDevice
    {
        [PrimaryKey]
        public int IdToken { get; set; }
        public string Token { get; set; }
    }
}
