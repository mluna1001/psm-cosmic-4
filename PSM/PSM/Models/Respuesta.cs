﻿using SQLite;
using System;

namespace PSM.Models
{
    public class Respuesta
    {
        [PrimaryKey]
        public int IdRespuesta { get; set; }
        public int IdPregunta { get; set; }
        public string TextoOpcion { get; set; }
        public bool Desencadena { get; set; }
        public double? Valor { get; set; }
        public Nullable<double> Puntuacion { get; set; }
        public bool? ValorDefault { get; set; }
        public Nullable<bool> CancelaCalificacion { get; set; }
        public Nullable<int> ClassId { get; set; }
        public Nullable<int> IdRespuestaAuditor { get; set; }
    }
}
