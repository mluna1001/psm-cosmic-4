﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace PSM.ViewModels
{
    public class usuarioModel : BaseViewModel
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public int IdUsuario { get; set; }
        public int IdKey { get; set; }
        public string Nombre { get; set; }
        public int NoEmpleado { get; set; }
        public string Contrasena { get; set; }
        public string Alias { get; set; }
        public string Correo { get; set; }
        public int? IdPerfil { get; set; }
        public int? IdUsuarioCosmic { get; set; }
        public int? IdProyecto { get; set; }
        public int? IdRegion { get; set; }
        public bool Bloqueado { get; set; }
        public int? IdParent { get; set; }
        public string UrlFoto { get; set; }
        public int? IdCliente { get; set; }
        public Guid? IdPrograma { get; set; }
        public string Telefono { get; set; }
        public bool? Eliminado { get; set; }
        public string ColorCard { get; set; }
        public List<usuarioModel> Users { get; set; }

        private List<AsistenciaUsuarioDto> asistencia;
        public List<AsistenciaUsuarioDto> Asistencia
        {
            get { return asistencia; }
            set
            {
                if (asistencia != value)
                {
                    asistencia = value;
                    if (asistencia.Any(s => s.Entrada))
                        Semaforo = new FileImageSource { File = "palomaverde.png" };
                    OnPropertyChanged("asistencia");
                }
            }
        }
        public FileImageSource Semaforo { get; set; } = new FileImageSource { File = "tacherojo.png" };

        //public ICommand DetailCommand { get; private set; }
        //public usuarioModel(int d)
        //{
        //    DetailCommand = new Command(DetalleClick);
        //}

        //private async void DetalleClick()
        //{
        //    var v = this;

        //    //await App.Navigator.PushAsync(new SemaforoDetailPage(v));
        //    var p = new NavigationPage(new SemaforoDetailPage(v));
        //    await Navigation.PushAsync(new SemaforoDetailPage(v));

        //    await Xamarin.Forms.Application.Current.MainPage.Navigation.PushAsync(p);
        //}

        protected virtual void OnPropertyChanged(string propertyName)
        {
            //PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class AsistenciaUsuarioDto
    {
        public int IdRuta { get; set; }
        public string TiendaNombre { get; set; }
        public int IdUsuario { get; set; }
        public Nullable<int> IdUsuarioCosmic { get; set; }
        public bool Entrada { get; set; }
        public decimal Latitud { get; set; }
        public decimal Longitud { get; set; }
        public decimal LatitudSalida { get; set; }
        public decimal LongitudSalida { get; set; }
        public DateTime FechaEntrada { get; set; }
        public DateTime FechaSalida { get; set; }
        public string DiaProgramado { get; set; }
    }
}
