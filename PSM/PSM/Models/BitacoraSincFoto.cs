﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class BitacoraSincFoto
    {
        [PrimaryKey]
        public int IdBitacoraSincFoto { get; set; }
        public int IdBitacoraSincronizacion { get; set; }
        public bool SincFoto { get; set; }
        public string URLtxt { get; set; }
    }
}
