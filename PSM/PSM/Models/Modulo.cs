﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class Modulo
    {
        [PrimaryKey]
        public int IdModulo { get; set; }
        public int IdCatalogoModulo { get; set; }
        public int IdProyecto { get; set; }
        public string  Descripcion { get; set; }
    }
}
