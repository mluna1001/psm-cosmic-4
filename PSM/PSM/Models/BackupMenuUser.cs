﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace PSM.Models
{
    public class BackupMenuUser
    {
        [PrimaryKey]
        public int IdMenuMovil { get; set; }
        public string Descripcion { get; set; }
        public string TargetType { get; set; }
        public string Icon { get; set; }
        public Nullable<int> IdParent { get; set; }
        public int Orden { get; set; }
        public bool Permiso { get; set; }
    }
}
