﻿namespace PSM.Models
{
    using SQLite;
    using System;

    public class Asistencia
    {
        [AutoIncrement, PrimaryKey]
        public int IdAsistencia { get; set; }
        public int IdRuta { get; set; }
        public int IdUsuario { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Longitud { get; set; }
        public decimal Latitud { get; set; }
        public bool estatus { get; set; }
        public string descripcion { get; set; }
        public bool Sincronizado { get; set; }
    }
}
