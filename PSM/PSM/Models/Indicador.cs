﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class Indicador
    {
        [PrimaryKey]
        public int IdIndicador { get; set; }
        public int IdCatalogoIndicador { get; set; }
        public int IdUsuario { get; set; }
        public int? IdTienda { get; set; }
        public string Descripcion { get; set; }
        public int NumeroIndicador { get; set; }
        public string Semaforo { get; set; }
        public string Color { get; set; }
        public DateTime Fecha { get; set; }
        public string TextoIndicador { get; set; }
    }
}
