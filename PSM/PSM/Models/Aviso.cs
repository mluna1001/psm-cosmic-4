﻿namespace PSM.Models
{
    using SQLite;
    using System;

    public class Aviso
    {
        [PrimaryKey]
        public int IdAviso { get; set; }
        public int IdRegion { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFinal { get; set; }
        public string Mensaje { get; set; }
        public string Titulo { get; set; }
        public int IdSeccion { get; set; }
        public int IdTipoAviso { get; set; }
        public string URL { get; set; }

        private bool? _vigente = null;
        public bool Vigente
        {
            get
            {
                if (_vigente.HasValue)
                {
                    return _vigente.Value;
                }
                else
                {
                    _vigente = FechaInicio <= DateTime.Now && DateTime.Now <= FechaFinal;
                }

                return _vigente.Value;
            }

            set
            {
                _vigente = value;
            }
        }
    }
}
