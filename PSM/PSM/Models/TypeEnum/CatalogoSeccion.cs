﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.TypeEnum
{
    public enum CatalogoSeccion : int
    {
        Entrada = 8,
        Secciones = 9,
        Abordajes = 10,
        Incidencias = 11,
        Avisos = 12,
        Imagen = 13,
        Salida = 14,
        Foto = 17,
        Indicador = 20
    }
}