﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PSM.TypeEnum
{
    public enum IdCatalogoBitacora
    {
        Default = 1,
        SeCreoBD,
        DBActions,
        IngresoUsuarioContraseña,
        LoginIncorrecto,
        LoginSinInternetNoSeLogeo,
        LoginCorrecto,
        NoGpsIniciado,
        SinPermisosGps,
        TapDisplayAction,
        EntradaLejosTienda,
        RecallUbicacionGps,
        SalidaLejosTienda,
        SalidaSinEntrada,
        VerificaEntradaSalidaGlobal,
        RealizoEntrada,
        EntradaEnServidor,
        EntradaGuardadaLocalmente,
        EntradaExternaGuardadaLocalmente,
        RealizoSalida,
        RealizoSalidaIncidencia,
        SalidaFaltanSecciones,
        SalidaEnServidor,
        SalidaGuardadaLocalmenta,
        SalidaExternaGuadadaLocalmente,
        SalioDeSeccionSinGuardar,
        GuardoRespuestas,
        IngresoASeccion,
        IngresoConstructor,
        TiendaSeleccionadaUpload,
        NoSubioTienda,
        DescargaExitosa,
        ErrorAlBajarInformacion,
        SubiendoInformacion,
        DatosCargadosEnServidor,
        NoSubioInformacionNoInternet,
        ErrorAlSubirInformacion,
        ObteniendoIdRutaDinamico,
        BorradoDBaseAutomatico,
        PreguntaBorradoDBaseAutomatico,
        SincronizacionRutaAutomatica,
        BDEliminadaAutomaticamente,
        SeRespaldoBD,
        LoginCorrectoConVersion,
        IniciaApp,
        AppOnSleep,
        AppOnResume,
        PreciosPageBarCode,
        Navegando,
        SeCargaronLosProductos,
        DescargandoCatalogoNoInternet,
        IniciaDescargaInformacion,
        TerminaDescargaInformacion,
        MensajeRegistrado,
        FaltaLlenarCampos,
        HoraFecha,
        RenderMultimedia,
        LoginBloqueado,
        IMEIDispositivo,
        AppsInstaladas,
        IMEIDistinto,
        EspacioAlmacenamientoDisponible,
        NivelBateria,
        Se
    }

    public enum TipoAviso
    {
        Informativo = 1,
        EnlaceExterno,
        Formulario,

    }
}
