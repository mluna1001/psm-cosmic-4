﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class ChatGrupo
    {

        [PrimaryKey]
        public int IdChatGrupo { get; set; }
        public string Descripcion { get; set; }
        public int IdProyecto { get; set; }
        public bool Baneado { get; set; }
        public DateTime CreadoEl { get; set; }
        public DateTime? ActualizadoEl { get; set; }

    }
}
