﻿
using SQLite;

namespace PSM.Models
{
	public class TituloCombo
	{
		[PrimaryKey]
		public int IdTituloCombo { get; set; }
		public int IdSeccion { get; set; }
		public string CompaniaTexto { get; set; }
		public string ProductoTexto { get; set; }
		public string PresentacionTexto { get; set; }
		public string UbicacionTexto { get; set; }
	}
}

