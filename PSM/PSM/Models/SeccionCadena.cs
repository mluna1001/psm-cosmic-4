﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class SeccionCadena
    {
        public int IdSeccionCadena { get; set; }
        public int IdSeccion { get; set; }
        public int IdCadena { get; set; }
        public int IdProyecto { get; set; }
    }
}
