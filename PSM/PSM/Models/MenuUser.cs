﻿
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class MenuUser
    {
        [PrimaryKey]
        public int IdMenuMovil { get; set; }
        public string Descripcion { get; set; }
        public string TargetType { get; set; }
        public string Icon { get; set; }
        public Nullable<int> IdParent { get; set; }
        public int Orden { get; set; }
        public bool Permiso { get; set; }
    }
}
