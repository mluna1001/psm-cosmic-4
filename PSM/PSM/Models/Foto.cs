﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class Foto
    {
        [PrimaryKey, AutoIncrement]
        public int IdPhoto { get; set; }
        public string Description { get; set; }
        public int IdPregunta { get; set; }
        public int CycleNumber { get; set; }
        public int SectionNumber { get; set; }
        public string PhotoName { get; set; }
        public string Alias { get; set; }
        public int IdProyectoTienda { get; set; }
        public int IdSeccion { get; set; }
        public int IdEmpaque { get; set; }
        public int IdUbicacion { get; set; }
        public bool FueSubida { get; set; }
        public int IdRuta { get; set; }
        public bool Sincronizado { get; set; }
    }
}
