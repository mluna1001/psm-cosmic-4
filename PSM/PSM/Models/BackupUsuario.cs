﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace PSM.Models
{
    public class BackupUsuario
    {
        [PrimaryKey]
        public int IdUsuario { get; set; }
        public string Alias { get; set; }
        public string Nombre { get; set; }
        public byte[] Password { get; set; }
        public int IdProyecto { get; set; }
        public int IdPerfil { get; set; }
        public int IdRegion { get; set; }
        public string AppCenterToken { get; set; }
        public int BorraBase { get; set; }
        [Ignore]
        public string IMEILog { get; set; }
    }
}
