using SQLite;
using System;

namespace PSM.Models
{
	public class Producto
	{
		[PrimaryKey]
		public int IdProducto { get; set; }
		public string Nombre { get; set; }
		public string SKU { get; set; }
		public int IdCatalogoProducto { get; set; }
		public bool Vigencia { get; set; }
		public int IdSeccion { get; set; }
		public string Titulo { get; set; }
		public int? Categoria { get; set; }
		public string CodigoBarras { get; set; }
		public double? Precio { get; set; }
		public double? PrecioPromo { get; set; }
		public int? IdParent { get; set; }
        public Nullable<int> Orden { get; set; }

    }
}
