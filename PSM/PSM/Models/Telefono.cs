﻿using SQLite;
using System;

namespace PSM.Models
{
    public class Telefono
    {
        [PrimaryKey]
        public int IdTelefono { get; set; }
        public string Modelo { get; set; }
        public string SO { get; set; }
        public Nullable<int> IdUsuario { get; set; }
        public Nullable<int> MemoriaRAMTotal { get; set; }
        public Nullable<int> MemoriaRAMDisponible { get; set; }
        public string DiscoTotal { get; set; }
        public string DiscoDisponible { get; set; }
        public Nullable<byte> Bateria { get; set; }
        public string IMEI { get; set; }
        public string AppsInstaladas { get; set; }
        public string VersionApp { get; set; }
        public bool Sincronizado { get; set; }
    }
}
