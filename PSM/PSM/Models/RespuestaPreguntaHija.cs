﻿using SQLite;

namespace PSM.Models
{
    public class RespuestaPreguntaHija
    {
        [PrimaryKey]
        public int IdRespuestaPreguntaHija { get; set; }
        public int IdRespuesta             { get; set; }
        public int IdPregunta { get; set; }
    }
}