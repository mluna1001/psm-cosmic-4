﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class CatalogoSeccion
{
    [PrimaryKey]
    public int IdCatalogoSeccion { get; set; }
    public string Descripcion { get; set; }
    public bool EsSeccion { get; set; }
    public string Nombre { get; set; }
    public string TargetType { get; set; }
    public string Icono { get; set; }

    public TipoSeccion Tipo()
    {
        return (TipoSeccion)IdCatalogoSeccion;
    }
}

public enum TipoSeccion
{
    /*
    1	Anaquel AnaquelPage NULL    NULL	1
2	Hallazgos   HallazgosPage   NULL    NULL	1
3	HallazgoProducto    PackagePage NULL    NULL	1
4	PrecioPromo PrecioPromoPage NULL    NULL	1
6	Precio  PackagePage NULL    NULL	1
7	Anaquel361  Anaquel361Page  NULL    NULL	1
8	Entrada EntradaPage NULL    NULL	0
9	Secciones   SeccionesPage   NULL    NULL	0
10	Abordajes   AbordajesPage   NULL    NULL	0
11	Incidencias IncidenciasPage NULL    NULL	0
12	Avisos  AvisosPage  NULL    NULL	0
13	Imagen  ImagenPage  NULL    NULL	0
14	Salida  SalidaPage  NULL    NULL	0 
        */

    Anaquel = 1,
    Hallazgos = 2,
    HallazgoProducto = 3,
    PrecioPromo = 4,
    Precio = 6,
    Anaquel361 = 7,
    Entrada = 8,
    Secciones = 9,
    Abordajes = 10,
    Incidencia11 = 11,
    Avisos = 12,
    Imagen = 13,
    Salida = 14,
    CalificacionProducto = 18,
    CodigoBarra = 20
}