﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class AbordajeMultimedia
    {

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int IdSeccionMenu { get; set; }
        public int IdMultimedia { get; set; }
        public int IdRuta { get; set; }
        public int Number { get; set; }
        public DateTime Inicio { get; set; }
        public DateTime Fin { get; set; }
        public bool Sincronizado { get; set; }

    }
}
