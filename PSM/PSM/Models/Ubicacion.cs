﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class Ubicacion
    {
        [PrimaryKey]
        public int IdUbicacion { get; set; }
        public int IdSeccion { get; set; }
        public string Nombre { get; set; }
    }
}
