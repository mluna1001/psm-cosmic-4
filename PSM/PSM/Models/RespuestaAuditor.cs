﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class RespuestaAuditor
    {
        [PrimaryKey, AutoIncrement]
        public int IdAnswerAudit { get; set; }
        public int IdSeccion { get; set; }
        public bool Sincronizado { get; set; }
        public int IdRespuestaAuditor { get; set; }
        public int IdRespuesta { get; set; }
        public int IdProyectoTienda { get; set; }
        public int IdRuta { get; set; }
        public int? IdUbicacion { get; set; }
        public string Respuesta { get; set; }
        public int? NumerodeCaja { get; set; }
        public byte[] Foto { get; set; }
        public string ArchivoRuta { get; set; }
        public int? FotoInvertida { get; set; }
        public string NombreDescarga { get; set; }
        public int? IdProducto { get; set; }
        public string FotoNombre { get; set; }
        public DateTime Fecha { get; set; }
    }
}
