﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PSM.Models
{
    public class PreguntaCadena
    {
        [PrimaryKey]
        public int IdPreguntaCadena { get; set; }
        public int IdPregunta { get; set; }
        public int IdCadena { get; set; }
        public Nullable<int> IdFormato { get; set; }
    }
}
