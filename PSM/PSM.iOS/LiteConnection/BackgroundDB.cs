﻿using System;
using System.IO;
using PSM.Interfaces;
using PSM.Models;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.LiteConnection.BackgroundDB))]
namespace PSM.iOS.LiteConnection
{
    public class BackgroundDB : Models.IBackground
    {
        public Background GetDataBase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "ioscache.db3");
            return new Background(path);
        }
    }
}
