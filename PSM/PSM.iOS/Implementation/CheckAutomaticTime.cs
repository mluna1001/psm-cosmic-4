﻿using System;
using PSM.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.CheckAutomaticTime))]
namespace PSM.iOS.Implementation
{
    public class CheckAutomaticTime : ICheckAutomaticTime
    {
        public CheckAutomaticTime()
        {
        }

        public bool IsAutomaticTime()
        {
            return true;
        }

        public bool IsAutomaticZone()
        {
            return true;
        }
    }
}
