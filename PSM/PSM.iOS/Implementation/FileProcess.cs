﻿using System;
using System.IO;
using Newtonsoft.Json;
using PSM.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.FileProcess))]
namespace PSM.iOS.Implementation
{
    public class FileProcess : IFileProcess
    {
        public JsonSerializer GetTextWriter(object objeto)
        {
            using (TextWriter textWriter = File.CreateText("LocalJsonFile.json"))
            {
                var serializer = new JsonSerializer();
                serializer.Serialize(textWriter, objeto);

                return serializer;
            }
        }
    }
}
