﻿using System;
using System.IO;
using PSM.Models;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.BackgroundDB))]
namespace PSM.iOS.Implementation
{
    public class BackgroundDB : Models.IBackground
    {
        public Background GetDataBase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "iossyscheck.db3");
            return new Background(path);
        }
    }
}
