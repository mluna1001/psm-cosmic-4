﻿using System;
using System.IO;
using PSM.Common;
using PSM.Interfaces;
using PSM.Models;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.PathService))]
namespace PSM.iOS.Implementation
{
    public class PathService : IDbService
    {
        public MobileResponse GetDataBase()
        {
            string personalFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, "ios_km.db3");

            MobileResponse cosmicResponse = new MobileResponse
            {
                Status = false,
                Message = "Existe"
            };

            if (File.Exists(path))
            {
                var psmdatabase = new DbPSM(path);
                cosmicResponse.Objeto = psmdatabase;
                cosmicResponse.Status = true;
            }
            else
            {
                var psmdatabase = new DbPSM(path);
                cosmicResponse.Objeto = psmdatabase;
            }

            return cosmicResponse;
        }
    }
}
