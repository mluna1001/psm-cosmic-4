﻿using System;
using PSM.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.StorageDevice))]
namespace PSM.iOS.Implementation
{
    public class StorageDevice : IStorageDevice
    {
        public string GetBatteryLevel()
        {
            return "0";
        }

        public string GetFreeStorageDevice()
        {
            return "0";
        }

        public string GetFullStorageDevice()
        {
            return "0";
        }
    }
}
