﻿using System;
using System.Threading.Tasks;
using PSM.Interfaces;
using ZXing.Mobile;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.iOS.Implementation.QrCodeScanningService))]
namespace PSM.iOS.Implementation
{
    public class QrCodeScanningService : IQrCodeScanningService
    {
        public async Task<string> ScanAsync()
        {
            var scanner = new MobileBarcodeScanner();
            var scanResults = await scanner.Scan();
            return (scanResults != null) ? scanResults.Text : string.Empty;
        }
    }
}
