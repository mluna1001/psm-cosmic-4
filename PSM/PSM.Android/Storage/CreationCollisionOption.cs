﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace PSM.Droid
{
	public enum CreationCollisionOption
	{
		GenerateUniqueName = 0,
		ReplaceExisting = 1,
		FailIfExists = 2,
		OpenIfExists = 3
	}
}