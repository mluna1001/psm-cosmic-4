﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using System.IO;
using PSM.Models;

[assembly: Dependency(typeof(PSM.Droid.LiteConnection.BackgroundDB))]
namespace PSM.Droid.LiteConnection
{
    public class BackgroundDB : IBackground
    {
        public Background GetDataBase()
        {
            var internalpath = global::Android.OS.Environment.ExternalStorageDirectory.Path;
            var path = Path.Combine(internalpath, "androidcache.db3");
            return new Background(path);
        }
    }
}