﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Common;
using PSM.Interfaces;
using PSM.Models;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Android.Implementation.PathService))]
namespace PSM.Android.Implementation
{
    public class PathService : IDbService
    {
        public MobileResponse GetDataBase()
        {
            string ipath = string.Empty;

            ipath = global::Android.OS.Environment.ExternalStorageDirectory.Path;
            string path = Path.Combine(ipath, "psm4.db3");
            var r = new MobileResponse { Status = false, Message = "Base no encontrada " + path };
            if (File.Exists(path))
            {
                r.Objeto = new DbPSM(path);
                r.Status = true;
            }
            else
            {
                r.Objeto = new DbPSM(path);
                r.Status = true;
            }
            return r;
        }
    }
}