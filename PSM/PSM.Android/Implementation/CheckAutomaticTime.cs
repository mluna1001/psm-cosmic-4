﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Helpers.Services;
using PSM.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(PSM.Android.Implementation.CheckAutomaticTime))]
namespace PSM.Android.Implementation
{
    public class CheckAutomaticTime : ICheckAutomaticTime
    {
        internal static Context Context;

        public bool IsAutomaticTime()
        {
            var variable = global::Android.Provider.Settings.Global.GetInt(Context.ContentResolver, global::Android.Provider.Settings.Global.AutoTime, 0);
            return variable == 1 ? true : false;
        }

        public bool IsAutomaticZone()
        {
            var variable = global::Android.Provider.Settings.Global.GetInt(Context.ContentResolver, global::Android.Provider.Settings.Global.AutoTimeZone, 0);
            return variable == 1 ? true : false;
        }
    }
}