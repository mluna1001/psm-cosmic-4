﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Gms.Extensions;
using Android.Media;
using Android.Util;
using Firebase.Iid;
using Firebase.Messaging;
using PSM.Models;
using Xamarin.Essentials;


namespace PSM.Droid.Implementation
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class MyFirebaseMessagingService : FirebaseMessagingService
    {
        const string TAG = "MyFirebaseMsgService";
        AndroidNotificationManager androidNotification = new AndroidNotificationManager();
        public override void OnMessageReceived(RemoteMessage message)
        {
            IDictionary<string, string> mensaje = message.Data;
            string titulo = mensaje["notiTitle"];
            string body = mensaje["notiBody"];
            androidNotification.CrearNotificacionLocal(titulo, body);

        }
        public override void OnNewToken(string token)
        {
            base.OnNewToken(token);

            Preferences.Set("TokenFirebase", token);
            sedRegisterToken(token);
        }
        public void sedRegisterToken(string token)
        {
            App.Token = token;         
        }
    }
}