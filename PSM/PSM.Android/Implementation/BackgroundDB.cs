﻿[assembly: Xamarin.Forms.Dependency(typeof(PSM.Droid.Implementation.BackgroundDB))]
namespace PSM.Droid.Implementation
{
    using PSM.Common;
    using PSM.Models;
    using System.IO;
    public class BackgroundDB : IBackground
    {
        public Background GetDataBase()
        {
            var internalpath = global::Android.OS.Environment.ExternalStorageDirectory.Path;
            var path = Path.Combine(internalpath, "androidsyscheck.db3");
            return new Background(path);
        }
    }
}