﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using System.Threading.Tasks;
using Android;
using Android.Widget;
using PSM.Android.Implementation;
using Android.AccessibilityServices;
using PSM.Droid.Implementation;
using Firebase.Iid;

namespace PSM.Droid
{
    [Activity(Label = "PSM", Icon = "@drawable/logo_firma", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        Task<bool> statuspermisos;
        protected override void OnCreate(Bundle savedInstanceState)
        {


            statuspermisos = TryGetPermission();

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            StorageDevice.Context = this;
            StorageDevice.Intent = Intent;

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            Xamarin.FormsMaps.Init(this, savedInstanceState);

            if (statuspermisos.Result)
            {
                LoadApplication(new App());
            }

            if ((int)Build.VERSION.SdkInt < 23)
            {
                LoadApplication(new App());
            }

            getTokenFCM();
            //App.Token = tok;
        }


        async void getTokenFCM()
        {
            string token = "";
            await Task.Run(() =>
            {
                token = FirebaseInstanceId.Instance.GetToken("401583749220", "FCM");

            });
            App.Token = token;
        }

        async Task<bool> TryGetPermission()
        {
            if ((int)Build.VERSION.SdkInt >= 23)
            {
                return await GetPermissionAsync();
            }
            return false;
        }

        const int RequestLocationId = 0;

        readonly string[] PermissionGroupLocation =
        {
            Manifest.Permission.WriteExternalStorage,
            Manifest.Permission.SystemAlertWindow,
            Manifest.Permission.AccessFineLocation,
            Manifest.Permission.Camera,
            Manifest.Permission.Internet,
            Manifest.Permission.ReadPhoneState,
            Manifest.Permission.AccessCoarseLocation
        };

        async Task<bool> GetPermissionAsync()
        {
            var status = false;
            const string permission = Manifest.Permission.WriteExternalStorage;
            const string ubicationPermission = Manifest.Permission.AccessFineLocation;
            const string cameraPermission = Manifest.Permission.Camera;
            const string readPhoneStatePermission = Manifest.Permission.ReadPhoneState;
            const string internetStatePermission = Manifest.Permission.Internet;

            if ((CheckSelfPermission(permission) == (int)global::Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(ubicationPermission) == (int)global::Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(cameraPermission) == (int)global::Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(readPhoneStatePermission) == (int)global::Android.Content.PM.Permission.Granted) &&
                (CheckSelfPermission(internetStatePermission) == (int)global::Android.Content.PM.Permission.Granted)
                )
            {
                //Toast.MakeText(this, "Permisos especiales concedidos", ToastLength.Short).Show();
                //LoadApplication(new App());
                return true;
            }

            if (ShouldShowRequestPermissionRationale(permission) &&
                ShouldShowRequestPermissionRationale(ubicationPermission) &&
                ShouldShowRequestPermissionRationale(cameraPermission) &&
                ShouldShowRequestPermissionRationale(readPhoneStatePermission) &&
                (CheckSelfPermission(internetStatePermission) == (int)global::Android.Content.PM.Permission.Granted)
                )
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("Permisos");
                alert.SetMessage("Cosmic Contigo requiere permisos para continuar");
                alert.SetPositiveButton("Conceder permisos", (senderAlert, args) =>
                {
                    RequestPermissions(PermissionGroupLocation, RequestLocationId);
                    status = true;
                });
                alert.SetNegativeButton("Cancelar", (senderAlert, args) =>
                {
                    Toast.MakeText(this, "Permisos denegados", ToastLength.Short).Show();
                    status = false;
                });

                Dialog dialog = alert.Create();
                dialog.Show();

                return status;
            }
            RequestPermissions(PermissionGroupLocation, RequestLocationId);
            return false;
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] global::Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            //base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            switch (requestCode)
            {
                case RequestLocationId:
                    if (grantResults[0] == (int)global::Android.Content.PM.Permission.Granted)
                    {
                        //Toast.MakeText(this, "Permisos especiales concedidos", ToastLength.Short).Show();
                        if (!this.IsFinishing)
                        {
                            //show dialog
                            //ShowDialog(1);
                        }
                        LoadApplication(new App());
                    }
                    else
                    {
                        Toast.MakeText(this, "Permisos especiales denegados", ToastLength.Short).Show();
                        AlertDialog.Builder alert = new AlertDialog.Builder(this);
                        alert.SetTitle("Permisos");
                        alert.SetMessage("Cosmic Contigo requiere permisos para continuar, al ser denegados la aplicación se cerrará.");
                        alert.SetPositiveButton("Salir", (senderAlert, args) =>
                        {
                            CloseApplication var = new CloseApplication();
                            var.CloseApp();
                        });

                        Dialog dialog = alert.Create();
                        dialog.Show();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}