﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using PSM.Controls;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using Android.Media;
using System.Threading.Tasks;
using PCLStorage;
using System.IO;
using System.Net.Http;
using Microsoft.AppCenter.Crashes;
using PSM.Droid.Storage;

[assembly: ExportRenderer(typeof(PSM.Controls.MediaElement), typeof(PSM.Droid.Controls.MediaElement))]
namespace PSM.Droid.Controls
{
    public class MediaElement : ViewRenderer<PSM.Controls.MediaElement, VideoView>
    {
        private VideoView _videoview { get; set; }

        protected override async void OnElementChanged(ElementChangedEventArgs<PSM.Controls.MediaElement> e)
        {
            base.OnElementChanged(e);
            var element = Element as PSM.Controls.MediaElement;
            if (element != null && e.OldElement == null)
            {
                var file = await SaveFile(element);
                var native = CreateNativeControl(element, file);
                SetNativeControl(native);
            }
        }

        private async Task<StorageFile> SaveFile(PSM.Controls.MediaElement element)
        {
            StorageFile videofile = null;
            IFile file = null;
            if (element.Source.IsFile)
            {
                var path = element.Source.FilePath;
                var pathsplit = path.Split('/');
                var filename = pathsplit[pathsplit.Length - 1];
                try
                {
                    file = await PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(filename);
                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex, App.TrackData());
                }

                if (file != null)
                {
                    try
                    {
                        videofile = KnownFolders.InternalStorage.GetFile(file.Name);
                    }
                    catch (Exception ex)
                    {
                        videofile = null;
                        Crashes.TrackError(ex, App.TrackData());
                    }

                    videofile = await WriteVideoFile(videofile, file, filename);
                }
                else
                {
                    file = await IFileFromUri(new Uri(path, UriKind.RelativeOrAbsolute));
                    videofile = await WriteVideoFile(videofile, file, filename);
                }
            }
            else if (element.Source.IsUri)
            {
                var path = element.Source.Uri.AbsoluteUri;
                var filename = element.Source.Uri.AbsolutePath.Replace("/", string.Empty);
                file = await IFileFromUri(element.Source.Uri);
                videofile = await WriteVideoFile(videofile, file, filename);
            }
            else
            {
                if (element.Source.Stream != null)
                {
                    try
                    {
                        videofile = KnownFolders.InternalStorage.GetFile(element.Source.TempFileName);
                    }
                    catch (Exception ex)
                    {
                        Crashes.TrackError(ex, App.TrackData());
                    }

                    if (videofile == null)
                    {
                        videofile = KnownFolders.InternalStorage.CreateFile(element.Source.TempFileName, CreationCollisionOption.OpenIfExists);
                        await element.Source.Stream.CopyToAsync(videofile.Open(System.IO.FileAccess.Write, Forms.Context));
                    }
                }
            }

            return videofile;
        }

        private async Task<StorageFile> WriteVideoFile(StorageFile video, IFile file, string filename)
        {
            StorageFile videofile = video;

            if (videofile == null && file != null)
            {
                videofile = KnownFolders.InternalStorage.CreateFile(filename, CreationCollisionOption.OpenIfExists);
                using (var streamreader = await file.OpenAsync(PCLStorage.FileAccess.Read))
                {
                    var streamwriter = videofile.Open(System.IO.FileAccess.ReadWrite, Forms.Context);
                    streamreader.CopyTo(streamwriter);
                    streamwriter.Dispose();
                    streamreader.Dispose();
                }
            }
            else
            {
                if (videofile == null)
                {
                    throw new Exception("No fue posible obtener el archivo");
                }
            }

            return videofile;
        }

        private async Task<IFile> IFileFromUri(Uri uri)
        {
            var path = uri.AbsoluteUri;
            var filename = uri.AbsolutePath.Replace("/", string.Empty);

            IFile file = null;

            if (path.Contains("http"))
            {
                var client = new HttpClient();
                var data = await client.GetByteArrayAsync(path);
                if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                {
                    try
                    {
                        file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                        using (var stream = new MemoryStream(data))
                        {
                            await stream.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                        }
                    }
                    catch (Exception ex)
                    {
                        file = null;
                        Crashes.TrackError(ex, App.TrackData());
                    }
                }
                else
                {
                    file = await FileSystem.Current.LocalStorage.GetFileAsync(filename);
                }
            }
            else
            {
                if (System.IO.File.Exists(path))
                {
                    if (await FileSystem.Current.LocalStorage.CheckExistsAsync(filename) == ExistenceCheckResult.NotFound)
                    {
                        try
                        {
                            file = await PCLStorage.FileSystem.Current.LocalStorage.CreateFileAsync(filename, PCLStorage.CreationCollisionOption.OpenIfExists);
                            using (var tempfile = File.Open(path, FileMode.Open))
                            {
                                await tempfile.CopyToAsync(await file.OpenAsync(PCLStorage.FileAccess.ReadAndWrite));
                            }
                        }
                        catch
                        {
                            file = null;
                        }
                    }
                    else
                    {
                        file = await FileSystem.Current.LocalStorage.GetFileAsync(filename);
                    }
                }
                else
                {
                    throw new Exception("No podemos obtener el archivo");
                }
            }
            return file;
        }

        private VideoView CreateNativeControl(PSM.Controls.MediaElement element, StorageFile file = null)
        {
            element.Start += Element_Start;
            element.End += Element_End;
            _videoview = new VideoView(Forms.Context)
            {
                LayoutParameters = new ViewGroup.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent)
            };

            _videoview.Error += (s, a) =>
            {
                element.OnMediaFailed(EventArgs.Empty);
            };

            _videoview.Completion += (s, a) =>
            {
                element.OnMediaEnded(EventArgs.Empty);
            };

            if (file != null)
            {
                _videoview.SetVideoPath(file.FullPath);
            }
            else
            {
                return _videoview;
            }

            MediaController controller = new MediaController(Forms.Context);
            controller.SetAnchorView(_videoview);

            if (element.AutoPlay)
            {
                _videoview.Start();
                element.OnMediaOpened(EventArgs.Empty);
            }

            _videoview.SetMediaController(controller);
            return _videoview;
        }

        /// <summary>
        /// Evento lanzado al realizarse la accion STOP en el MediaElement [PSM.Controls]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Element_End(object sender, EventArgs e)
        {
            if (_videoview != null)
            {
                _videoview.StopPlayback();
            }
        }

        /// <summary>
        /// Evento lanzado al realizarse la accion Play en el MediaElement [PSM.Controls]
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Element_Start(object sender, EventArgs e)
        {
            if (_videoview != null)
            {
                _videoview.Start();
            }
        }
    }
}