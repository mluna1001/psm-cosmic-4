﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;
using System.Net;
using Xamarin.Forms;
using PSM.Controls;
using System.IO;
using Android.Graphics;
using PSM.Function;
using Apitron.PDF.Rasterizer;
using Apitron.PDF.Rasterizer.Configuration;
using PSM.Droid.Storage;

[assembly: ExportRenderer(typeof(PSM.Controls.PdfElement), typeof(PSM.Droid.Controls.PdfElement))]
namespace PSM.Droid.Controls
{
    public class PdfElement : ViewRenderer<PSM.Controls.PdfElement, global::Android.Widget.ListView>
    {
        protected override async void OnElementChanged(ElementChangedEventArgs<PSM.Controls.PdfElement> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
            {
                var pdfelement = (Element as Xamarin.Forms.View) as PSM.Controls.PdfElement;
                var filesplit = pdfelement.Uri.Split('/');
                var filename = filesplit[filesplit.Length - 1];
                var file = await PCLStorage.FileSystem.Current.LocalStorage.GetFileAsync(filename);
                StorageFile pdfnewfile = null;
                try
                {
                    pdfnewfile = KnownFolders.InternalStorage.GetFile(filename);
                }
                catch
                {
                }

                if (pdfnewfile == null)
                {
                    pdfnewfile = KnownFolders.InternalStorage.CreateFile(filename);
                    using (var streamreader = await file.OpenAsync(PCLStorage.FileAccess.Read))
                    {
                        var streamwriter = pdfnewfile.Open(System.IO.FileAccess.ReadWrite, Forms.Context);
                        streamreader.CopyTo(streamwriter);
                        streamwriter.Dispose();
                        streamreader.Dispose();
                    }
                }

                var listimage = ImagesFromPDF(pdfnewfile.Open(FileAccess.Read, Forms.Context));
                var nativecontrol = new global::Android.Widget.ListView(Forms.Context);
                nativecontrol.Adapter = new ImageAdapter(Forms.Context, 0, listimage);
                SetNativeControl(nativecontrol);
            }
        }

        public static List<ImageSrc> ImagesFromPDF(Stream file)
        {
            List<ImageSrc> imagenes = new List<ImageSrc>();
            Document doc = new Document(file);
            foreach (var page in doc.Pages)
            {
                int[] bitmapData = page.RenderAsInts((int)page.Width, (int)page.Height, new RenderingSettings());
                Bitmap bm = Bitmap.CreateBitmap(bitmapData, (int)page.Width, (int)page.Height, Bitmap.Config.Argb8888);
                var image = new ImageView(Forms.Context);
                image.SetImageBitmap(bm);
                imagenes.Add(new ImageSrc { Data = file, View = image });
            }
            return imagenes;
        }

    }

    public class ImageSrc
    {
        public Stream Data { get; set; }
        public ImageView View { get; set; }
    }

    public class ImageAdapter : ArrayAdapter<ImageSrc>
    {
        private List<ImageSrc> Items { get; set; }

        public ImageAdapter(Context context, int resource, List<ImageSrc> items) : base(context, resource, items)
        {
            Items = items;
        }

        public override global::Android.Views.View GetView(int position, 
            global::Android.Views.View convertView, ViewGroup parent)
        {
            return Items[position].View;
        }
    }
}